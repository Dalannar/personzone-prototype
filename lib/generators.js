const _ = require("lodash");

const UserGenerator = require("./generators/UserGenerator");
const AuthGenerator = require("./generators/AuthGenerator");
const UserPrivacyGenerator = require("./generators/UserPrivacyGenerator");
const ZoneGenerator = require("./generators/ZoneGenerator");
const SubzoneGenerator = require("./generators/SubzoneGenerator");
const ZonetraitGenerator = require("./generators/ZonetraitGenerator");
const RatingGenerator = require("./generators/RatingGenerator");
const CommentGenerator = require("./generators/CommentGenerator");
const LocationGenerator = require("./generators/LocationGenerator");

module.exports = function(config){
	let generators = {
		auth: new AuthGenerator(config.auth),
		user: new UserGenerator(config.user),
		user_privacy: new UserPrivacyGenerator(config.user_privacy),
		zone: new ZoneGenerator(config.zone),
		subzone: new SubzoneGenerator(config.subzone),
		zonetrait: new ZonetraitGenerator(config.zonetrait),
		rating: new RatingGenerator(config.rating),
		comment: new CommentGenerator(config.comment),
		location: new LocationGenerator(config.location),
	};

	_.each(generators, function(g){
		g.setGenerators(generators);
	});

	return generators;
};