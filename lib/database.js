const Sequelize = require("sequelize");
const winston = require("winston");

module.exports = function(host, port, username, password, database){
	winston.info(`Establishing database connection with ${username}@${host}:${port} - Database: ${database}`);

	const db = new Sequelize({
		host, port, username, password, database,
		dialect: "mysql",
		define: {
			underscored: true,
		},
		// `logging: winston.debug` doesn't work, it prints a lot of internal shit
		logging: (log) => { winston.debug(log); }
	});

	require("./models")(db);

	return db;
};