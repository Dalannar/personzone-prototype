module.exports = function(db, dataTypes){
	const user_location = db.define("user_location", {});

	user_location.ACD = {
		columns: {
			total: [],
			admin: [],
			owner: ["created_at", "updated_at"],
			minimal: ["user_id", "location_id"],
		}
	};

	return user_location;
};