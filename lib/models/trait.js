const _ = require("lodash");

module.exports = function(db, dataTypes){
	const Trait = db.define("trait", {
		// The name of the trait
		name: {
			type: dataTypes.TEXT,
			allowNull: false,

			set(val){
				this.setDataValue("name", _.upperFirst(_.toLower(val)));
			}
		},

	},{
		timestamps: false,
	});

	Trait.ACD = {
		columns: {
			total: [],
			admin: [],
			owner: [],
			minimal: ["id", "name"],
		},
	};

	return Trait;
};