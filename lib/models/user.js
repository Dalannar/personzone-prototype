const { toWordCase } = require("../../util/validation");
const { GENDERS, BLOOD_TYPES, RELATIONSHIP_STATUSES } = require("../../util/enums"); 
const crypto = require("crypto");
const uuidv1 = require("uuid/v1");

module.exports = function(db, dataTypes){
	let User = db.define("user", {
		name: {
			type: dataTypes.STRING,
			allowNull: false,

			set(val){
				this.setDataValue("name", toWordCase(val));
			}
		},

		// The user's email
		email: {
			type: dataTypes.STRING,
			allowNull: false,
			unique: true,
			validate: {
				isEmail: true,
			},
		},

		// The user's personal url that sits at the end of the /zone url
		purl: {
			type: dataTypes.STRING,
			allowNull: false,
			unique: true,
		},
		
		// The default zone
		default_zone: {
			type: dataTypes.INTEGER
		},

		// The user's personzone score
		score: {
			type: dataTypes.INTEGER,
			allowNull: false,
			defaultValue: 0,
		},

		// The last date when the score was updated
		score_updated_at: {
			type: dataTypes.DATE,
			defaultValue: null,
		},

		// The user's gender
		gender: {
			type: dataTypes.ENUM,
			values: GENDERS,
			allowNull: true,
			defaultValue: null,
		},

		// The user's blood type
		blood_type: {
			type: dataTypes.ENUM,
			values: BLOOD_TYPES,
			allowNull: true,
			defaultValue: null,
		},

		// The user's date of birth
		dob: {
			type: dataTypes.DATEONLY,
		},

		// Whether the user has a driving license or not
		driving_license: {
			type: dataTypes.BOOLEAN,
			allowNull: true,
			defaultValue: null,
		},

		// Whether the user is looking for work or not
		seeking_work: {
			type: dataTypes.BOOLEAN,
			allowNull: true,
			defaultValue: null,
		},

		// The user's current relationship status
		rel_status: {
			type: dataTypes.ENUM,
			values: RELATIONSHIP_STATUSES,
			allowNull: true,
			defaultValue: null,
		},

		// The user's defining word (whatever that is...)
		def_word: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValue: null,
		},
	});

	User.ACD = {
		columns: {
			total: [],
			admin: ["id"],
			owner: ["email", "created_at", "updated_at", "dob"],
			minimal: ["name", "purl", "score", "gender", "seeking_work", "driving_license", "rel_status", "def_word", "blood_type"],
		}
	};

	return User;
};