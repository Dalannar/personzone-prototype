module.exports = function(db, dataTypes){
	const user_link = db.define("user_link", {
		// The name of the link
		title: {
			type: dataTypes.STRING,
			allowNull: false,
		},

		// The url of the link
		url: {
			type: dataTypes.STRING,
			allowNull: false,
		}
	});

	user_link.ACD = {
		columns: {
			total: [],
			admin: ["id", "user_id"],
			owner: ["created_at", "updated_at"],
			minimal: ["title", "url"],
		}
	};

	return user_link;
};