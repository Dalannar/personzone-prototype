const { toWordCase } = require("../../util/validation");

module.exports = function(db, dataTypes){
	const Zone = db.define("zone", {
		// The zone's type
		type: {
			type: dataTypes.ENUM,
			values: require("./zonetypes"),
			allowNull: false,
		},

		// The zone's name
		name: {
			type: dataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty: true,
				is: /^[-a-zA-Z0-9_ ]+$/i,
			},

			set(val){
				this.setDataValue("name", toWordCase(val));				
			}
		},

		// The zone's url name
		zurl: {
			type: dataTypes.STRING,
			allowNull: false,
			validate: {
				is: /^[-a-zA-Z0-9_]+$/i,
			},
			unique: "uniqueUserZurl",
		},

		user_id: {
			type: dataTypes.INTEGER,
			unique: "uniqueUserZurl",
		},
	});
	
	Zone.ACD = {
		columns: {
			total: [],
			admin: [],
			owner: ["user_id", "created_at", "updated_at"],
			minimal: ["id", "type", "name", "zurl"],
		},
	};

	return Zone;
};