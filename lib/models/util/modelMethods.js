const _ = require("lodash");

module.exports = {

	/**
	 * Get an object with the appropriate information contained in this instance based on the level of access supplied
	 * @param {String|Object} ACL Access Control List - The level of access to get information for.
	 * 		If the model instance has multiple sub instances within it (result of an includes query)
	 * 		then an object can be supplied with different levels of access for different models.
	 * 		If an object is provided there should be atleast one "root" property which defines the access level of the top model
	 * 		A "default" property can be supplied to use for any subsequent include models that have no specified access level.
	 * 		If "default" is not present in this case, the "root" access level will be used.
	 * @returns {Object} An object with the appropriate information contained in this instance based on the level of access supplied 
	 */
	getData: function(ACL="minimal"){
		const { ACD } = this.constructor;

		// There is no ACU information, fallback and return sequelize
		if(!ACD)
			return this.get({plain: true});

		// Built the ACL object
		if(_.isString(ACL)){
			ACL = {root: ACL};
		}else{
			// If the object provided does not contain a "root" property create the default one
			if(!_.has(ACL, "root")){
				ACL.root = "minimal";
			}
		}

		let retObj = {};

		// No breaks because they are subsets of each other
		switch(ACL.root){
			case "total": 
				_.forEach(ACD.columns.total, key => retObj[key] = this.getDataValue(key));
			case "admin": 
				_.forEach(ACD.columns.admin, key => retObj[key] = this.getDataValue(key));
			case "owner": 
				_.forEach(ACD.columns.owner, key => retObj[key] = this.getDataValue(key));
			case "minimal":
			default: 
				_.forEach(ACD.columns.minimal, key => retObj[key] = this.getDataValue(key));
		}

		if(this._options){
			if(this._options.includeNames){
				_.forEach(this._options.includeNames, function(key){
					const data = this.get(key);
					if(!data)
						return;

					// New ACL for the next level
					// First take the specified access level for the next data,
					// if that doesn't exist try default, otherwise use ACL.root
					const nACL = _.assign({}, ACL, {root: ACL[key] || ACL.default || ACL.root});

					retObj[key] = _.isArray(data) ? _.map(data, d => d.getData(nACL)) : data.getData(nACL);

				}.bind(this));
			}
		}

		return retObj;
	}
};