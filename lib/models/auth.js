const crypto = require("crypto");

module.exports = function(db, dataTypes){
	const Auth = db.define("auth", {
		// The user's hashed password
		password: {
			type: dataTypes.STRING,
			allowNull: false,

			set(val){
				const salt = this.getDataValue("salt") || crypto.randomBytes(16).toString("hex");
				this.setDataValue("salt", salt);
				this.setDataValue("password", this.hashPassword(val, salt));

				const emailVerificationHash = this.getDataValue("email_verification_hash") || crypto.randomBytes(16).toString("hex");
				this.setDataValue("email_verification_hash", emailVerificationHash);
			}
		},

		// The user's password salt
		salt: {
			type: dataTypes.STRING,
			allowNull: false,
		},

		// Whether or not the user's email has been verified or not
		email_verified: {
			type: dataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},

		// The hash to use to verify a user's email address
		email_verification_hash: {
			type: dataTypes.STRING,
			allowNull: false,
		},

		// The last time the user has logged into the application
		last_login: {
			type: dataTypes.DATE,
			allowNull: false,
			defaultValue: dataTypes.NOW,
		},
	});

	// TODO: Remove this trash
	Auth.accessLevels = {
		columns: {
			full: ["password", "salt", "email_verification_hash", "last_login"],
			owner: ["email_verified"],
			minimal: []
		}
	};

	Auth.ACD = {
		columns: {
			total: ["password", "salt"],
			admin: ["email_verification_hash"],
			owner: ["email_verified", "last_login", "created_at", "updated_at"],
			minimal: ["id"],
		},
	};

	Auth.prototype.hashPassword = function(password, salt){
		const hash = crypto.createHash("sha256");
		hash.update(password + salt);

		return hash.digest("hex");
	};

	Auth.prototype.validatePassword = function(password){
		const salt = this.getDataValue("salt");
		const hashed = this.hashPassword(password, salt);

		return this.getDataValue("password") === hashed;
	};

	return Auth;
};