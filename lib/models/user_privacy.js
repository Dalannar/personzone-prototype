const { PRIVACIES } = require("../../util/enums");

module.exports = function(db, dataTypes){
	const UserPrivacy = db.define("user_privacy", {
		// User's email privacy setting
		email: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's join_date privacy setting
		join_date: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's gender privacy setting
		gender: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's date of birth privacy setting
		dob: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's blood_type privacy setting
		blood_type: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's driving license privacy setting
		driving_license: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's seeking work privacy setting
		seeking_work: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's relationship status privacy setting
		rel_status: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},

		// User's defining word privacy setting
		def_word: {
			type: dataTypes.ENUM,
			values: PRIVACIES,
			allowNull: false,
			defaultValue: "private"
		},
	});

	UserPrivacy.ACD = {
		columns: {
			total: [],
			admin: ["id", "user_id"],
			owner: ["created_at", "updated_at"],
			minimal: ["email", "join_date", "gender", "dob", "blood_type", "driving_license", "seeking_work", "rel_status", "def_word"],
		}
	};

	return UserPrivacy;
};