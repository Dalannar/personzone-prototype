const { EXPERIENCE_TYPES } = require("../../util/enums");

module.exports = function(db, dataTypes){
	const user_experience = db.define("user_experience", {
		// The start data of the experience
		start_date: {
			type: dataTypes.DATE,
			allowNull: true,
			defaultValue: null,
		},

		// The end data of the experience
		end_date: {
			type: dataTypes.DATE,
			allowNull: true,
			defaultValue: null,
		},

		// The title of the experience
		title: {
			type: dataTypes.STRING,
			allowNull: false,
		},

		// The name of the institution
		institution: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValue: null,
		},

		// Google maps place_id for the location of the experience
		place_id: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValue: null,
		},

		// The type of user experience
		type: {
			type: dataTypes.ENUM,
			values: EXPERIENCE_TYPES,
			allowNull: true,
			defaultValue: null,
		},

		// A user wrote description of the experience
		description: {
			type: dataTypes.TEXT,
			allowNull: true,
			defaultValue: null,
		}
	});

	user_experience.ACD = {
		columns: {
			total: [],
			admin: ["id", "user_id"],
			owner: ["created_at", "updated_at"],
			minimal: ["description", "institution", "place_id", "institution", "type", "title", "start_date", "end_date"],
		}
	};

	return user_experience;
};