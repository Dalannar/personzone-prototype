const _ = require("lodash");

module.exports = function(db, dataTypes){
	const Subzone = db.define("subzone", {
		// The subzone's name
		name: {
			type: dataTypes.STRING,
			allowNull: false,

			validate: {
				is: /^[a-zA-Z ]+$/i,
			},

			set(val){
				this.setDataValue("name", _.upperFirst(_.toLower(val)));
			}
		},
	});

	Subzone.ACD = {
		columns: {
			total: [],
			admin: [],
			owner: ["created_at", "updated_at"],
			minimal: ["id", "zone_id", "name"],
		},
	};

	return Subzone;
};