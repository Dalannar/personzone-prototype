module.exports = function(db, dataTypes){
	const Zonetrait = db.define("zonetrait", {
		rank_updated_at: {
			type: dataTypes.DATE,
			allowNull: false,
			defaultValue: db.fn("NOW"),
		},

		rank_outdated: {
			type: dataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: true,
		},

		number_ratings: {
			type: dataTypes.INTEGER.UNSIGNED,
			allowNull: false,
			defaultValue: 0,
		},

		average_rating: {
			type: dataTypes.FLOAT,
			allowNull: false,
			defaultValue: 0,
		},
	});

	Zonetrait.ACD = {
		columns: {
			total: [],
			admin: ["rank_outdated", "rank_updated_at"],
			owner: ["created_at", "updated_at"],
			minimal: ["id", "trait_id", "average_rating", "number_ratings"],
		},
	};

	return Zonetrait;
};