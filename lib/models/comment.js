module.exports = function(db, dataTypes){
	const Comment = db.define("comment", {
		// The text of the comment
		comment: {
			type: dataTypes.TEXT,
			allowNull: false,
		},

		// Whether this comment has been thanked or not
		thanked: {
			type: dataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},

		// Whether the author of the comment should be anonymous or not towards the recipient of the comment
		anonymous: {
			type: dataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: true,
		}
	});

	Comment.ACD = {
		columns: {
			total: [],
			admin: [],
			owner: ["created_at", "updated_at"],
			minimal: ["id", "comment", "thanked", "anonymous"],
		},
	};

	return Comment;
};