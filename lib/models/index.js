const modelMethods = require("./util/modelMethods");

module.exports = function(db){
	// Importing the tables
	db.import("./auth");
	db.import("./user");
	db.import("./user_link");
	db.import("./user_experience");
	db.import("./user_phone");
	db.import("./user_privacy");
	db.import("./location");
	db.import("./user_location");
	db.import("./zone");
	db.import("./subzone");
	db.import("./comment");
	db.import("./trait");
	db.import("./zonetrait");
	db.import("./rating");

	// Add all the custom model instance methods to each model
	for(let model in db.models){
		for(let method in modelMethods){
			db.models[model].prototype[method] = modelMethods[method];
		}
	}

	// Setting the associations between tables
	db.models.auth.belongsTo(db.models.user);
	db.models.user.hasOne(db.models.auth);

	db.models.zone.belongsTo(db.models.user, { onDelete: "cascade" });
	db.models.user.hasMany(db.models.zone);
	db.models.user.belongsTo(db.models.zone, { foreignKey: "default_zone", constraints: false });

	db.models.user_privacy.belongsTo(db.models.user, { onDelete: "cascade" });
	db.models.user.hasOne(db.models.user_privacy);

	db.models.user_experience.belongsTo(db.models.user, { onDelete: "cascade" });
	db.models.user.hasMany(db.models.user_experience);

	db.models.location.belongsToMany(db.models.user, { through: db.models.user_location});
	db.models.user.belongsToMany(db.models.location, { through: db.models.user_location});

	db.models.user_phone.belongsTo(db.models.user, { onDelete: "cascade" });
	db.models.user.hasMany(db.models.user_phone);

	db.models.user_link.belongsTo(db.models.user, { onDelete: "cascade" });
	db.models.user.hasMany(db.models.user_link);

	db.models.subzone.belongsTo(db.models.zone, { onDelete: "cascade" });
	db.models.zone.hasMany(db.models.subzone);

	db.models.comment.belongsTo(db.models.subzone, { onDelete: "cascade" });
	db.models.subzone.hasMany(db.models.comment);
	db.models.comment.belongsTo(db.models.user);
	db.models.user.hasMany(db.models.comment);

	db.models.zonetrait.belongsTo(db.models.trait, { onDelete: "cascade" });
	db.models.zonetrait.belongsTo(db.models.zone, { onDelete: "cascade" });
	db.models.trait.hasMany(db.models.zonetrait);
	db.models.zone.hasMany(db.models.zonetrait);

	db.models.rating.belongsTo(db.models.zonetrait, { onDelete: "cascade" });
	db.models.rating.belongsTo(db.models.user, { onDelete: "cascade" });
	db.models.zonetrait.hasMany(db.models.rating);
	db.models.user.hasMany(db.models.rating);
};