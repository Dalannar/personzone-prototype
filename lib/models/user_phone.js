module.exports = function(db, dataTypes){
	const user_phone = db.define("user_phone", {
		// Whether this phone contact is private or not
		private: {
			type: dataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: true,
		},

		// The phone number
		number: {
			type: dataTypes.STRING,
			allowNull: false,
		},

		// The contact title
		title: {
			type: dataTypes.STRING,
			allowNull: false
		},
	});
	
	user_phone.ACD = {
		columns: {
			total: [],
			admin: ["id", "user_id"],
			owner: ["created_at", "updated_at"],
			minimal: ["private", "number", "title"]
		}
	};

	return user_phone;
};