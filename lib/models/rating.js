module.exports = function(db, dataTypes){
	const Rating = db.define("rating", {
		// The value of the rating
		rating: {
			type: dataTypes.INTEGER,

			validate: {
				min: 1,
				max: 5,
			},
		},

		// Whether or not the rating has been thanked
		thanked: {
			type: dataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},

	});

	Rating.ACD = {
		columns: {
			total: [],
			admin: ["id"],
			owner: ["user_id", "created_at", "updated_at"],
			minimal: ["zonetrait_id", "rating", "thanked"],
		},
	};

	return Rating;
};