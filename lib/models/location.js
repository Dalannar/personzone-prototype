const { LOCATION_TYPES } = require("../../util/enums");

module.exports = function(db, dataTypes){
	const location = db.define("location", {
		// Which type of location this information is about
		type: {
			type: dataTypes.ENUM,
			values: LOCATION_TYPES,
			allowNull: false,
			defaultValue: LOCATION_TYPES[0],
		},

		place_id: {
			type: dataTypes.STRING,
			allowNull: false,
		},

		administrative_area_level_1: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValues: null,
		},

		administrative_area_level_2: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValues: null,
		},

		administrative_area_level_3: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValues: null,
		},

		country: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValues: null,
		},

		locality: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValue: null,
		},

		route: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValue: null,
		},

		point: {
			type: dataTypes.GEOMETRY,
			allowNull: true,
			defaultValue: null,
		},

		formatted_address: {
			type: dataTypes.STRING,
			allowNull: true,
			defaultValue: null,
		}
	});

	location.ACD = {
		columns: {
			total: [],
			admin: ["id"],
			owner: ["created_at", "updated_at"],
			minimal: ["type", "place_id", "administrative_level_1", "administrative_level_2", "administrative_level_3", "country", "point", "formatted_address"],
		}
	};

	return location;
};