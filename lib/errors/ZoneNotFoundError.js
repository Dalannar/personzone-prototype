const ApiError = require("./ApiError");

module.exports = class ZoneNotFoundError extends ApiError{
	constructor(zoneId, ...args){
		super(...args);
		this.zoneId = zoneId;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 404;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "ZoneNotFoundError",
			message: `Zone with id '${this.zoneId}' could not be found.`,
		};
	}
};