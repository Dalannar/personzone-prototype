const ApiError = require("./ApiError");

module.exports = class SubzoneNotFoundError extends ApiError{
	constructor(subzoneId, ...args){
		super(...args);
		this.subzoneId = subzoneId;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 404;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "SubzoneNotFoundError",
			message: `Subzone with id '${this.subzoneId}' could not be found.`,
		};
	}
};