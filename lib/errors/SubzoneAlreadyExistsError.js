const ApiError = require("./ApiError");

module.exports = class SubzoneAlreadyExistsError extends ApiError{
	constructor(name, ...args){
		super(...args);
		this.name = name;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 409;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "SubzoneAlreadyExistsError",
			message: `Subzone with name '${this.name}' already exists.`,
		};
	}
};