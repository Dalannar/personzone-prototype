const ApiError = require("./ApiError");

module.exports = class InvalidDobError extends ApiError{
	constructor(dob, ...args){
		super(...args);
		this.dob = dob;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 400;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "InvalidDobError",
			message: `Date of birth '${this.dob}' is not a valid date of birth.`,
		};
	}
};