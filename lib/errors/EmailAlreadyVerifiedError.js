const ApiError = require("./ApiError");

module.exports = class EmailAlreadyVerifiedError extends ApiError{
	/**
	 * @override
	 */
	getResponseStatus(){
		return 403;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "EmailAlreadyVerifiedError",
			message: "The email is already verified.",
		};
	}
};