const winston = require("winston");
const ExtendableError = require("es6-error");

module.exports = class ApiError extends ExtendableError{

	/**
	 * Get the response code that should be sent when encountering this error
	 * @returns {Number} A code to response to the client with
	 */
	getResponseStatus(){
		winston.warn("ApiError is being called directly. Make sure 'getResponseStatus' is overwritten by extending errors.");
		return 500;
	}

	/**
	 * Get the response object that should be sent when encountering this error
	 * @returns {object} An object to respond to the client with
	 */
	getResponseObject(){
		winston.warn("ApiError is being called directly. Make sure 'getResponseObject' is overwritten by extending errors.");
		return {
			error: "UndefinedError",
			message: "An undefined error has occured.",
		};
	}

	/**
	 * Get the response object that should be sent when encountering this error, and the server is in dev mode
	 * @return {object} An object to respond to the client with
	 */
	getDevResponseObject(){
		return Object.assign(this.getResponseObject(), {
			dev: {
				name: this.name,
				message: this.message,
				stack: this.stack,
			},
		});
	}

	/**
	 * @returns {String} the log level that should be used when logging this error through winston
	 */
	getWinstonLogLevel(){
		return "debug";
	}
};