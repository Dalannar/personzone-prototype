module.exports = {
	EmailAlreadyExistsError: require("./EmailAlreadyExistsError"),
	EmailAlreadyVerifiedError: require("./EmailAlreadyVerifiedError"),
	IncorrectHashError: require("./IncorrectHashError"),
	InvalidDobError: require("./InvalidDobError"),
	LocaleNotFoundError: require("./LocaleNotFoundError"),
	PurlAlreadyExistsError: require("./PurlAlreadyExistsError"),
	SubzoneAlreadyExistsError: require("./SubzoneAlreadyExistsError"),
	SubzoneNotFoundError: require("./SubzoneNotFoundError"),
	TraitNotFoundError: require("./TraitNotFoundError"),
	UnauthorizedError: require("./UnauthorizedError"),
	UserNotFoundError: require("./UserNotFoundError"),
	ZoneAlreadyExistsError: require("./ZoneAlreadyExistsError"),
	ZoneNotFoundError: require("./ZoneNotFoundError"),
	ZonetraitAlreadyExistsError: require("./ZonetraitAlreadyExistsError"),
	ZonetraitNotFoundError: require("./ZonetraitNotFoundError"),
	CommentNotFoundError: require("./CommentNotFoundError")
};