const ApiError = require("./ApiError");

module.exports = class ZonetraitAlreadyExists extends ApiError{
	constructor(traitId, ...args){
		super(...args);
		this.traitId = traitId;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 409;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "ZonetraitAlreadyExists",
			message: `Zonetrait for trait '${this.traitId}' already exists.`,
		};
	}
};