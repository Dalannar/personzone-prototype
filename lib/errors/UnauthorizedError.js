const ApiError = require("./ApiError");

module.exports = class UnauthorizedError extends ApiError{
	/**
	 * @override
	 */
	getResponseStatus(){
		return 401;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "UnauthorizedError",
			message: "Unauthorized request.",
		};
	}
};