const ApiError = require("./ApiError");

module.exports = class UserNotFoundError extends ApiError{
	constructor(purl, ...args){
		super(...args);
		this.purl = purl;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 404;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "UserNotFoundError",
			message: `User with purl '${this.purl}' could not be found.`,
		};
	}
};