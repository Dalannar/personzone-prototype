const ApiError = require("./ApiError");

module.exports = class CommentNotFoundError extends ApiError{
	constructor(id, ...args){
		super(...args);
		this.id = id;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 404;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "CommentNotFoundError",
			message: `Comment with id '${this.id}' could not be found.`,
		};
	}
};