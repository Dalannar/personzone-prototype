const ApiError = require("./ApiError");

module.exports = class IncorrectHashError extends ApiError{
	constructor(hash, ...args){
		super(...args);
		this.hash = hash;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 422;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "IncorrectHashError",
			message: `Hash '${this.hash}' is not correct.`,
		};
	}
};