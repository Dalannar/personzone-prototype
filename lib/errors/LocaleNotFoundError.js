const ApiError = require("./ApiError");

module.exports = class LocaleNotFoundError extends ApiError{
	constructor(code, ...args){
		super(...args);
		this.code = code;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 404;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "LocaleNotFoundError",
			message: `Locale with code '${this.code}' could not be found.`,
		};
	}
};