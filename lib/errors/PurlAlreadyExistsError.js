const ApiError = require("./ApiError");

module.exports = class PurlAlreadyExistsError extends ApiError{
	constructor(purl, ...args){
		super(...args);
		this.purl = purl;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 409;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "PurlAlreadyExistsError",
			message: `Purl '${this.purl}' already exists.`,
		};
	}
};