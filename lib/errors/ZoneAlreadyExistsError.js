const ApiError = require("./ApiError");

module.exports = class ZoneAlreadyExistsError extends ApiError{
	constructor(zurl, ...args){
		super(...args);
		this.zurl = zurl;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 409;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "ZoneAlreadyExistsError",
			message: `Zone with zurl '${this.zurl}' already exists.`,
		};
	}
};