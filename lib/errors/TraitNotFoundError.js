const ApiError = require("./ApiError");

module.exports = class TraitNotFoundError extends ApiError{
	/**
	 * TraitNotFoundError
	 * Thrown when a trait could not be found
	 * @param {Number|Null} id The id of the trait, or null if not applicable (client did not use id)
	 * @param {String|Null} name The name of the trait, or null if not application (client did not use name) 
	 * @param {*} args Other arguments used for superclass ApiError 
	 */
	constructor(id, name, ...args){
		super(...args);
		this.id = id;
		this.name = name;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 404;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "TraitNotFoundError",
			message: `Trait with [id=${this.id};name=${this.name}] could not be found.`,
		};
	}
};