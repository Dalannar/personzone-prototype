const ApiError = require("./ApiError");

module.exports = class ZonetraitNotFoundError extends ApiError{
	constructor(zonetraitId, ...args){
		super(...args);
		this.zonetraitId = zonetraitId;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 404;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "ZonetraitNotFoundError",
			message: `Zonetrait with id '${this.zonetraitId}' could not be found.`,
		};
	}
};