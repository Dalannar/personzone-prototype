const ApiError = require("./ApiError");

module.exports = class EmailAlreadyExistsError extends ApiError{
	constructor(email, ...args){
		super(...args);
		this.email = email;
	}

	/**
	 * @override
	 */
	getResponseStatus(){
		return 409;
	}

	/**
	 * @override
	 */
	getResponseObject(){
		return {
			error: "EmailAlreadyExistsError",
			message: `Email '${this.email}' already exists.`,
		};
	}
};