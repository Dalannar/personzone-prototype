const winston = require("winston");
const ApiError = require("../../errors/ApiError");

/**
 * An error handling middleware
 *
 * @param {object} [config] The configuration object for the error handler middleware
 * @param {bool} [config.printStackTrace=false] If set to true errors will return a stack trace to the client
 * @param {bool} [config.printValidationErrors=false] If set to true, validation errors will returns the validation parameters that failed to the client
 * @param {bool} [config.logErrorsToConsole=false] If set to true will use winston to log the errors to the console.
 *                                                Validation errors are logged on .debug() and everything else on .error()
 */
module.exports = function(config){
	return function(err, req, res, next){
		let toSend = {};
		let status = err.status || 500;

		if(err instanceof ApiError){
			status = err.getResponseStatus();
			toSend = config.printStackTrace ? err.getDevResponseObject() : err.getResponseObject();

			if(config.logErrorsToConsole)
				winston.log(err.getWinstonLogLevel(), err.getDevResponseObject());
		
		}else if(err.message === "Validation failed"){
			status = 400;
			if(config.printValidationErrors)
				toSend.validationErrors = err.array();

			if(config.logErrorsToConsole){
				winston.debug(err);
				winston.debug(err.array());
			}

		}else{
			if(config.printStackTrace){
				toSend.name = err.name;
				toSend.message = err.message;
				if(err.stack)
					toSend.stack = err.stack.split("\n");
			}

			if(config.logErrorsToConsole)
				winston.error(err);
		}

		// Send only if it hasn't been sent already in some other part of the chain
		if(!res.headersSent)
			res.status(status).send(toSend);
		
		return next();
	};
};