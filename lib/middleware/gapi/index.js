const googleMaps = require("@google/maps");

class Gapi {
	constructor(gapi, config){
		this.gapi = gapi;
		this.config = config;
	}

	place(options){
		return new Promise((resolve, reject) => {
			this.gapi.place(options, (err, response) => {
				if(err) return reject(err);
				return resolve(response);
			});
		});
	}

	placesAutoComplete(options){
		return new Promise((resolve, reject) => {
			this.gapi.placesAutoComplete(options, (err, response) => {
				if(err) return reject(err);
				return resolve(response);
			});
		});
	}
}

/**
 * @param {Object} config Configuration object
 * @param {String} config.API_KEY The google API key to use
 */
module.exports = function(config){
	const gClient = googleMaps.createClient({
		key: config.API_KEY
	});

	const gapi = new Gapi(gClient, config);

	return function(req, res, next){
		req.gapi = gapi;
		return next();
	};
};