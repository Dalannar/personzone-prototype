const winston = require("winston");
const Avatarer = require("./Avatarer");

/**
 * 
 * @param {object} config The config object for the Avatarer
 * @param {string} config.userAvatarPath The path to the directory to save user avatars in
 */
module.exports = function(config){
	return function(req, res, next){
		req.avatarer = new Avatarer(config);
		return next();
	};
};