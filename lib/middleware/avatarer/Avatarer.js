const mv = require("mv");
const fs = require("fs");
const path = require("path");
const jimp = require("jimp");
const winston = require("winston");

const FILE_EXT = "jpg";

module.exports = class Avatarer{
	/**
	 * @param {Object} config The config object for the Avatarer
	 * @param {String} config.userAvatarPath The path to save new user avatar images under
	 * @param {String} config.placeholderAvatarPath The path to a placeholder image in case a user does not have an avatar
	 */
	constructor(config){
		this.config = config;
	}

	/**
	 * Get the location of the user's avatar, otherwise gets the location of a placeholder image
	 * @param {String} purl The user's personal URL
	 * @returns {String} The location of the file corresponding to the user's avatar
	 */
	getUserAvatar(purl){
		let p = path.resolve(this.config.userAvatarPath, `${purl}.${FILE_EXT}`);
		if(!fs.existsSync(p))
			p = path.resolve(this.config.placeholderAvatarPath, `placeholder.${FILE_EXT}`);

		return p;
	}

	/**
	 * @param {File} file The file of the image to process
	 * @param {String} name The name of the file to save the image as
	 */
	processUserAvatar(file, name){
		let fName = path.resolve(this.config.userAvatarPath, `${name}.${FILE_EXT}`);
		mv(file.path, fName, {}, function(err){
			if(err) winston.error(err);

			jimp.read(fName, function(err, avatar){
				if(err) winston.error(err);

				avatar.resize(240, 240)
					.write(fName, function(err){
						if(err) winston.error(err);
					});
			});
		});
	}
};