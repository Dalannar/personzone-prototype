const nodemailer = require("nodemailer");
const _ = require("lodash");
const winston = require("winston");

/**
 * A wrapper for the nodemailer transport object
 */
class Mailer {
	constructor(config, defaults){
		this.mailer = nodemailer.createTransport(config, defaults);
		this.templates = require("./templates");
	}

	/**
	 * Send an email based on a template
	 * @param {string} template The name of the template to use
	 * @param {object} templateParams The parameters to pass onto the template
	 * @param {object} data The data to use when sending the email. This object should follow the schema set by nodemailer.sendMail
	 * @param {func} [cb] A callback for when the mail is sent. This function should follow the schema set by nodemailer.sendMail
	 * @returns {Promise} Returns a Promise that will only fulfill/reject if cb is not passed
	 */
	sendTemplate(template, templateParams, data, cb){
		return new Promise(function(fulfill, reject){
			// The template does not exist
			if(!this.templates[template]){
				let err = new ReferenceError(`Could not resolve template '${template}'`);
				return _.isFunction(cb) ? cb(err) : reject(err);
			}

			let t = this.templates[template](templateParams);
			let d = _.merge(t, data);

			let mailerPromise = this.mailer.sendMail(d, cb);

			// Because 'reason' if we provide a function in cb nodemailer decides to not return a Promise anymore
			// that's why we need to test if it's a Promise before attaching .then()
			if(mailerPromise instanceof Promise)
				return mailerPromise.then(fulfill, reject);

			return mailerPromise;
		}.bind(this));
	}

	// Exposing methods from the original nodemailer object
	verify(cb){ return this.mailer.verify(cb) }
	sendMail(data, cb){ return this.mailer.sendMail(data, cb) }
}

/**
 * Middleware for the email processing
 *
 * @param {object} config The config object passed onto the nodemailer object
 * @param {object} [defaults] The defaults object passed onto the nodemailer object
 */
module.exports = function(config, defaults){
	let mailer = new Mailer(config, defaults);
	mailer.verify(function(err, success){
		if(err){
			winston.error(err);
		}else{
			winston.info("Mailer system running");
		}
	});

	return function(req, res, next){
		req.mailer = mailer;
		next();
	};
};