const _ = require("lodash");

module.exports = function({ purl, hash }){
	if(_.isNil(hash))
		throw new Error("Hash parameter was not provided");

	return {
		subject: "Email verification",
		text: `Thank you for registering with personzone. Click the link to verify your email <http://localhost:3000/api/users/${purl}/verify-email/${hash}>
			.\n\nDISCLAIMER: Personzone is an upcoming web application currently in the first stages of being built.
			If you have received this email and have no idea of what personzone is then please disregard it.
			This stupid developer humbly apoloziges for being a nuisance.`,
	}
}