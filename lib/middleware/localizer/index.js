const fs = require("fs");
const path = require("path");

class Localizer {

	/**
	 * @param {Object} config Configuration options for the localizer middleware
	 * @param {String} config.localeDir The directory to look for available locale files
	 * @param {String} [config.defaultLocale="en"] The default locale to use
	 * 
	 * @returns {Localizer} this
	 */
	constructor(config={}){
		if(!config.localeDir || typeof config.localeDir !== "string")
			throw new TypeError(`Invalid locale directory supplied, ${config.localeDir}`);

		this.localeDir = config.localeDir;
		this.defaultLocale = config.defaultLocale || "en";

		return this;
	}

	/**
	 * Get the locale object
	 * @param {String} code The locale to load
	 * 
	 * @returns {Object} an object capable of initializing polyglot, example = {locale: "en", phrases: {...}}
	 */
	_getLocale(code){
		const dir = path.join(this.localeDir, code);
		let locale = {locale: code, phrases: {}};

		fs.readdirSync(dir).forEach(function(fname){
			locale.phrases[fname.replace(/\.[^/.]+$/, "")] =
				require(path.join(dir, fname));
		});

		return locale;
	}

	/**
	 * Returns a list of all the available locales
	 * A locale is available if it exists as a directory in the locale directory specified during construction
	 * @returns {String[]} an array with the codes of all available locales in the system
	 */
	getAvailableLocales(){
		const dir = this.localeDir;
		return fs.readdirSync(dir).filter(function(fname){
			return fs.lstatSync(path.join(dir, fname)).isDirectory();
		});
	}

	/**
     * Get the phrases object for the relevant locale. If 'locale' cookie is set uses that,
     * otherwise looks at 'accept-language' HTTP header.
     * If no locale is found uses the default locale
     * @param {object} req The request object
	 * 
	 * @returns {Object} the loaded phrases for the specified locale
     */
	getLocale(req){
		const _this = this;
		const availableLocales = this.getAvailableLocales();

		// Get the locale that is in the cookie if there is one
		if(req.cookies["locale"]){
			return this._getLocale(req.cookies["locale"]);
		}

		// There is no locale cookie, see if there is an accept-language
		let clientAccept = require("accept-language-parser").parse(req.headers["accept-language"]);
		clientAccept.forEach(function(o){
			if(availableLocales.indexOf(o.code) !== -1)
				return _this._getLocale(o.code);
		});

		// Send the default locale because we can't find another language
		return this._getLocale(this.defaultLocale);
	}

	/**
     * Sets the locale cookie
	 * @param {Object} req The request object
	 * @param {Object} res The response object
	 * @param {String} code The locale to set
	 * 
	 * @throws {ReferenceError} if the specified code is not an available locale
     */
	setLocale(req, res, code){
		if(this.getAvailableLocales().indexOf(code) === -1)
			throw new ReferenceError(`Locale with code ${code} does not exist`);
        
		res.cookie("locale", code);
	}

	/**
     * Clears the locale cookie
     * @param {object} res The response object
     */
	clearLocale(res){
		res.cookie("locale", null);
	}
}

module.exports = function(config){
	const localizer = new Localizer(config);
	return function(req, res, next){
		req.localizer = {
			getLocale: () => {return localizer.getLocale(req);},
			setLocale: (localeCode) => {return localizer.setLocale(req, res, localeCode);},
			clearLocale: () => {return localizer.clearLocale(res);},
			getAvailableLocales: localizer.getAvailableLocales,
		};
		next();
	};
};