const fs = require("fs");
const _ = require("lodash");

class I18n{
	/**
     * 
     * @param {object} config The configuration object
     * @param {string} config.localeDir The directory holding the locales
     * @param {string} [config.defaultLocale="en"] The default locale to use. Defaults to 'en'
     */
	constructor(config){
		if(!_.isString(config.localeDir))
			throw new TypeError("localeDir not provided");

		this.localeDir = config.localeDir;
		this.defaultLocale = config.defaultLocale || "en";
	}

	/**
     * Get the object of the specified locale
     * @param {string} locale The locale to get
     */
	_getLocale(locale){
		return require(`${this.localeDir}/${locale}`);
	}

	/**
     * Get a list of the available locales
     */
	getAvailableLocales(){
		return fs.readdirSync(this.localeDir);
	}

	/**
     * Get the locale object relevant. If 'locale' cookie is set uses that,
     * otherwise looks at 'accept-language' HTTP header.
     * If no locale is found uses the default locale
     * @param {object} req The request object
     */
	getLocale(req){
		const availableLocales = this.getAvailableLocales();

		// Get the locale that is in the cookie if there is one
		if(req.cookies["locale"]){
			if(_.indexOf(availableLocales, req.cookies["locale"]) !== -1){
				return this._getLocale(req.cookies["locale"]);
			}
		}

		// There is no locale cookie, see if there is an accept-language
		let clientAccept = _.map(_.split(req.headers["accept-language"], ","), o => o.substring(0, 2));
		let locale;
		_.forEach(clientAccept, function(o){
			if(_.indexOf(availableLocales, o) !== -1){
				locale = o;
				// Stop the loop
				return false;
			}
		});

		if(!locale)
			locale = this.defaultLocale;
        
		return this._getLocale(locale);
	}

	/**
     * Sets the locale cookie and returns the object corresponding to that locale
     * If the locale does not exist throws a reference error
     * @param {object} req The request object
     * @param {object} res The response object
     * @param {string} locale The locale to set
     */
	setLocale(req, res, locale){
		if(_.indexOf(this.getAvailableLocales(), locale) === -1)
			throw new ReferenceError(`Locale ${locale} does not exist`);
        
		res.cookie("locale", locale);
		return this._getLocale(locale);
	}

	/**
     * Clears the locale cookie
     * @param {object} res The response object
     */
	clearLocale(res){
		res.cookie("locale", null);
	}
}

module.exports = function(config){
	const i18n = new I18n(config);
	return function(req, res, next){
		req.i18n = {
			getLocale: () => {return i18n.getLocale(req);},
			setLocale: (locale) => {return i18n.setLocale(req, res, locale);},
			clearLocale: () => {return i18n.clearLocale(res);},
			getAvailableLocales: i18n.getAvailableLocales,
		};
		next();
	};
};