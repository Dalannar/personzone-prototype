module.exports = {
	getUserByPurl,
	getLatestUsers
};

/**
 * Get a user by purl. Includes the user's auth and the user's zones
 * @param {Sequelize} db The database object
 * @param {String} purl The personal url of the user
 * @returns {Promise} sequelize promise
 */
function getUserByPurl(db, purl){
	return db.models.user.findOne({
		where: { purl },
		include: [ db.models.auth, db.models.zone ]
	});
}

/**
 * Get an array of the most recently registered users
 * @param {Sequelize} db The database object
 * @param {Number} [limit=24] The number of results to get
 * @returns {Promise} sequelize promise
 */
function getLatestUsers(db, limit){
	return db.models.user.findAll({
		limit: limit || 24,
		order: [["created_at", "desc"]]
	});
}