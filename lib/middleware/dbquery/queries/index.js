const _ = require("lodash");

module.exports = _.assign(
	{},
	require("./zones"),
	require("./zonetraits"),
	require("./users"),
	require("./subzones"),
	require("./rankings"),
	require("./comments")
);