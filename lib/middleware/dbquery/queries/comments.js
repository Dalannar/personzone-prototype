module.exports = {
	getFeedbackComments,
};

/**
 * Get a page of comments received by a user identified by receiver_id
 * @param {Sequelize} db The database object
 * @param {Number} receiver_id The id of the user receiving the comment
 * @param {Number} limit The limit on the number of results to return
 * @param {Number} page The page of results to return
 * @param {String} order The order to return
 * @param {Object} options
 * @param {Number} [options.zone] Filter only to comments on the zone with the specified id
 * @param {Number} [options.subzone] Filter only to comments on the subzone with the specified id
 * @param {String} [options.thanked="all"] Possible values: "all", "true" or "false". Specified whether the returns comments need to have been thanked, not thanked or all
 * 
 * @returns {Promise} sequelize promise
 */
function getFeedbackComments(db, receiver_id, limit, page, order, options={}){
	return db.models.comment.findAndCount({
		where: (options.thanked === "all" || options.thanked === null) ? {} : {thanked: (options.thanked === "true")},
		include: [{
			model: db.models.subzone,
			where: options.subzone ? { id: options.subzone } : {},
			required: true,
			include: [{
				model: db.models.zone,
				where: options.zone ? { user_id: receiver_id, id: options.zone } : { user_id: receiver_id }
			}]
		}],
		limit,
		offset: limit * page,
		order: [
			["created_at", order]
		]
	});
}