module.exports = {
	getZoneSubzones
};

/**
 * Get subzones belonging to a specific zone
 * @param {Sequelize} db The database object
 * @param {Number} zone_id The id of the zone to get subzones for
 * @returns {Promise} sequelize promise
 */
function getZoneSubzones(db, zone_id){
	return db.models.subzone.findAll({
		where: { zone_id },
	});
}