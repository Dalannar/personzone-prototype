module.exports = {
	getZoneById,
	getZoneByUserIdZurl,
};

/**
 * Get a zone. Includes a list of the subzones and zonetraits (+ traits)
 * @param {Sequelize} db The database object
 * @param {Number} id The id of the zone
 * @returns {Promise} sequelize promise
 */
function getZoneById(db, id){
	return db.models.zone.findById(id, {
		include: [ db.models.subzone, {
			model: db.models.zonetrait,
			include: [ db.models.trait ],
		}],
	});
}

/**
 * Get a zone by user id and zurl. Includes a list of the subzones and zonetraits (+ traits)
 * @param {Sequelize} db The database object
 * @param {String} zurl The url of the zone
 * @param {Number} user_id The id of the user that owns the zone
 * @returns {Promise} sequelize promise
 */
function getZoneByUserIdZurl(db, user_id, zurl){
	return db.models.zone.findOne({
		where: { zurl, user_id },
		include: [ db.models.subzone, {
			model: db.models.zonetrait,
			include: [ db.models.trait ],
		}],
	});
}