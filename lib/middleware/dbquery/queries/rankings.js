const _ = require("lodash");

module.exports = {
	getZonetraitRankings,
};

function getZonetraitRankings(db, {
	limit=10,
	page=0,
	order="desc",
	trait_id,
	location,
}={}){
	const locationWhere = _.assign({}, location, { type: "location"} );

	return new Promise(async (resolve, reject) => {
		const countPromise = db.models.zonetrait.count({
			where: trait_id ? { trait_id } : {},
			include: [{
				model: db.models.zone,
				required: true,
				include: [{
					model: db.models.user,
					required: true,
					include: [{
						model: db.models.location,
						required: !_.isEmpty(location),
						where: locationWhere
					}]
				}]
			}]
		});
	
		const searchPromise = db.models.zonetrait.findAll({
			subQuery: false,
			where: trait_id ? { trait_id } : {},
			attributes: {
				include: [
					[db.fn("ROUND", db.col("average_rating")), "round_average_rating"]
				]
			},
			include: [db.models.trait, {
				model: db.models.zone,
				required: true,
				include: [{
					model: db.models.user,
					required: true,
					include: [{
						model: db.models.location,
						required: !_.isEmpty(location),
						where: locationWhere
					}]
				}]
			}],
			limit,
			offset: limit * page,
			order: [
				[db.literal("round_average_rating"), order],
				["number_ratings", order],
				["average_rating", order],
				["zone", "user", "name", order],
				["id", order]
			],
		});

		const [count, rows] = await Promise.all([countPromise, searchPromise]).catch(reject);
		resolve({count, rows});
	});
}

/**
 * Get a list of zonetrait rankings
 * @param {Sequelize} db The database object
 * @param {Number} limit The number of results to return
 * @param {Number} page The page of results to return
 * @param {"ASC"|"DESC"} order The order or results to get
 * @param {Object} [options] Options for the search
 * @param {Number|null} [options.trait_id=null] The id of the trait to rank zonetraits by. If null will search for all traits
 * @param {String|null} [options.gender=null] The gender of the users to rank. If null will search for all users regardless of gender
 * @param {String|null} [options.find_purl=null] If set will ignore the given page and return the page that contains the first instance of the given purl
 * @returns {Promise} sequelize promise
 */
function _getZonetraitRankings(db, limit, page, order, options={}){
	options = _.assign({}, options);
	
	const countPromise = db.models.zonetrait.count({
		where: options.trait_id ? { trait_id: options.trait_id } : {},
		include: options.gender ? [{
			model: db.models.zone,
			required: true,
			include: [{
				model: db.models.user,
				where: { gender: options.gender }
			}]
		}] : []
	});

	const searchPromise = db.models.zonetrait.findAll({
		subQuery: false,
		attributes: {
			include: [
				[db.fn("AVG", db.col("ratings.rating")), "final_rating"],
				[db.fn("COUNT", db.col("ratings.rating")), "total_ratings"],
			],
		},
		where: options.trait_id ? { trait_id: options.trait_id } : {},
		include: [db.models.trait, {
			model: db.models.rating,
			attributes: [],
		},{
			model: db.models.zone,
			required: true,
			include: [{
				model: db.models.user,
				where: options.gender ? { gender: options.gender } : {},
			}],
		}],
		// Set limit only if we aren't supposed to find a particular record
		limit: options.find_purl ? null : limit,
		// Set offset only if we aren't supposed to find a particular record
		offset: options.find_purl ? null : limit * page,
		order: [
			// Order by the score first
			["score", order], 
			// Then alphabetically
			["zone", "user", "name", order],
			// And finally by ID because there's nothing else to order by
			["id", order]
		],
		group: "zonetrait.id",
	});

	return new Promise(function(resolve, reject){
		Promise.all([
			countPromise,
			searchPromise
		]).then(function(results){
			const count = results[0];
			const rows = results[1];

			if(options.find_purl){
				const index = _.findIndex(rows, function(row){
					return row.get("zone").get("user").get("purl") === options.find_purl;
				});

				const page = Math.floor(index/limit);
				return resolve({count, page, rows: rows.slice(page * limit, (page + 1) * limit)});
			}

			return resolve({count: results[0], rows: results[1]});
		}).catch(reject);
	});
}