module.exports = {
	getZoneZonetraits,
	getZoneZonetraitsUserRating,
	getZoneZonetraitsFinalRating,
};

/**
 * Get a list of zonetraits for a given zone
 * @param {Sequelize} db The database object
 * @param {Number} zone_id The id of the zone to get zonetraits for
 * @returns {Promise} sequelize promise
 */
function getZoneZonetraits(db, zone_id){
	return db.models.zonetrait.findAll({
		where: { zone_id },
		include: [ db.models.trait ],
	});
}

/**
 * Get a list of zonetraits for a given zone, and including information about a specific user's rating, if exists
 * @param {Sequelize} db The database object
 * @param {Number} zone_id The id of the zone to get zonetraits for
 * @param {Number} user_id The id of the user to get ratings of
 * @returns {Promise} sequelize promise
 */
function getZoneZonetraitsUserRating(db, zone_id, user_id){
	return db.models.zonetrait.findAll({
		where: { zone_id },
		include: [ db.models.trait, {
			model: db.models.rating,
			where: { user_id },
			required: false,
			limit: 1,
		}],
	});
}

/**
 * Get a list of zonetraits for a given zone, and including information about average rating and total number of ratings
 * @param {Sequelize} db The database object
 * @param {Number} zone_id The id of the zone to get zonetraits for
 * @returns {Promise} sequelize promise
 */
function getZoneZonetraitsFinalRating(db, zone_id){
	return db.models.zonetrait.findAll({
		attributes: {
			include: [
				[db.fn("AVG", db.col("ratings.rating")), "final_rating"],
				[db.fn("COUNT", db.col("ratings.rating")), "total_ratings"],
			],
		},
		where: { zone_id },
		include: [ db.models.trait, {
			model: db.models.rating,
			attributes: []
		}],
		group: "zonetrait.id",
	});
}