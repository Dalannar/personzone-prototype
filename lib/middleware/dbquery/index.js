module.exports = function(){
	return function(req, res, next){
		req.dbquery = require("./queries");
		return next();
	};
};