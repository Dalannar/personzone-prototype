const _ = require("lodash");
const crypto = require("crypto");
const uuidv1 = require("uuid/v1");

const Generator = require("./Generator");
const { generatePurl } = require("../../util/models");
const { weightedRNG } = require("../../util/math");

module.exports = class UserGenerator extends Generator{
	/**
	 * @constructor UserGenerator
	 * @param {Object} config The configuration object
	 * @param {Object} config.probabilities An object describing the probability values to use when generating users
	 * @param {Object} config.probabilities.gender An object describing the possible values and their probability for the gender to generate
	 */
	constructor(config){
		super(config);

		this.readData({
			maleFirstNames: this.config.parameters.male_first_names,
			femaleFirstNames: this.config.parameters.female_first_names,
			lastNames: this.config.parameters.last_names,
			defWords: this.config.parameters.defining_words
		});
	}

	/**
	 * @override
	 * @param {object} options
	 * @param {number} [options.id] The id to assign the user
	 * @param {number} [options.zone_id] The id to assign to the generated zones (will increase by 1 for each new zone)
	 * 									If assigned will also generate the user with 'default_zone'
	 * @param {number} [options.location_id] The id to assign to the generated locations (will increase by 1 for each new location)
	 * @param {boolean} [options.gen_auth=true] Whether to generate auth data
	 * @param {boolean} [options.gen_user_privacy=true] Whether to generate user_privacy data
	 * @param {boolean} [options.gen_zones=true] Whether to generate zones data
	 * @param {boolean} [options.gen_locations=true] Whether to generate locations data
	 */
	generate({
		id, zone_id, location_id,
		gen_auth=true,
		gen_user_privacy=true,
		gen_zones=true,
		gen_locations=true
	}={}){
		const gender = weightedRNG(this.config.probabilities.gender);
		const name = this.generateName(gender);
		const purl = generatePurl(name);
		const email = this.generateEmail(purl);
		const dob = weightedRNG(this.config.probabilities.dob) ? this.generateDob() : null;
		const blood_type = weightedRNG(this.config.probabilities.blood_type);
		const rel_status = weightedRNG(this.config.probabilities.rel_status);
		const driving_license = weightedRNG(this.config.probabilities.driving_license);
		const seeking_work = weightedRNG(this.config.probabilities.seeking_work);

		const def_word = weightedRNG(this.config.probabilities.def_word) ? this.generateDefWord() : null;

		let user = {
			name, purl, email, gender, dob,
			blood_type, rel_status, driving_license, seeking_work,
			def_word
		};
		
		if(_.isNumber(id))
			user.id = id;

		if(gen_auth)
			user.auth = this.generators.auth.generate({user_id: id});

		if(gen_user_privacy)
			user.user_privacy = this.generators.user_privacy.generate({user_id: id});

		if(gen_zones){
			user.zones = _.map(_.range(weightedRNG(this.config.probabilities.number_zones)), function(v, index){
				return this.generators.zone.generate({id: zone_id + index, user_id: id, personal: index === 0, no_personal: index !== 0});
			}.bind(this));
		}

		if(gen_locations){
			user.locations = _.map(_.range(weightedRNG(this.config.probabilities.number_locations)), function(v, index){
				return this.generators.location.generate({id: location_id + index});
			}.bind(this));
		}

		if(_.isNumber(zone_id))
			user.default_zone = zone_id;

		return user;
	}

	/**
	 * Generate an email based on a purl
	 * @param {String} purl The purl to generate the email based on
	 * @return {String} The generated email
	 */
	generateEmail(purl){
		const emailDomain = weightedRNG(this.config.probabilities.email_domain);
		return `${purl}@${emailDomain}`;
	}

	/**
	 * Generate a defining word
	 * @returns {string} The generated defining word
	 */
	generateDefWord(){
		return this.data.defWords[_.random(this.data.defWords.length-1)];
	}

	/**
	 * Generate a date of birth
	 * @returns {date} The generated dob
	 */
	generateDob(){
		const start = new Date(this.config.parameters.start_dob_year, 0, 1);
		const end = new Date(this.config.parameters.end_dob_year, 0, 1);
		return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime())).toISOString().split("T")[0];
	}

	/**
	 * Generate a name based on a gender
	 * @param {String} gender The gender to generate the name based on
	 * @returns {String} The generated name
	 */
	generateName(gender){
		let firstNames = [];
		let lastNames = this.data.lastNames;

		if(gender === "M")
			firstNames = this.data.maleFirstNames;
		else if(gender === "F")
			firstNames = this.data.femaleFirstNames;
		else
			firstNames = _.random() ? this.data.maleFirstNames : this.data.femaleFirstNames;

		const nFirstNames = weightedRNG(this.config.probabilities.number_first_names);
		const nLastNames = weightedRNG(this.config.probabilities.number_last_names);

		const firstName = _.join(_.map(_.range(nFirstNames), () => firstNames[_.random(firstNames.length-1)]), " ");
		const lastName = _.join(_.map(_.range(nLastNames), () => lastNames[_.random(lastNames.length-1)]), " ");

		return `${firstName} ${lastName}`;
	}
};