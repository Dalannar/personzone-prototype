const _ = require("lodash");
const Generator = require("./Generator");

const { weightedRNG } = require("../../util/math");

module.exports = class LocationGenerator extends Generator {
	/**
	 * @constructor LocationGenerator
	 * @param {Object} config
	 * @param {Object} config.probabilities
	 */
	constructor(config){
		super(config);

		this.readData({
			locations: this.config.parameters.locations
		});
	}

	/**
	 * @override
	 * @param {object} options
	 * @param {number} [options.id] The id to use
	 */
	generate({
		id
	}={}){
		let user_location = _.assign({}, this.generateLocationDetails());

		if(_.isNumber(id))
			user_location.id = id;

		return user_location;
	}

	/**
	 * Generate location details (includes point, locality, country, etc...)
	 * @returns {object} generated details
	 */
	generateLocationDetails(){
		return this.data.locations[_.random(this.data.locations.length-1)];
	}
};