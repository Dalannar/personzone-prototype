const _ = require("lodash");
const Generator = require("./Generator");

const { weightedRNG } = require("../../util/math");

module.exports = class UserPrivacyGenerator extends Generator {
	/**
	 * @constructor UserPrivacyGenerator
	 * @param {Object} config
	 * @param {Object} config.probabilities
	 * @param {Object} config.probabilities.email
	 * @param {Object} config.probabilities.join_date
	 * @param {Object} config.probabilities.gender
	 * @param {Object} config.probabilities.dob
	 * @param {Object} config.probabilities.blood_type
	 * @param {Object} config.probabilities.driving_license
	 * @param {Object} config.probabilities.seeking_work
	 * @param {Object} config.probabilities.rel_status
	 * @param {Object} config.probabilities.def_word
	 */
	constructor(config){
		super(config);
	}

	/**
	 * @override
	 * @param {object} [options]
	 * @param {number} [options.id] The id to use
	 * @param {number} [options.user_id] The user id to use
	 */
	generate({
		id, user_id
	}={}){
		const email = weightedRNG(this.config.probabilities.email);
		const join_date = weightedRNG(this.config.probabilities.join_date);
		const gender = weightedRNG(this.config.probabilities.gender);
		const dob = weightedRNG(this.config.probabilities.dob);
		const blood_type = weightedRNG(this.config.probabilities.blood_type);
		const driving_license = weightedRNG(this.config.probabilities.driving_license);
		const seeking_work = weightedRNG(this.config.probabilities.seeking_work);
		const rel_status = weightedRNG(this.config.probabilities.rel_status);
		const def_word = weightedRNG(this.config.probabilities.def_word);

		let privacy = {
			email, join_date, gender,
			dob, blood_type, driving_license,
			seeking_work, rel_status, def_word
		};

		if(_.isNumber(id))
			privacy.id = id;

		if(_.isNumber(user_id))
			privacy.user_id = user_id;

		return privacy;
	}
};