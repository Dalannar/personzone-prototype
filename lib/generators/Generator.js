const _ = require("lodash");

const { normalizeRNGWeights } = require("../../util/math");

module.exports = class Generator {
	/**
	 * @constructor Generator
	 * Base class for a data generator
	 * @param {Object} config The configuration object. If not supplied a default configuration should be created
	 */
	constructor(config){
		this.config = config;
		this.config.probabilities = _.mapValues(this.config.probabilities, normalizeRNGWeights);
	}

	/**
	 * Set connections to all other generators
	 * @param {object} generators generatorName => Generator 
	 */
	setGenerators(generators){
		this.generators = generators;
	}

	/**
	 * 'require' data necessary for generation (list of names to use, etc)
	 * @param {object} [paths={}] Mapping of data name to name (or list of names) of files to require
	 * @returns {object} this.data (data name => data that was 'require'd) 
	 */
	readData(paths={}){
		this.data = _.mapValues(paths, function(path){
			if(_.isArray(path))
				return _.flatten(_.map(path, require));
			
			return require(path);
		});

		return this.data;
	}

	/**
	 * Generate a single result of data, given an id
	 * This should be overriden by any subclass of Generator
	 * @returns {*} The generated piece of data
	 */
	generate(){
		return {};
	}
};