const _ = require("lodash");
const Generator = require("./Generator");

const { weightedRNG } = require("../../util/math");

module.exports = class AuthGenerator extends Generator {
	/**
	 * @constructor AuthGenerator
	 * @param {Object} config
	 * @param {Object} config.probabilities
	 * @param {Object} config.probabilities.email_verified
	 */
	constructor(config){
		super(config);
	}

	/**
	 * @override
	 * @param {object} options
	 * @param {number} [options.id] The id to use
	 * @param {number} [options.user_id] The user id to use
	 */
	generate({
		id, user_id
	}={}){
		const email_verified = weightedRNG(this.config.probabilities.email_verified);
		const password = this.config.parameters.password;

		let auth = {
			email_verified, password
		};

		if(_.isNumber(id))
			auth.id = id;

		if(_.isNumber(user_id))
			auth.user_id = user_id;

		return auth;
	}
};