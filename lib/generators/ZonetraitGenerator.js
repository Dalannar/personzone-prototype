const _ = require("lodash");
const Generator = require("./Generator");

module.exports = class ZonetraitGenerator extends Generator {
	/**
	 * @constructor ZonetraitGenerator
	 * @param {Object} config
	 * @param {Object} config.probabilities
	 */
	constructor(config){
		super(config);
	}

	/**
	 * @override
	 * @param {object} options
	 * @param {number} [options.id] The id to use
	 * @param {number} [options.zone_id] The zone id to use
	 * @param {number[]} [options.trait_ids] An array of usable trait ids
	 */
	generate({
		id, zone_id,
		trait_ids
	}={}){
		if(_.isEmpty(trait_ids))
			return null;

		const trait_id = trait_ids[_.random(trait_ids.length-1)];
		let zonetrait = {
			trait_id
		};

		if(_.isNumber(id))
			zonetrait.id = id;

		if(_.isNumber(zone_id))
			zonetrait.zone_id = zone_id;

		return zonetrait;
	}
};