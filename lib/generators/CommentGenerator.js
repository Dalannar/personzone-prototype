const _ = require("lodash");
const Generator = require("./Generator");

const { weightedRNG } = require("../../util/math");

module.exports = class CommentGenerator extends Generator {
	/**
	 * @constructor AuthGenerator
	 * @param {Object} config
	 * @param {object} config.parameters
	 * @param {string[]} config.parameters.comments
	 * @param {Object} config.probabilities
	 * @param {Object} config.probabilities.thanked %true% of %false% probabilities
	 */
	constructor(config){
		super(config);

		this.readData({
			comments: this.config.parameters.comments
		});
	}

	/**
	 * @override
	 * @param {object} options
	 * @param {number} [options.id] The id to use
	 * @param {number} [options.subzone_id] The user id to use
	 * @param {number[]} [options.user_ids] An array of user ids to use as the commenter
	 */
	generate({
		id, subzone_id,
		user_ids
	}={}){
		if(_.isEmpty(user_ids))
			return null;

		const comment = this.data.comments[_.random(this.data.comments.length-1)];
		const thanked = weightedRNG(this.config.probabilities.thanked);
		const anonymous = weightedRNG(this.config.probabilities.anonymous);

		let c = {
			comment,
			thanked,
			anonymous
		};

		if(_.isNumber(id))
			c.id = id;

		if(_.isNumber(subzone_id))
			c.subzone_id = subzone_id;

		return c;
	}
};