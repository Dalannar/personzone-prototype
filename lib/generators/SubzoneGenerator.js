const _ = require("lodash");
const Generator = require("./Generator");

const { weightedRNG } = require("../../util/math");

module.exports = class SubzoneGenerator extends Generator {
	/**
	 * @constructor SubzoneGenerator
	 * @param {Object} config
	 * @param {Object} config.probabilities
	 * @param {Object} config.probabilities.email_verified
	 */
	constructor(config){
		super(config);

		this.readData({
			subzoneNames: this.config.parameters.subzone_names
		});
	}

	/**
	 * @override
	 * @param {object} options
	 * @param {number} [options.id] The id to use
	 * @param {number} [options.zone_id] The zone id to use
	 * @param {string[]} [options.used_names] An array of names that should not be used
	 */
	generate({
		id, zone_id,
		used_names
	}={}){
		const name = this.generateName(used_names);

		let subzone = {
			name
		};

		if(_.isNumber(id))
			subzone.id = id;

		if(_.isNumber(zone_id))
			subzone.zone_id = zone_id;

		return subzone;
	}

	/**
	 * Generate a name
	 * @param {String[]} [usedNames=[]] An array of names that should not be used
	 * @returns {String} The generated name
	 */
	generateName(usedNames=[]){
		const usableNames = _.without(this.data.subzoneNames, ...usedNames);
		return usableNames[_.random(usableNames.length-1)];
	}
};