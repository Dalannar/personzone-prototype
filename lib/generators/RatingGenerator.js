const _ = require("lodash");
const Generator = require("./Generator");

module.exports = class RatingGenerator extends Generator {
	/**
	 * @constructor ZonetraitGenerator
	 * @param {Object} config
	 * @param {Object} config.probabilities
	 */
	constructor(config){
		super(config);
	}

	/**
	 * @override
	 * @param {object} options
	 * @param {number} [options.id] The id to use
	 * @param {number} [options.zonetrait_id] The zonetrait id to use
	 * @param {number[]} [options.user_ids] An array of usable user ids
	 */
	generate({
		id, zonetrait_id,
		user_ids
	}={}){
		if(_.isEmpty(user_ids))
			return null;

		const rating = _.random(1, 5);
		const user_id = user_ids[_.random(user_ids.length-1)];
		let r = {
			user_id,
			rating,
		};

		if(_.isNumber(id))
			r.id = id;

		if(_.isNumber(zonetrait_id))
			r.zonetrait_id = zonetrait_id;

		return r;
	}
};