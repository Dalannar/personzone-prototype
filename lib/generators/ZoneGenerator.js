const _ = require("lodash");
const Generator = require("./Generator");

const { weightedRNG } = require("../../util/math");
const { generateZurl } = require("../../util/models");

module.exports = class ZoneGenerator extends Generator {
	/**
	 * @constructor ZoneGenerator
	 * @param {Object} config
	 * @param {Object} config.probabilities
	 * @param {Object} config.probabilities.type
	 */
	constructor(config){
		super(config);
		this.zone_names = _.flatten(_.map(this.config.parameters.zone_names, require));
	}

	/**
	 * @override
	 * @param {object} [options]
	 * @param {number} [options.id] The id to use
	 * @param {number} [options.user_id] The user id to use
	 * @param {boolean} [options.personal=false] If true will only generate 'personal' zones
	 * @param {boolean} [options.no_personal=false] If true will not generate 'personal' zones
	 */
	generate({
		id, user_id,
		personal=false,
		no_personal=false,
	}={}){
		const typeProbabilities = no_personal ? 
			_.filter(this.config.probabilities.type, (arr) => arr[0] !== "personal") :
			this.config.probabilities.type;

		const type = personal ? "personal" : weightedRNG(typeProbabilities);
		const name = this.generateName(type);
		const zurl = generateZurl(name);

		let zone = {
			type, name, zurl
		};

		if(_.isNumber(id))
			zone.id = id;

		if(_.isNumber(user_id))
			zone.user_id = user_id;

		return zone;
	}

	/**
	 * Generate a name based on a type of zone
	 * @param {string} type The type of the zone to generate the name based on
	 * @returns {string} The generated name
	 */
	generateName(type){
		if(type === "personal")
			return "Personal";
		else
			return this.zone_names[_.random(this.zone_names.length-1)];
	}
};