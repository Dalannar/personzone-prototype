const express = require("express");
const _ = require("lodash");
const winston = require("winston");

module.exports = function(db){
	const router = express.Router();

	router.get("/", welcome(db));

	return router;
};

function welcome(db){
	return [
		function(req, res, next){
			let renderProperties = _.assign({}, req.defaultRenderProperties);

			req.dbquery.getLatestUsers(db, 24)
				.then(function(users){
					renderProperties.latestUsers = _.map(users, user => user.getData("minimal"));
					res.render("welcome", renderProperties);
				}).catch(function(error){
					winston.error("Error retrieving latest users.", error);
					res.render("welcome", renderProperties);
				});
		}
	];
}