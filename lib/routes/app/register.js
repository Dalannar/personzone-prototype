const express = require("express");

module.exports = function(db){
	const router = express.Router();

	router.get("/", register());

	return router;
};

function register(){
	return [
		function(req, res, next){
			res.render("register", req.defaultRenderProperties);
		}
	];
}