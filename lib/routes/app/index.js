const express = require("express");

const { defaultRenderProperties, enableRPI } = require("../util");

module.exports = function(db){
	const router = express.Router();

	router.use(defaultRenderProperties());
	router.use(enableRPI());

	const createWelcomeRouter = require("./welcome");
	router.use("/welcome", createWelcomeRouter(db));
	router.get("/", (req, res) => res.redirect("/welcome"));

	const createRegisterRouter = require("./register");
	router.use("/register", createRegisterRouter(db));

	const createAccountRouter = require("./account");
	router.use("/a", createAccountRouter(db));

	const createRankingsRouter = require("./rankings");
	router.use("/rankings", createRankingsRouter(db));

	return router;
};