const _ = require("lodash");
const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { isPurl } = require("../../../util/validation");
const { extractLocationDetails } = require("../../../util/gapi");
const { asyncMiddleware } = require("../../../util/express");

module.exports = function(db){
	const router = express.Router();

	router.get("/", rankings(db));
	router.get("/:trait_id", rankings(db));

	return router;
};

function rankings(db){
	return [
		check("trait_id").optional({checkFalsy: true}).isInt(),
		check("page").optional({checkFalsy: true}).isInt(),
		check("limit").optional({checkFalsy: true}).isInt(),
		check("order").optional({checkFalsy: true}).isIn(["asc", "desc"]),
		check("place_id").optional({checkFalsy: true}),
		check("render_results_html").optional({checkFalsy: true}).isBoolean(),
		asyncMiddleware(async (req, res, next) => {
			validationResult(req).throw();
			const params = _.assign({}, {
				trait_id: null,
				page: 0,
				limit: 10,
				order: "desc",
				place_id: null,
				render_results_html: false,
			}, matchedData(req));

			params.limit = _.toNumber(params.limit);

			// Default render properties
			let renderProperties = _.assign({}, req.defaultRenderProperties);
			renderProperties.queryParams = params;
			renderProperties.traitName = null;
			renderProperties.placeDetails = {};
			renderProperties.mapViewport = null;
			renderProperties.rankings = [];
			renderProperties.totalRanks = null;
			
			// Sending the selected trait name
			if(_.isNil(params.trait_id))
				return res.render("rankings", renderProperties);

			params.trait_id = _.toNumber(params.trait_id);
			if(params.trait_id === 0)
				renderProperties.traitName = "All traits";
			else
				renderProperties.traitName = (await db.models.trait.findById(params.trait_id)).get("name");

			// Sending the selected location
			let location = null;
			if(params.place_id){
				const placeDetails = (await req.gapi.place({placeid: params.place_id})).json.result;
				renderProperties.placeDetails = placeDetails;
				renderProperties.mapViewport = placeDetails.geometry.viewport;

				// Pick only the relevant information to use in search
				location = _.pick(extractLocationDetails(placeDetails), [
					"administrative_area_level_3", "administrative_area_level_2", "administrative_area_level_1",
					"locality", "country", "route",
				]);
			}

			// Get the rankings
			const { count, rows } = await req.dbquery.getZonetraitRankings(db, {
				page: params.page,
				limit: params.limit,
				order: params.order,
				trait_id: params.trait_id,
				location,
			});

			// Set only the first location for each ranking result
			let rankings = _.map(rows, function(row){
				let rank = row.get({plain: true});
				rank.zone.user.location = {};
				if(rank.zone.user.locations.length > 0){
					rank.zone.user.location = rank.zone.user.locations[0];
				}

				delete rank.zone.user.locations;
				return rank;
			});

			renderProperties.rankings = rankings;
			renderProperties.totalRanks = count;

			if(params.render_results_html)
				res.render("server-rendering/rank-results", renderProperties);
			else
				res.render("rankings", renderProperties);
		})
	];
}