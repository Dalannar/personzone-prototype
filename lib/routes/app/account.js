const express = require("express");
const _ = require("lodash");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { isPurl, isZurl } = require("../../../util/validation");

module.exports = function(db){
	const router = express.Router();

	router.get("/:purl", zone(db));
	router.get("/:purl/z/:zurl", zone(db));

	router.get("/:purl/feedback/:zurl", feedback(db));
	router.get("/:purl/feedback", feedback(db));

	router.get("/:purl/settings", settings(db));
	router.get("/:purl/settings/:zurl", settings(db));

	return router;
};

function settings(db){
	return [
		check("purl").custom(isPurl),
		check("zurl").custom(isZurl).optional({checkFalsy: true}),
		function(req, res, next){
			validationResult(req).throw();
			const params = Object.assign({}, {
				zurl: "",
			}, matchedData(req));

			// User not logged in
			if(!req.user)
				return res.redirect(`/a/${params.purl}`);

			// Not correct user
			if(req.user.get("purl") !== params.purl)
				return res.redirect(`/a/${params.purl}`);

			let renderProperties = Object.assign({}, req.defaultRenderProperties);
			renderProperties.purl = params.purl;
			renderProperties.queryParams = params;

			req.dbquery.getUserByPurl(db, params.purl)
				.then(function(user){
					if(!user)
						return res.redirect(`/a/${params.purl}`);
					
					renderProperties.user = user.getData("owner");

					res.render("settings", renderProperties);
				}).catch(next);
		}
	];
}

function feedback(db){
	return [
		check("purl").custom(isPurl),
		check("zurl").custom(isZurl).optional({checkFalsy: true}),
		check("page").isInt().optional({checkFalsy: true}).toInt(),
		check("resultsPer").isInt().optional({checkFalsy: true}).toInt(),
		check("orderBy").isIn(["asc", "desc"]).optional({checkFalsy: true}),
		check("subzone").isInt().optional({checkFalsy: true}).toInt(),
		check("thanked").isIn(["all", "true", "false"]).optional({checkFalsy: true}),
		function(req, res, next){
			validationResult(req).throw();
			const params = Object.assign({}, {
				page: 0,
				resultsPer: 10,
				orderBy: "desc",
				thanked: "all"
			}, matchedData(req));

			// User not logged in
			if(!req.user)
				return res.redirect(`/a/${params.purl}`);

			// Not correct user
			if(req.user.get("purl") !== params.purl)
				return res.redirect(`/a/${params.purl}`);

			let renderProperties = Object.assign({}, req.defaultRenderProperties);
			renderProperties.purl = params.purl;
			renderProperties.queryParams = params;

			req.dbquery.getUserByPurl(db, params.purl)
				.then(function(user){
					if(!user)
						return res.redirect(`/a/${params.purl}`);

					renderProperties.user = user.getData("owner");

					let zonePromise = Promise.resolve();
					if(params.zurl){
						zonePromise = db.models.zone.findOne({where: { zurl: params.zurl }, include: [ db.models.subzone ]});
					}

					zonePromise.then(function(zone){
						if(zone)
							renderProperties.subzones = zone.get("subzones").map(s => s.getData("owner"));
					
						let searchOptions = {thanked: params.thanked};
						if(params.subzone){
							searchOptions.subzone = params.subzone;
						}else if(params.purl && zone){
							searchOptions.zone = zone.get("id");
						}

						req.dbquery.getFeedbackComments(db, user.get("id"), params.resultsPer, params.page, params.orderBy, searchOptions)
							.then(function({rows, count}){
								renderProperties.comments = rows.map(o => o.getData("minimal"));
								renderProperties.totalComments = count;

								res.render("feedback", renderProperties);
							}).catch(next);
					}).catch(next);
				}).catch(next);
		}
	];
}

function zone(db){
	return [
		check("purl").custom(isPurl),
		check("zurl").optional({checkFalsy: true}).custom(isZurl),
		check("email_verification_successful").optional(),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			let userAccess = "anon";
			if(req.user){
				userAccess = req.user.get("purl") === params.purl ? "owner" : "logged";
			}

			let renderProperties = Object.assign({}, req.defaultRenderProperties);
			renderProperties.purl = params.purl;
			renderProperties.zurl = params.zurl;
			renderProperties.email_verification_successful = _.has(params, "email_verification_successful");

			req.dbquery.getUserByPurl(db, params.purl)
				.then(function(user){
					if(!user){
						res.status(404);
						return res.render("zone", renderProperties);
					}

					renderProperties.user = user.getData(userAccess === "owner" ? "owner" : "minimal");

					const findZone = params.zurl ?
						req.dbquery.getZoneByUserIdZurl(db, user.get("id"), params.zurl) :
						req.dbquery.getZoneById(db, user.get("default_zone"));

					findZone.then(function(zone){
						if(!zone){
							res.status(404);
							return res.render("zone", renderProperties);
						}

						renderProperties.zone = zone.getData(userAccess === "owner" ? "owner" : "minimal");
						// Repeated here again because we might not have been supplied a zurl in the url
						renderProperties.zurl = zone.get("zurl");

						if(userAccess === "owner"){
							req.dbquery.getZoneZonetraitsFinalRating(db, zone.get("id"))
								.then(function(zonetraits){
									renderProperties.zone.zonetraits = zonetraits.map(o => o.getData("owner"));
									res.render("zone", renderProperties);
								}).catch(next);
						}else if(userAccess === "logged"){
							req.dbquery.getZoneZonetraitsUserRating(db, zone.get("id"), req.user.get("id"))
								.then(function(zonetraits){
									renderProperties.zone.zonetraits = zonetraits.map(o => o.getData({
										root: "minimal",
										ratings: "owner",
									}));

									renderProperties.zone.zonetraits = renderProperties.zone.zonetraits.map(function(z){
										return Object.assign({}, z, {
											ratings: null,
											rating: z.ratings.length > 0 ? z.ratings[0] : null,
										});
									});

									res.render("zone", renderProperties);
								}).catch(next);
						}else{
							
							res.render("zone", renderProperties);
						}

					}).catch(next);
				}).catch(next);
		}
	];
}