const Polyglot = require("node-polyglot");
const _ = require("lodash");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

module.exports = {
	/**
	 * Set req.defaultRenderProperties, used for all app pages
	 * Read documentation for full explanation
	 */
	defaultRenderProperties(){
		return [
			function(req, res, next){
				const locale = req.localizer.getLocale();
				const p = new Polyglot(locale);
				req.defaultRenderProperties = {
					authPurl: req.user ? req.user.get("purl") : null,
					locale,
					t: p.t.bind(p),
				};

				return next();
			}
		];
	},

	/**
	 * Check the request object for param "rpi", wether it has a value or not
	 * In case "rpi" is present then set's response object's render function to just send the renderProperties instead of rendering the page
	 */
	enableRPI(){
		return [
			check("rpi").optional(),
			function(req, res, next){
				validationResult(req).throw();
				const params = matchedData(req);

				// The request contains "rpi" (even if it doesn't have a value associated)
				if(_.has(params, "rpi"))
					res.render = (page, renderProperties) => res.json(renderProperties);

				return next();
			}
		];
	},
};