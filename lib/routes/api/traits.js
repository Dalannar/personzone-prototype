const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const TraitNotFoundError = require("../../errors/TraitNotFoundError");

module.exports = function(db){
	const router = express.Router();

	router.get("/", getTraits(db));

	return router;
};

/**
 * @api {get} /traits/ Get traits
 * @apiName Get traits
 * @apiGroup Traits
 *
 * @apiParam {String} [name=""] The name of the trait to get.
 * @apiParam {String} [search=""] A term to search for traits with. If this is set the function will act as trait searching
 * @apiParam {Number} [limit=6] A maximum number of results to return
 *
 * @apiSuccess {Number} id The id of the trait
 * @apiSuccess {String} trait The name of the trait
 * 
 * @apiUse TraitNotFoundError
 */
function getTraits(db){
	return [
		check("name").optional({checkFalsy: true}),
		check("search").optional({checkFalsy: true}),
		check("limit").optional({checkFalsy: true}).isInt(),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(params.search){
				const limit = Number(params.limit) || 6;
				db.models.trait.findAll({
					where: {
						name: {
							$like: `%${params.search}%`
						}
					},
					limit,
				}).then(function(traits){
					res.status(200).send(traits.map(trait => trait.getData("minimal")));
				}).catch(next);
			}else{
				db.models.trait.findOne({
					where: { 
						name: db.fn("lower", params.name)
					}
				}).then(function(trait){
					if(!trait)
						throw new TraitNotFoundError(null, params.name);

					res.status(200).send(trait.getData("minimal"));
				}).catch(next);
			}
		}
	];
}