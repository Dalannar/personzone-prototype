const express = require("express");
const validator = require("validator");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { isPurl, isCommentText } = require("../../../util/validation");

const {
	UnauthorizedError,
	SubzoneNotFoundError,
	CommentNotFoundError,
	UserNotFoundError
} = require("../../errors");

module.exports = function(db){
	const router = express.Router();

	router.get("/:id/thank", thankComment(db));
	router.post("/", createComment(db));

	return router;
};

/**
 * @api {get} /comments/:id/thank Thank comment
 * @apiName Thank comment
 * @apiGroup Comments
 *
 * @apiParam {number} id The id of the comment to thank
 * 
 * @apiUse UnauthorizedError
 * @apiUse CommentNotFoundError
 */
function thankComment(db){
	return [
		check("id").isInt().toInt(),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
	
			// User is not logged in
			if(!req.user)
				throw new UnauthorizedError();
	
			db.models.comment.findById(params.id, {
				include: [{
					model: db.models.subzone,
					include: [ db.models.zone ],
				}],
			}).then(function(comment){
				// Comment does not exist
				if(!comment)
					throw new CommentNotFoundError(params.id);
	
				// Zone does not belong to logged in user
				if(comment.get("subzone").get("zone").get("user_id") !== req.user.get("id"))
					throw new UnauthorizedError();
	
				comment.update({
					thanked: true,
				}).then(function(){
					return res.sendStatus(200);
				}).catch(next);
			}).catch(next);
		}
	];
}

/**
 * @api {post} /comments/ Create comment
 * @apiName Create comment
 * @apiGroup Comments
 *
 * @apiParam {Number} subzone_id The id of the subzone to comment on
 * @apiParam {String} comment The text of the comment
 * @apiParam {String} commenter_purl The personal url of the user making the comment
 *
 * @apiSuccess {Number} id The id of the newly created comment
 * @apiSuccess {Bool} thanked Whether or not the comment has been thanked
 * @apiSuccess {String} comment The text of the comment
 * @apiSuccess {Number} user_id The id of the user that created the comment
 * @apiSuccess {Number} subzone_id The id of the subzone that the comment was created on
 * @apiSuccess {Date} created_at The timestamp of when the comment was created
 * @apiSuccess {Date} updated_at The timestamp of when the comment was updated
 * 
 * @apiUse UnauthorizedError
 * @apiUse SubzoneNotFoundError
 * @apiUse UserNotFoundError
 */
function createComment(db){
	return [
		check("commenter_purl").exists().custom(isPurl),
		check("subzone_id").exists().isInt().toInt(),
		check("comment").exists().custom(isCommentText),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();
		
			if(req.user.get("purl") !== params.commenter_purl)
				throw new UnauthorizedError();

			db.models.user.findOne({
				where: { purl: params.commenter_purl }
			}).then(function(commenter){
				if(!commenter)
					throw new UserNotFoundError(params.commenter_purl);

				db.models.subzone.findById(params.subzone_id, {
					include: [ db.models.zone ]
				}).then(function(subzone){
					if(!subzone)
						throw new SubzoneNotFoundError(params.subzone_id);
	
					if(subzone.get("zone").get("user_id") === commenter.get("id"))
						throw new UnauthorizedError();
	
					db.models.comment.create({
						comment: params.comment,
						user_id: commenter.get("id"),
						subzone_id: params.subzone_id
					}).then(function(comment){
						res.status(201).send(comment);
					}).catch(next);
				}).catch(next);
			}).catch(next);
		}
	];
}