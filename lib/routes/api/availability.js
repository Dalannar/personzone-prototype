const express = require("express");

const { isEmail, isPurl } = require("../../../util/validation");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

module.exports = function(db){
	const router = express.Router();

	router.get("/email/:email", email(db));
	router.get("/email", (req, res, next) => res.sendStatus(400));

	router.get("/purl/:purl", purl(db));
	router.get("/purl", (req, res, next) => res.sendStatus(400));
	
	return router;
};

/**
 * @api {get} /availability/purl/:purl Check purl availability
 * @apiName Check purl availability
 * @apiGroup Availability
 *
 * @apiParam {string} purl The purl to check availability for
 *
 * @apiSuccess {bool} available Whether the purl is available or not
 */
function purl(db){
	return [
		check("purl").custom(isPurl),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			db.models.user.findOne({
				where: { purl: params.purl },
			}).then(function(user){
				res.send({available: !user});
			}).catch(next);
		}
	];
}

/**
 * @api {get} /availability/email/:email Check email availability
 * @apiName Check email availability
 * @apiGroup Availability
 *
 * @apiParam {string} email The email to check availability for
 *
 * @apiSuccess {bool} available Whether the email is available or not
 */
function email(db){
	return [
		check("email").custom(isEmail),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			db.models.user.findOne({
				where: { email: params.email },
			}).then(function(user){
				res.send({available: !user});
			}).catch(next);
		}
	];
}