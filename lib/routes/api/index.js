const express = require("express");

module.exports = function(db){
	const router = express.Router();

	const createUsersRouter = require("./users");
	router.use("/users", createUsersRouter(db));

	const createAuthRouter = require("./auth");
	router.use("/auth", createAuthRouter(db));

	const createSubzonesRouter = require("./subzones");
	router.use("/subzones", createSubzonesRouter(db));

	const createCommentsRouter = require("./comments");
	router.use("/comments", createCommentsRouter(db));

	const createZonetraitsRouter = require("./zonetraits");
	router.use("/zonetraits", createZonetraitsRouter(db));

	const createTraitsRouter = require("./traits");
	router.use("/traits", createTraitsRouter(db));

	const createRatingsRouter = require("./ratings");
	router.use("/ratings", createRatingsRouter(db));

	const createAvailabilityRouter = require("./availability");
	router.use("/availability", createAvailabilityRouter(db));

	const createZonesRouter = require("./zones");
	router.use("/zones", createZonesRouter(db));

	const createI18nRouter = require("./i18n");
	router.use("/i18n", createI18nRouter(db));

	const createGapiRouter = require("./gapi");
	router.use("/gapi", createGapiRouter(db));

	const createRankingsRouter = require("./rankings");
	router.use("/rankings", createRankingsRouter(db));

	return router;
};


/**
 * @apiDefine UserNotFoundError
 * @apiError (404) UserNotFoundError The user could not be found
 * @apiErrorExample {json} UserNotFoundError
 * 		HTTP/1.1 404 Not Found
 * 		{
 * 			error: "UserNotFoundError",
 * 			message: "User with purl 'pedro123' could not be found."
 * 		}
 */

/**
 * @apiDefine UnauthorizedError
 * @apiError (401) UnauthorizedError Unauthorized request
 * @apiErrorExample {json} UnauthorizedError
 * 		HTTP/1.1 401 Unauthorized
 * 		{
 * 			error: "UnauthorizedError",
 * 			message: "Unauthorized request."
 * 		}
 */

/**
 * @apiDefine EmailAlreadyVerifiedError
 * @apiError (403) EmailAlreadyVerifiedError Email is already verified
 * @apiErrorExample {json} EmailAlreadyVerifiedError
 * 		HTTP/1.1 403 Forbidden
 * 		{
 * 			error: "EmailAlreadyVerifiedError",
 * 			message: "The email is already verified."
 * 		}
 */

/**
 * @apiDefine IncorrectHashError
 * @apiError (422) IncorrectHashError Hash is not correct
 * @apiErrorExample {json} IncorrectHashError
 * 		HTTP/1.1 422 Unprocessable Entity
 * 		{
 * 			error: "IncorrectHashError",
 * 			message: "Hash '3f1774e7bb7779160c939a0370f8fbc9' is not correct.",
 * 		}
 */

/**
 * @apiDefine PurlAlreadyExistsError
 * @apiError (409) PurlAlreadyExistsError Purl already exists and is in use
 * @apiErrorExample {json} PurlAlreadyExistsError
 * 		HTTP/1.1 409 Conflict
 * 		{
 * 			error: "PurlAlreadyExistsError",
 * 			message: "Purl 'pedro123' already exists."
 * 		}
 */

/**
 * @apiDefine EmailAlreadyExistsError
 * @apiError (409) EmailAlreadyExistsError Email already exists and is in use
 * @apiErrorExample {json} EmailAlreadyExistsError
 * 		HTTP/1.1 409 Conflict
 * 		{
 * 			error: "EmailAlreadyExistsError",
 * 			message: "Email 'pedro@yopmail.com' already exists."
 * 		}
 */

/**
 * @apiDefine ZoneNotFoundError
 * @apiError (404) ZoneNotFoundError The zone could not be found
 * @apiErrorExample {json} ZoneNotFoundError
 * 		HTTP/1.1 404 Not Found
 * 		{
 *	 		error: "ZoneNotFoundError",
 *			message: `Zone with id '1652' could not be found.`
 * 		}
 */

/**
 * @apiDefine ZonetraitAlreadyExistsError
 * @apiError (409) ZonetraitAlreadyExistsError The zonetrait already exists
 * @apiErrorExample {json} ZonetraitAlreadyExistsError
 * 		HTTP/1.1 409 Conflict
 * 		{
 * 			error: "ZonetraitAlreadyExists",
 *			message: `Zonetrait for trait '123' already exists.`
 * 		}
 */

/**
 * @apiDefine ZonetraitNotFoundError
 * @apiError (404) ZonetraitNotFoundError The zonetrait could not be found
 * @apiErrorExample {json} ZonetraitNotFoundError
 * 		HTTP/1.1 404 Not Found
 * 		{
 * 			error: "ZonetraitNotFoundError",
 *			message: `Zonetrait with id '${this.zonetraitId}' could not be found.`
 * 		}
 */

/**
 * @apiDefine ZoneAlreadyExistsError
 * @apiError (409) ZoneAlreadyExistsError The zone already exists
 * @apiErrorExample {json} ZoneAlreadyExistsError
 * 		HTTP/1.1 409 Conflict
 * 		{
 * 			error: "ZoneAlreadyExistsError",
 * 			message: "Zone with zurl 'personal' already exists."
 * 		}
 */

/**
 * @apiDefine LocaleNotFoundError
 * @apiError (404) LocaleNotFoundError The locale could not be found
 * @apiErrorExample {json} LocaleNotFoundError
 * 		HTTP/1.1 404 Not Found
 * 		{
 * 			error: "LocaleNotFoundError",
 * 			message: "Locale with code 'fr' could not be found"
 * 		}
 */

/**
 * @apiDefine SubzoneNotFoundError
 * @apiError (404) SubzoneNotFoundError The subzone could not be found
 * @apiErrorExample {json} SubzoneNotFoundError
 * 		HTTP/1.1 404 Not Found
 * 		{
 * 			error: "SubzoneNotFoundError",
 * 			message: "Subzone with id '2' could not be found"
 * 		}
 */

/**
 * @apiDefine SubzoneAlreadyExistsError
 * @apiError (409) SubzoneAlreadyExistsError The subzone already exists
 * @apiErrorExample {json} SubzoneAlreadyExistsError
 * 		HTTP/1.1 409 Conflict
 * 		{
 * 			error: "SubzoneAlreadyExistsError",
 * 			message: "Subzone with name 'sampleName' already exists"
 * 		}
 */

/**
 * @apiDefine TraitNotFoundError
 * @apiError (404) TraitNotFoundError The trait could not be found
 * @apiErrorExample {json} TraitNotFoundError
 * 		HTTP/1.1 404 Not Found
 * 		{
 * 			error: "TraitNotFoundError",
 * 			message: "Trait with [id=null;name=SomethingTrait] could not be found"
 * 		}
 */

/**
 * @apiDefine InvalidDobError
 * @apiError (400) InvalidDobError The date of birth is not valid
 * @apiErrorExample {json} InvalidDobError
 * 		HTTP/1.1 400 Bad Request
 * 		{
 * 			error: "InvalidDobError",
 * 			message: "Date of birth '2999-01-01' is not a valid date of birth"
 * 		}
 */

/**
 * @apiDefine CommentNotFoundError
 * @apiError (404) CommentNotFoundError The comment could not be found
 * @apiErrorExample {json} CommentNotFoundError
 * 		HTTP/1.1 404 Not Found
 * 		{
 * 			error: "CommentNotFoundError",
 * 			message: "Comment with id '1' could not be found"
 * 		}
 */
