const _ = require("lodash");
const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { isPurl, isZurl } = require("../../../util/validation");

const UnauthorizedError = require("../../errors/UnauthorizedError");
const UserNotFoundError = require("../../errors/UserNotFoundError");

module.exports = function(db){
	const router = express.Router();

	router.get("/zone-ratings", getZoneRatings(db));
	router.get("/flow-ratings", getFlowRatings(db));

	return router;
};

/**
 * @api {get} /ratings/zone-ratings Get zone ratings
 * @apiName Get zone ratings
 * @apiGroup Ratings
 * 
 * @apiParam {String} purl The personal URL of the user that owns the zone
 * @apiParam {String} zurl The url of the zone to get ratings for
 * 
 * @apiSuccess {Number} zonetrait_id The id of the zonetrait
 * @apiSuccess {Number} avg The average rating of the zonetrait
 * @apiSuccess {Number} count The number of ratings made to the zonetrait
 * 
 * @apiSuccessExample {json} Response
 * 		HTTP/1.1 200 OK
 * 		[
 *			{
 * 				"zonetrait_id": 3,
 *				"avg": 4,
 *				"count": 1
 *			}
 *		]
 *
 * @apiUse UnauthorizedError
 */
function getZoneRatings(db){
	return [
		check("purl").exists().custom(isPurl),
		check("zurl").exists().custom(isZurl),

		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();

			if(req.user.get("purl") !== params.purl)
				throw new UnauthorizedError();

			db.models.rating.findAll({
				attributes: [
					"zonetrait_id",
					[db.fn("AVG", db.col("rating.rating")), "avg"],
					[db.fn("COUNT", db.col("rating.id")), "count"],
				],
				group: ["rating.zonetrait_id"],
				include: [{
					attributes: [],
					model: db.models.zonetrait,
					groub: ["id"],
					include: [{
						attributes: [],
						model: db.models.zone,
						where: { zurl: params.zurl },
						include: [{
							attributes: [],
							model: db.models.user,
							where: { purl: params.purl }
						}]
					}]
				}]
			}).then(function(ratings){
				// TODO: This is super hacky, just to transform the string that sequelize gives us back
				// in the 'avg' field.... please replace this at some point, right now by back is hurting and I just want to stop
				ratings = _.map(ratings, function(o){
					return {
						zonetrait_id: o.get("zonetrait_id"),
						avg: Number.parseFloat(o.get("avg")),
						count: o.get("count"),
					};
				});
				res.status(200).send(ratings);
			}).catch(next);
		}
	];
}

/**
 * @api {get} /flow-ratings Get flow ratings
 * @apiName Get flow ratings
 * @apiGroup Ratings
 * 
 * @apiDescription A flow is a link between two users. This api will return all ratings made by user 'a' to user 'b'
 * 				Note that, if receiver_zurl is provided and a zone with that zurl does not exist
 * 				the api call will not launch a ZoneNotFoundError. Instead it will quietly respond with an empty array
 * 
 * @apiParam {String} rater_purl The personal URL of the user that rated
 * @apiParam {String} receiver_purl The personal URL of the user that was rated 
 * @apiParam {String} [receiver_zurl] The URL of the zone. If this is not present, then return the ratings for every zone that belongs to the receiver
 * 
 * @apiSuccess {Number} id The id of the rating
 * @apiSuccess {Number} rating The rating
 * @apiSuccess {Date} created_at The date when the rating was created
 * @apiSuccess {Date} updated_at The date when the rating was updated
 * @apiSuccess {Number} zonetrait_id The id of the zonetrait rated
 * @apiSuccess {Number} user_id The id of the user that rated
 * 
 * @apiSuccess {Object} zonetrait
 * @apiSuccess {Number} zonetrait.id The id of the zonetrait this rating belongs to
 * @apiSuccess {Date} zonetrait.created_at The date when the zonetrait was created
 * @apiSuccess {Date} zonetrait.updated_at The date when the zonetrait was updated
 * @apiSuccess {Number} zonetrait.trait_id The id of the trait
 * @apiSuccess {Number} zonetrait.zone_id The id of the zone
 *
 * @apiSuccess {Object} zonetrait.zone
 * @apiSuccess {Number} zonetrait.zone.id The id of the zone
 * @apiSuccess {String} zonetrait.zone.type The type of the zone
 * @apiSuccess {String} zonetrait.zone.name The name of the zone
 * @apiSuccess {String} zonetrait.zone.zurl The URL of the zone
 * @apiSuccess {Number} zonetrait.zone.user_id The id of the user who owns the zone
 * @apiSuccess {Date} zonetrait.zone.created_at The date when the zone was created
 * @apiSuccess {Date} zonetrait.zone.updated_at The date when the zone was updated
 * 
 * @apiSuccessExample {json} Response
 * 		HTTP/1.1 200 OK
 * 		[
 *			{
 *				"id": 2,
 *				"rating": 4,
 *				"created_at": "2017-10-25T12:46:55.000Z",
 *				"updated_at": "2017-10-25T12:46:55.000Z",
 * 				"zonetrait_id": 3,
 *				"user_id": 2,
 *				"zonetrait": {
 *					"id": 3,
 *					"created_at": "2017-10-23T11:12:57.000Z",
 *					"updated_at": "2017-10-23T11:12:57.000Z",
 *					"trait_id": 50,
 *					"zone_id": 1,
 *					"zone": {
 *						"id": 1,
 *						"type": "personal",
 *						"name": "Personal",
 *						"zurl": "personal",
 *						"user_id": 1,
 *						"created_at": "2017-10-23T11:12:53.000Z",
 *						"updated_at": "2017-10-23T11:12:53.000Z"
 *					}
 *				}
 *			}
 *		]
 *
 * @apiUse UnauthorizedError
 * @apiUse UserNotFoundError
 */
function getFlowRatings(db){
	return [
		check("rater_purl").exists().custom(isPurl),
		check("receiver_purl").exists().custom(isPurl),
		check("receiver_zurl").optional({checkFalsy: true}),

		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();

			if(req.user.get("purl") !== params.rater_purl)
				throw new UnauthorizedError();

			db.models.user.findAll({
				where: {
					$or: [{
						purl: params.rater_purl,
					}, {
						purl: params.receiver_purl,
					}]
				},
				limit: 2,
			}).then(function(users){
				let rater_id, receiver_id;
				_.forEach(users, function(u){
					switch(u.get("purl")){
						case params.rater_purl: return rater_id = u.get("id");
						case params.receiver_purl: return receiver_id = u.get("id");
					}
				});

				if(!rater_id)
					throw new UserNotFoundError(params.rater_purl);
				if(!receiver_id)
					throw new UserNotFoundError(params.receiver_purl);

				let zoneWhere = { user_id: receiver_id };
				if(params.receiver_zurl)
					zoneWhere.zurl = params.receiver_zurl;

				db.models.rating.findAll({
					where: { user_id: rater_id },
					include: [{
						model: db.models.zonetrait,
						include: [{
							model: db.models.zone,
							where: zoneWhere,
						}],
					}],
				}).then(function(ratings){
					ratings = _.filter(ratings, o => !_.isNil(o.zonetrait));
					res.status(200).send(ratings);
				}).catch(next);
			}).catch(next);
		}
	];
}