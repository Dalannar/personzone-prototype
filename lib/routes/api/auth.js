const sequelize = require("sequelize");
const express = require("express");
const passport = require("passport");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

module.exports = function(db){
	const router = express.Router();

	router.post("/login", login(db));
	router.get("/logout", logout());

	return router;
};

/**
 * @api {post} /login Login
 * @apiName Login
 * @apiGroup Auth
 *
 * @apiParam {string} email The email to login with
 * @apiParam {string} password The password to login with
 *
 * @apiSuccess {string} purl The user's personal url
 */
function login(db){
	return [
		passport.authenticate("local"),
		function(req, res, next){
			req.user.get("auth").update({
				last_login: sequelize.fn("NOW")
			});

			res.send({purl: req.user.get("purl")});
		}
	];
}

/**
 * @api {get} /logout Logout
 * @apiName Logout
 * @apiGroup Auth
 */
function logout(){
	return [
		function(req, res, next){
			req.logout();
			res.sendStatus(200);
		}
	];
}