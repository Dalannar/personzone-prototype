const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { isSubzoneName } = require("../../../util/validation");
const UnauthorizedError = require("../../errors/UnauthorizedError");
const ZoneNotFoundError = require("../../errors/ZoneNotFoundError");
const SubzoneAlreadyExistsError = require("../../errors/SubzoneAlreadyExistsError");
const SubzoneNotFoundError = require("../../errors/SubzoneNotFoundError");

module.exports = function(db){
	const router = express.Router();

	router.delete("/:id", deleteSubzone(db));
	router.post("/", createSubzone(db));

	return router;
};

/**
 * @api {delete} /:id Delete subzone
 * @apiName Delete subzone
 * @apiGroup Subzone
 *
 * @apiParam {Number} id The id of the subzone to delete
 * 
 * @apiUse UnauthorizedError
 * @apiUse SubzoneNotFoundError
 */
function deleteSubzone(db){
	return [
		check("id").exists().isInt(),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();
	
			db.models.subzone.findById(params.id, {
				include: [ db.models.zone ],
			}).then(function(subzone){
				if(!subzone)
					throw new SubzoneNotFoundError(params.id);
	
				if(subzone.get("zone").get("user_id") !== req.user.get("id"))
					throw new UnauthorizedError();
	
				subzone.destroy().then(function(){
					res.sendStatus(200);
				}).catch(next);
			}).catch(next);
		}
	];
}

/**
 * @api {post} / Create subzone for zone
 * @apiName Create subzone for zone
 * @apiGroup Subzone
 *
 * @apiParam {String} subzone The name of the subzone to create
 * @apiParam {Number} zone_id The id of the zone to add the subzone to
 *
 * @apiSuccess {Number} id The id of the newly created subzone
 * @apiSuccess {Number} zone_id The id of the zone
 * @apiSuccess {String} subzone The name of the newly created subzone
 * @apiSuccess {Date} created_at The date when the subzone was created
 * @apiSuccess {Date} updated_at The date when the subzone was last updated
 * 
 * @apiUse UnauthorizedError
 * @apiUse ZoneNotFoundError
 * @apiUse SubzoneAlreadyExistsErrors
 */
function createSubzone(db){
	return [
		check("subzone").exists().custom(isSubzoneName),
		check("zone_id").exists().isInt(),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();

			db.models.zone.findById(params.zone_id)
				.then(function(zone){
					if(!zone)
						throw new ZoneNotFoundError(params.zone_id);

					if(zone.get("user_id") !== req.user.get("id"))
						throw new UnauthorizedError();

					db.models.subzone.findOrCreate({
						where: { name: params.subzone, zone_id: zone.get("id") }
					}).spread(function(subzone, created){
						if(!created)
							throw new SubzoneAlreadyExistsError(params.subzone);
						
						res.status(201).send(subzone);
					}).catch(next);
				}).catch(next);
		}
	];
}