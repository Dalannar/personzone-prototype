const sequelize = require("sequelize");
const express = require("express");
const winston = require("winston");
const formidable = require("formidable");
const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { isUserName, isEmail, isPurl, isLocation, isJobTitle } = require("../../../util/validation");
const { GENDERS, LOCATION_TYPES } = require("../../../util/enums");
const { extractLocationDetails } = require("../../../util/gapi");
const { asyncMiddleware } = require("../../../util/express");
const { generatePurl } = require("../../../util/models");

const {
	UserNotFoundError,
	UnauthorizedError,
	EmailAlreadyVerifiedError,
	IncorrectHashError,
	PurlAlreadyExistsError,
	EmailAlreadyExistsError,
	InvalidDobError
} = require("../../errors");

module.exports = function(db){
	const router = express.Router();

	router.get("/:purl/resend-email-verification", resendEmailVerification(db));
	router.get("/:purl/verify-email/:hash", verifyEmail(db));
	router.get("/:purl/verify-email", (req, res, next) => res.sendStatus(400));
	router.get("/:purl/avatar", getAvatar(db));
	router.get("/", searchUsers(db));
	
	router.post("/:purl/avatar", updateAvatar(db));
	router.post("/", createUser(db));
	router.post("/:purl", updateUser(db));

	router.post("/:purl/location", postLocation(db));

	return router;
};

/**
 * @api {post} /users/:purl/location Post location
 * @apiName Post location
 * @apiGroup Users
 * 
 * @apiParam {string} purl The user's personal URL
 * @apiParam {enum} location_type The type of location to create
 * @apiParam {string} place_id Google map's place id for the location to create
 * 
 * @apiSuccessExample Respose
 * 		HTTP/1.1 201 Created
 * 
 * @apiUse UnauthorizedError
 */
function postLocation(db){
	return [
		check("purl").exists(isPurl),
		check("place_id").exists(),
		check("type").exists().isIn(LOCATION_TYPES),
		asyncMiddleware(async (req, res, next) => {
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();
	
			if(req.user.get("purl") !== params.purl)
				throw new UnauthorizedError();

			const response = await req.gapi.placeDetails(params.place_id);
			let location = extractLocationDetails(response.json.result);
			location.place_id = params.place_id;
			location.type = params.type;

			const l = await db.models.location.create({});
			await req.user.addLocation(l);
			res.send(201);
		})
	];
}

/**
 * @api {get} /users/:purl/resend-email-verification Resend email verification
 * @apiName Resend email verification
 * @apiGroup Users
 *
 * @apiParam {String} purl The user's personal URL
 * 
 * @apiSuccessExample Response
 * 		HTTP/1.1 200 OK
 * 
 * @apiUse UserNotFoundError
 * @apiUse UnauthorizedError
 * @apiUse EmailAlreadyVerifiedError
 */
function resendEmailVerification(db){
	return [
		check("purl").custom(isPurl),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
	
			if(!req.user)
				throw new UnauthorizedError();
	
			if(req.user.get("purl") !== params.purl)
				throw new UnauthorizedError();

			db.models.user.findOne({
				where: { purl: params.purl },
				include: [ db.models.auth ],
			}).then(function(user){
				if(!user)
					throw new UserNotFoundError(params.purl);

				if(user.get("auth").get("email_verified"))
					throw new EmailAlreadyVerifiedError();

				req.mailer.sendTemplate("verifyEmail", {
					purl: user.get("purl"),
					hash: user.get("auth").get("email_verification_hash")
				},{
					to: user.get("email")
				}).then(function(){
					res.sendStatus(200);
				}).catch(next);
			}).catch(next);
		}
	];
}

/**
 * @api {get} /users/:purl/verify-email/:hash Verify email
 * @apiName Verify email
 * @apiGroup Users
 *
 * @apiParam {String} purl The user's personal URL
 * @apiParam {String} hash The email verification hash to test
 * 
 * @apiSuccessExample Response
 * 		Redirect to /a/:purl
 * 
 * @apiUse UserNotFoundError
 * @apiUse IncorrectHashError
 */
function verifyEmail(db){
	return [
		check("purl").custom(isPurl),
		check("hash").exists(),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
	
			db.models.user.findOne({
				where: { purl: params.purl },
				include: [ db.models.auth ],
			}).then(function(user){
				if(!user)
					throw new UserNotFoundError(params.purl);
	
				const auth = user.get("auth");
				if(auth.get("email_verification_hash") === params.hash){
					auth.update({
						email_verified: true,
					}).then(function(){
						res.redirect(`/a/${user.get("purl")}?email_verification_successful`);
					}).catch(next);
	
				}else{
					throw new IncorrectHashError(params.hash);
				}
			}).catch(next);
		}
	];
}

/**
 * @api {get} /users/:purl/avatar Get avatar
 * @apiName Get avatar
 * @apiGroup Users
 * 
 * @apiParam {String} purl The user's personal URL
 * 
 * @apiSuccessExample {image/jpeg} Response
 * 		HTTP/1.1 200 OK
 */
function getAvatar(db){
	return [
		check("purl").optional({checkFalsy: true}).custom(isPurl),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
			res.sendFile(req.avatarer.getUserAvatar(params.purl));
		}
	];
}

/**
 * @api {post} /users/:purl/avatar Set avatar
 * @apiName Set avatar
 * @apiGroup Users
 * 
 * @apiParam {String} purl The user's personal URL
 * @apiParam {File} image The new avatar
 * 
 * @apiSuccessExample Success
 * 		HTTP/1.1 200 OK
 * 
 * @apiUse UnauthorizedError
 */
function updateAvatar(db){
	return [
		check("purl").custom(isPurl),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();

			if(params.purl !== req.user.get("purl"))
				throw new UnauthorizedError();

			let form = new formidable.IncomingForm();
			form.type = "multipart";

			form.parse(req, function(err, fields, files){
				if(err)
					throw(err);
				else
					res.sendStatus(200);
			});

			form.on("file", function(name, file){
				// Move the file if it's under our max avatar size limit
				if(file.size <= process.env.APP_MAX_AVATAR_SIZE)
					req.avatarer.processUserAvatar(file, req.user.get("purl"));
			});
		}
	];
}

/**
 * @api {post} /users/:purl Update user
 * @apiName Update user
 * @apiGroup Users
 * 
 * @apiParam {String} purl The user's personal url
 * @apiParam {String} [first_name] First name value to update
 * @apiParam {String} [last_name] Last name value to update
 * 
 * @apiSuccess {String} email The user's email
 * @apiSuccess {Date} created_at The timestamp of when the user was created
 * @apiSuccess {Date} updated_at The timestamp of when the user was last updated
 * @apiSuccess {Date} dob The user's date of birth
 * @apiSuccess {String} first_name The user's first name
 * @apiSuccess {String} last_name The user's last name
 * @apiSuccess {String} purl The user's personal url
 * @apiSuccess {String} location The user's location
 * @apiSuccess {String} location_place_id The user's location place_id
 * @apiSuccess {String} country The user's country
 * @apiSuccess {String} locality The user's locality
 * @apiSuccess {String} job_title The user's job title
 * @apiSuccess {Number} score The user's personzone score
 * @apiSuccess {String} gender The user's gender
 * 
 * @apiSuccessExample {json} Response
 * 		HTTP/1.1 200 OK
 * 		{
 *   		"email": "pedro.dalannar.marques@gmail.com",
 *   		"created_at": "2018-03-19T13:35:58.000Z",
 *   		"updated_at": "2018-03-21T12:04:57.000Z",
 *   		"dob": null,
 *   		"first_name": "Pedro Daniel",
 *   		"last_name": "Magalhaes Marques",
 *   		"purl": "pedro456",
 *   		"location": null,
 *   		"location_place_id": null,
 *   		"country": null,
 *   		"locality": null,
 *   		"job_title": null,
 *   		"score": 0,
 *   		"gender": "O"
 * 		}
 *
 * @apiUse UnauthorizedError
 * @apiUse UserNotFoundError
 */
function updateUser(db){
	return [
		check("purl").custom(isPurl),
		check("name").optional({checkFalsy: true}).custom(isUserName),
		check("new_purl").optional({checkFalsy: true}).custom(isPurl),
		check("gender").optional({checkFalsy: true}).isIn(GENDERS),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();

			if(req.user.get("purl") !== params.purl)
				throw new UnauthorizedError();

			let valuesToUpdate = {};
			if(params.name)
				valuesToUpdate.name = params.name;

			if(params.new_purl)
				valuesToUpdate.purl = params.new_purl;

			if(params.gender)
				valuesToUpdate.gender = params.gender;

			db.models.user.update(valuesToUpdate, {
				where: { purl: params.purl },
				limit: 1,
			}).then(function(results){
				if(results[0] < 1)
					throw new UserNotFoundError(params.purl);		

				db.models.user.findOne({
					where: { purl: params.new_purl || params.purl }
				}).then(function(user){
					if(!user)
						throw new UserNotFoundError(params.purl);

					res.status(200).send(user.getData("owner"));
				}).catch(next);
			}).catch(next);
		}
	];
}

/**
 * @api {post} /users Create user
 * @apiName Create user
 * @apiGroup Users
 * 
 * @apiParam {String} name The name of the user
 * @apiParam {String} email The email of the user
 * @apiParam {String} password The user's password
 * 
 * @apiSuccess {String} purl The user's personal URL
 * 
 * @apiSuccessExample {json} Response
 * 		HTTP/1.1 201 Created
 * 		{
 * 			purl: "pedro.marques.9asdj202mq1s"
 * 		}
 * 
 * @apiUse EmailAlreadyExistsError 
 */
function createUser(db){
	return [
		check("name").exists().custom(isUserName),
		check("email").exists().custom(isEmail),
		check("password").exists(),

		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			db.models.user.create({
				name: params.name,
				email: params.email,
				purl: generatePurl(params.name),
				auth: {
					password: params.password
				},
				zones: [{
					type: "personal",
					name: "Personal",
					zurl: "personal",
				}]
			},{
				include: [ db.models.zone, db.models.auth ]
			}).then(function(user){
				winston.info(`Successfully registered a new user, ${user.get("purl")}`);

				// Set the default zone for the newly created user
				user.update({
					default_zone: user.zones[0].get("id")
				}).then(function(){
					req.login(user, function(err){
						if(err) winston.error(err);

						res.status(201).send({purl: user.get("purl")});
					});
				});

				req.mailer.sendTemplate("verifyEmail", {
					purl: user.get("purl"),
					hash: user.get("auth").get("email_verification_hash")
				},{
					to: user.get("email"),
				}).catch(next);
			}).catch(function(err){
				if(err instanceof db.UniqueConstraintError){
					if(err.errors[0].path === "email")
						return next(new EmailAlreadyExistsError(params.email));
				}

				return next(err);
			});
		}
	];
}

/**
 * @api {get} /users/ Search users
 * @apiName Search users
 * @apiGroup Users
 * 
 * @apiParam {String} [search=""] The search term to use when searching for users
 * @apiParam {Number} [limit=6] The limit of results to get
 * @apiParam {Number} [page=0] The page of results to get
 * 
 * @apiSuccess {String} first_name The first name of the user
 * @apiSuccess {String} last_name The last name of the user
 * @apiSuccess {String} purl The user's personal url
 * @apiSuccess {String} location The user's location
 * @apiSuccess {String} job_title The user's job title
 * 
 * @apiSuccessExample {json} Response
 * 		HTTP/1.1 200 OK
 * 		[
 *			{
 *				"first_name": "Pedro Daniel",
 *				"last_name": "Magalhaes Marques",
 *				"purl": "Pedro4341",
 *				"location": "Norwich",
 *				"job_title": "Junior"
 *			}
 *		]
 */
function searchUsers(db){
	return [
		check("search").optional({checkFalsy: true}),
		check("limit").optional({checkFalse: true}).isInt(),
		check("page").optional({checkFalsy: true}).isInt(),
		
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
		
			let searches = [""];
			if(params.search)
				searches = params.search.split(" ");
		
			let limit = Number(params.limit) || 6;
			let offset = Number(params.limit * params.page) || 0;
		
			let fnSearch = searches.map(function(o){ return { first_name: { $like: `%${o}%` } }; });
			let lnSearch = searches.map(function(o){ return { last_name: { $like: `%${o}%` } }; });
			let loSearch = searches.map(function(o){ return { location: { $like: `%${o}%` } }; });
			let jtSearch = searches.map(function(o){ return { job_title: { $like: `%${o}%` } }; });
			let puSearch = searches.map(function(o){ return { purl: { $like: `%${o}%` } }; });
		
			let concatenated = fnSearch.concat(lnSearch, loSearch, jtSearch, puSearch);

			db.models.user.findAll({
				attributes: ["first_name", "last_name", "purl", "location", "job_title"],
				where: {
					[sequelize.Op.or]: concatenated,
				},
				limit,
				offset,
			}).then(function(users){
				res.send(users);
			}).catch(next);
		}
	];
}