const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const LocaleNotFoundError = require("../../errors/LocaleNotFoundError");

module.exports = function(db){
	const router = express.Router();

	router.get("/set-locale/:locale", setLocale(db));

	return router;
};

/**
 * @api {get} /i18n/set-locale/:locale Set prefered locale
 * @apiName Set prefered locale
 * @apiGroup i18n
 * 
 * @apiParam {String} locale The locale to set as prefered
 * 
 * @apiSuccessExample
 * 		HTTP/1.1 200 OK
 * 
 * @apiUse LocaleNotFoundError
 */
function setLocale(db){
	return [
		check("locale").isAlpha(),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			try {
				req.localizer.setLocale(params.locale);
				res.sendStatus(200);
			} catch (e) {
				if(e instanceof ReferenceError)
					throw new LocaleNotFoundError(params.locale);
				
				next(e);
			}
		}
	];
}