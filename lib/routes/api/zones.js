const _ = require("lodash");
const express = require("express");
const winston = require("winston");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const zoneTypes = require("../../models/zonetypes");

const { isZurl, isPurl, isZoneName } = require("../../../util/validation");
const UnauthorizedError = require("../../errors/UnauthorizedError");
const UserNotFoundError = require("../../errors/UserNotFoundError");
const ZoneNotFoundError = require("../../errors/ZoneNotFoundError");
const ZoneAlreadyExistsError = require("../../errors/ZoneAlreadyExistsError");

module.exports = function(db){
	const router = express.Router();

	router.delete("/:zurl", deleteZone(db));
	router.get("/:zurl", getZone(db));
	router.post("/", createZone(db));

	return router;
};

/**
 * @api {delete} /zones/:zurl Delete zone
 * @apiName Delete zone
 * @apiGroup Zones
 * 
 * @apiParam {String} zurl The url of the zone
 * @apiParam {String} purl The personal URL of the user of the zone
 * 
 * @apiUse UnauthorizedError
 * @apiUse ZoneNotFoundError
 */
function deleteZone(db){
	return [
		check("zurl").exists().custom(isZurl),
		check("purl").exists().custom(isPurl),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
	
			if(!req.user)
				throw new UnauthorizedError();
	
			if(req.user.get("purl") !== params.purl)
				throw new UnauthorizedError();
	
			db.models.zone.findOne({
				where: { zurl: params.zurl, user_id: req.user.get("id") },
			}).then(function(zone){
				if(!zone)
					throw new ZoneNotFoundError();
	
				zone.destroy().then(function(){
					res.sendStatus(200);
				}).catch(next);
			}).catch(next);
		}
	];
}

/**
 * @api {get} /zones/:zurl Get zone
 * @apiName Get zone
 * @apiGroup Zones
 *
 * @apiParam {String} zurl The zone's url
 * @apiParam {String} purl The personal url of the user who owns the zone
 *
 * @apiSuccess {Number} id The zone's id
 * @apiSuccess {Number} user_id The id of the user that this zone belongs to
 * @apiSuccess {String} type The zone's type
 * @apiSuccess {String} name The zone's name
 * @apiSuccess {String} zurl The zone's url
 * @apiSuccess {Date} created_at The date when the zone was created
 * @apiSuccess {Date} updated_at The date when the zone was updated
 *
 * @apiSuccess {object[]} subzones
 * @apiSuccess {Number} subzones.id The id of the subzone
 * @apiSuccess {Number} subzones.zone_id The id of the zone this subzone belongs to
 * @apiSuccess {String} subzone.subzone The name of the subzone
 * @apiSuccess {Date} subzone.created_at The date when the subzone was created
 * @apiSuccess {Date} subzone.updated_at The date when the subzone was updated
 *
 * @apiSuccess {object[]} zonetraits
 * @apiSuccess {Number} zonetraits.id The id of the zonetrait
 * @apiSuccess {Number} zonetraits.zone_id The id of the zone this zonetrait belongs to
 * @apiSuccess {Number} zonetraits.trait_id The id of the trait this zonetrait represents
 * @apiSuccess {Date} zonetraits.created_at The date when the zonetrait was created
 * @apiSuccess {Date} zonetraits.updated_at The date when the zonetrait was updated
 *
 * @apiSuccess {object} zonetraits.trait
 * @apiSuccess {Number} zonetraits.trait.id The id of the trait
 * @apiSuccess {String} zonetraits.trait.trait The name of the trait
 * 
 * @apiUse ZoneNotFoundError
 */
function getZone(db){
	return [
		check("zurl").exists().custom(isZurl),
		check("purl").exists().custom(isPurl),
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
	
			db.models.zone.findOne({
				where: { zurl: params.zurl },
				include: [ db.models.subzone, {
					model: db.models.zonetrait,
					include: [ db.models.trait ],
				},{
					model: db.models.user,
					attributes: [],
					where: { purl: params.purl },
				}],
			}).then(function(zone){
				if(!zone)
					throw new ZoneNotFoundError();
	
				res.status(200).send(zone);
			}).catch(next);
		}
	];
}

/**
 * @api {post} /zones/ Create zone
 * @apiName Create zone
 * @apiGroup Zones
 *
 * @apiParam {String} type The type of zone to create
 * @apiParam {String} name The name for the zone
 * @apiParam {String} zurl The url for the zone
 * @apiParam {String} purl The personal URL of the user to own this zone
 *
 * @apiSuccess {Number} id The id of the zone
 * @apiSuccess {Number} user_id The id of the user the zone belongs to
 * @apiSuccess {String} type The type of the zone
 * @apiSuccess {String} name The name of the zone
 * @apiSuccess {String} zurl The url of the zone
 * @apiSuccess {Date} created_at The date when the zone was created
 * @apiSuccess {Date} updated_at The date when the zone was updated
 * 
 * @apiUse UnauthorizedError
 * @apiUse UserNotFoundError
 * @apiUse ZoneAlreadyExistsError
 */
function createZone(db){
	return [
		check("type").exists().isIn(zoneTypes),
		check("name").exists().custom(isZoneName),
		check("zurl").exists().custom(isZurl),
		check("purl").exists().custom(isPurl),
		
		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
	
			if(!req.user)
				throw new UnauthorizedError();
	
			if(req.user.get("purl") !== params.purl)
				throw new UnauthorizedError();
	
			db.models.user.findOne({
				where: { purl: params.purl }
			}).then(function(user){
				if(!user)
					throw new UserNotFoundError(params.purl);

				db.models.zone.create({
					type: params.type,
					name: params.name,
					zurl: params.zurl,
					user_id: user.get("id")
				}).then(function(zone){
					res.status(201).send(zone);
				}).catch(function(err){
					if(err instanceof db.UniqueConstraintError)
						next(new ZoneAlreadyExistsError());
					else
						next(err);
				});
			}).catch(next);
		}
	];
}