const express = require("express");
const _ = require("lodash");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { extractLocationDetails } = require("../../../util/gapi");
const { asyncMiddleware } = require("../../../util/express");

module.exports = function(db){
	const router = express.Router();

	router.get("/:trait_id", rankings(db));

	return router;
};

/**
 * @api {get} /:trait_id Get rankings
 * @apiName Get rankings
 * @apiGroup Rankings
 * 
 * @apiParam {number} trait_id The id of the trait to get rankings for, or 0 if for all
 * @apiParam {number} page
 * @apiParam {number} limit
 * @apiParam {string} order
 * @apiParam {string} place_id
 */
function rankings(db){
	return [
		check("trait_id").isInt(),
		check("page").optional({checkFalsy: true}).isInt(),
		check("limit").optional({checkFalsy: true}).isInt(),
		check("order").optional({checkFalsy: true}).isIn(["asc", "desc"]),
		check("place_id").optional({checkFalsy: true}),
		asyncMiddleware(async (req, res, next) => {
			validationResult(req).throw();
			const params = _.assign({}, {
				page: 0,
				limit: 10,
				order: "desc",
				place_id: null,
			}, matchedData(req));

			params.trait_id = _.toNumber(params.trait_id);
			let location = null;
			if(params.place_id){
				const placeDetails = (await req.gapi.place({placeid: params.place_id})).json.result;
				location = _.pick(extractLocationDetails(placeDetails), [
					"administrative_area_level_3", "administrative_area_level_2", "administrative_area_level_1",
					"locality", "country", "route",
				]);
			}

			const { count, rows } = await req.dbquery.getZonetraitRankings(db, {
				page: params.page,
				limit: params.limit,
				order: params.order,
				trait_id: params.trait_id,
				location,
			});

			let rankings = _.map(rows, function(row){
				let rank = row.get({plain: true});
				rank.zone.user.location = {};
				if(rank.zone.user.locations.length > 0)
					rank.zone.user.location = rank.zone.user.locations[0];

				delete rank.zone.user.locations;
				return rank;
			});

			res.send({count, rows, page: params.page, limit: params.limit, order: params.order, trait_id: params.trait_id, place_id: params.place_id});
		})
	];
}