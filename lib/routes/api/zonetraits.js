const _ = require("lodash");
const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const UnauthorizedError = require("../../errors/UnauthorizedError");
const ZoneNotFoundError = require("../../errors/ZoneNotFoundError");
const ZonetraitNotFoundError = require("../../errors/ZonetraitNotFoundError");

module.exports = function(db){
	const router = express.Router();

	router.post("/:id/rate", rateZonetrait(db));
	router.delete("/:id", deleteZonetrait(db));
	router.post("/", createZonetrait(db));

	return router;
};

/**
 * @api {post} /zonetraits/:id/rate Rate zonetrait
 * @apiName Rate zonetrait
 * @apiGroup Zonetraits
 *
 * @apiParam {Number} id The id of the zonetrait
 * @apiParam {Number} rating The (new) rating
 *
 * @apiSuccess {Number} id The id of the rating
 * @apiSuccess {Number} zonetrait_id The id of the usertrait
 * @apiSuccess {Number} rating The rating given
 * @apiSuccess {Date} created_at The timestamp of when the rating was created
 * @apiSuccess {Date} updated_at The timestamp of when the rating was updated
 * @apiSuccess {Number} user_id The id of the user who rated
 * 
 * @apiSuccessExample {json} Rating created
 * 		HTTP/1.1 201 Created
 * 		{
 *			"id": 3,
 *			"rating": 5,
 *			"created_at": "2017-10-23T10:43:50.000Z",
 *			"updated_at": "2017-10-23T10:43:54.975Z",
 *			"zonetrait_id": 3,
 *			"user_id": 2
 *		}
 * 
 * @apiSuccessExample {json} Rating updated
 * 		HTTP/1.1 200 OK
 * 		{
 *			"id": 3,
 *			"rating": 5,
 *			"created_at": "2017-10-23T10:43:50.000Z",
 *			"updated_at": "2017-10-23T10:43:54.975Z",
 *			"zonetrait_id": 3,
 *			"user_id": 2
 *		}
 * 
 * @apiUse UnauthorizedError
 * @apiUse ZonetraitNotFoundError
 */
function rateZonetrait(db){
	return [
		check("id").exists().isInt(),
		check("rating").exists().isInt({min: 1, max: 5}),

		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
			
			if(!req.user)
				throw new UnauthorizedError();
		
			db.models.zonetrait.findById(params.id, {
				include: [ db.models.zone ],
			}).then(function(zonetrait){
				if(!zonetrait)
					throw new ZonetraitNotFoundError(params.id);
		
				if(zonetrait.get("zone").get("user_id") === req.user.get("id"))
					throw new UnauthorizedError();
		
				db.models.rating.findOrCreate({
					where: {
						user_id: req.user.get("id"),
						zonetrait_id: zonetrait.get("id"), 
					}
				}).spread(function(rating, created){
					rating.update({
						rating: _.toNumber(params.rating)
					}).then(function(rating){
						zonetrait.set("rank_outdated", true);
						zonetrait.save().then(function(){
							res.status(created ? 201 : 200).send(rating);
						}).catch(next);
					}).catch(next);
				}).catch(next);
			}).catch(next);
		}
	];
}

/**
 * @api {delete} /zonetraits/:id Delete zonetrait
 * @apiName Delete zonetrait
 * @apiGroup Zonetraits
 *
 * @apiParam {Number} id The id of the zonetrait
 * 
 * @apiSuccessExample Response
 * 		HTTP/1.1 200 OK
 * 
 * @apiUse UnauthorizedError
 * @apiUse ZonetraitNotFoundError
 */
function deleteZonetrait(db){
	return [
		check("id").exists().isInt(),

		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);
			
			if(!req.user)
				throw new UnauthorizedError();
	
			db.models.zonetrait.findById(params.id, {
				include: [ db.models.zone ],
			}).then(function(zonetrait){
				if(!zonetrait)
					throw new ZonetraitNotFoundError(params.id);
	
				if(zonetrait.get("zone").get("user_id") !== req.user.get("id"))
					throw new UnauthorizedError();
	
				zonetrait.destroy(
				).then(function(){
					res.sendStatus(200);
				}).catch(next);
			}).catch(next);
		}
	];
}

/**
 * @api {post} /zonetraits Create zonetrait
 * @apiName Create zonetrait
 * @apiGroup Zonetraits
 *
 * @apiParam {Number} trait_id The id of the trait
 * @apiParam {Number} zone_id The id of the zone
 *
 * @apiSuccess {Number} id The id of the zonetrait that was created
 * @apiSuccess {Number} trait_id The id of the trait
 * @apiSuccess {Number} zone_id The id of the zone
 * @apiSuccess {Date} created_at The timestamp of when the zonetrait was created
 * @apiSuccess {Date} updated_at The timestamp of when the zonetrait was updated
 * 
 * @apiSuccess {Object} trait
 * @apiSuccess {Number} trait.id The id of the trait
 * @apiSuccess {String} trait.trait The name of the trait
 * 
 * @apiSuccessExample {json} Zonetrait created
 * 		HTTP/1.1 201 Created
 * 		{
 *			"id": 5,
 *			"created_at": "2017-10-23T10:20:17.000Z",
 *			"updated_at": "2017-10-23T10:20:17.000Z",
 *			"trait_id": 26,
 *			"zone_id": 1,
 *			"trait": {
 *				"id": 26,
 *				"trait": "Cheerful"
 *			}
 *		}
 * 
 * @apiSuccessExample {json} Zonetrait already exists
 * 		HTTP/1.1 200 OK
 * 		{
 *			"id": 5,
 *			"created_at": "2017-10-23T10:20:17.000Z",
 *			"updated_at": "2017-10-23T10:20:17.000Z",
 *			"trait_id": 26,
 *			"zone_id": 1,
 *			"trait": {
 *				"id": 26,
 *				"trait": "Cheerful"
 *			}
 *		}
 * 
 * @apiUse ZoneNotFoundError
 * @apiUse UnauthorizedError
 */
function createZonetrait(db){
	return [
		check("trait_id").exists().isInt(),
		check("zone_id").exists().isInt(),

		function(req, res, next){
			validationResult(req).throw();
			const params = matchedData(req);

			if(!req.user)
				throw new UnauthorizedError();
	
			db.models.zone.findById(params.zone_id, {
			}).then(function(zone){
				if(!zone)
					throw new ZoneNotFoundError(params.zone_id);

				if(zone.get("user_id") !== req.user.get("id"))
					throw new UnauthorizedError();

				db.models.zonetrait.findOrCreate({
					where: { zone_id: zone.get("id"), trait_id: params.trait_id },
					include: [ db.models.trait ],
				}).spread(function(zonetrait, created){

					// If this is not included we can't send information about the trait back to the client
					// (If the zonetrait doesn't exist and is created 'zonetrait' won't hold information about the trait table)
					db.models.zonetrait.findById(zonetrait.get("id"), {
						include: [ db.models.trait ],
					}).then(function(zt){
						res.status(created ? 201 : 200).send(zt);
					}).catch(next);
				}).catch(next);
			}).catch(next);
		}
	];
}