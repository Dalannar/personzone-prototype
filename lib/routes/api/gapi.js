const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

const { asyncMiddleware } = require("../../../util/express");

module.exports = function(db){
	const router = express.Router();

	router.get("/place", place());
	router.get("/placesAutoComplete", placesAutoComplete());

	return router;
};

/**
 * @api {get} /gapi/places Get place details
 * @apiName Get place details
 * @apiGroup Gapi
 * 
 * @apiParam {string} place_id The place_id of the place to get details about
 * @todo Add response examples here!
 */
function place(){
	return [
		check("place_id").exists(),
		asyncMiddleware(async (req, res, next) => {
			validationResult(req).throw();
			const params = matchedData(req);

			res.send(await req.gapi.place({
				placeid: params.place_id
			}));
		})
	];
}

/**
 * @api {get} /gapi/placesAutoComplete Predict place autocomplete
 * @apiName Predict places autocomplete
 * @apiGroup Gapi
 * 
 * @apiParam {string} input The input to predict places with
 * @todo Add response examples here!
 */
function placesAutoComplete(){
	return [
		check("input").exists(),
		asyncMiddleware(async (req, res, next) => {
			validationResult(req).throw();
			const params = matchedData(req);

			res.send(await req.gapi.placesAutoComplete({
				input: params.input
			}));
		})
	];
}