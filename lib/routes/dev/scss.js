const express = require("express");

const { check, validationResult } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");

module.exports = function(){
	const router = express.Router();

	router.get("/:page", [
		check("page"),
		(req, res, next) => {
			validationResult(req).throw();
			const params = matchedData(req);

			res.render(`dev/scss/${params.page}`);
		}
	]);

	return router;
};