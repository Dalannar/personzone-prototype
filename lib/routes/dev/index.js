const express = require("express");

const { check } = require("express-validator/check");

module.exports = function(db){
	const router = express.Router();

	const createScssRouter = require("./scss");
	router.use("/scss", createScssRouter());

	router.get("/util-gapi", (req, res, next) => {
		res.render("dev/util-gapi");
	});

	return router;
};