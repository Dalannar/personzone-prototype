const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const expressSession = require("express-session");

const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

const mailer = require("./middleware/mailer");
const errorHandler = require("./middleware/errorHandler");
const localizer = require("./middleware/localizer");
const avatarer = require("./middleware/avatarer");
const dbquery = require("./middleware/dbquery");
const gapi = require("./middleware/gapi");

module.exports = function(db){

	const server = express();

	server.use("/public", express.static("public"));

	server.set("views", "./views");
	server.set("view engine", "pug");

	// SMTP mailing service
	server.use(mailer({
		port: process.env.MAILER_PORT,
		host: process.env.MAILER_HOST,
		auth: {
			user: process.env.MAILER_USER,
			pass: process.env.MAILER_PASS,
		}
	}, {
		from: `Personzone ${process.env.MAILER_USER}`,
	}));

	// i18n
	server.use(localizer({
		localeDir: `${__dirname}/../locales`,
		defaultLocale: "en"
	}));

	// Avatarer
	server.use(avatarer({
		userAvatarPath: global.__userAvatarPath,
		placeholderAvatarPath: global.__imagePath,
	}));

	// DBQuery
	server.use(dbquery());

	// Google API
	server.use(gapi({
		API_KEY: process.env.GOOGLE_MAPS_API_KEY
	}));

	server.use(cookieParser());
	server.use(bodyParser.json());
	server.use(bodyParser.urlencoded({extended: true}));

	server.use(expressSession({
		secret: process.env.SERVER_SESSION_SECRET,
		resave: false,
		saveUninitialized: true,
	}));

	// Authentication with passport
	server.use(passport.initialize());
	server.use(passport.session());

	// req.user will be equal to undefined or an object with the user table (including auth table inside (.get("auth")))
	passport.serializeUser(function(user, done){
		done(null, user.get("id"));
	});

	passport.deserializeUser(function(user, done){
		db.models.user.findById(user, {
			include: [ db.models.auth ]
		}).then(function(user){
			done(null, user);
		}).catch(function(err){
			done(err);
		});
	});

	passport.use(new LocalStrategy({
		usernameField: "email",
		passwordField: "password",
	}, function(email, password, done){
		db.models.user.findOne({
			where: { email },
			include: [ db.models.auth ],
		}).then(function(user){
			if(!user) return done(null, false);
			if(!user.get("auth").validatePassword(password)) return done(null, false);
			return done(null, user);
		}).catch(function(err){
			return done(err);
		});
	}));

	const createApiRouter = require("./routes/api");
	server.use("/api", createApiRouter(db));
	
	const createAppRouter = require("./routes/app");
	server.use("/", createAppRouter(db));

	const createDevRouter = require("./routes/dev");
	server.use("/dev", createDevRouter(db));
	
	// Error handling
	server.use(errorHandler({
		printStackTrace: process.env.NODE_ENV === "development",
		printValidationErrors: process.env.NODE_ENV === "development",
		logErrorsToConsole: true,
	}));

	return server;
};