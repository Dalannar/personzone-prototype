const inquirer = require("inquirer");

function getBar(){
	return new Promise(resolve => {
		setTimeout(() => resolve("foo"), 5000);
	});
}

function getFoo(){
	return new Promise(resolve => {
		setTimeout(() => resolve("foo"), 5000);
	});
}

async function getOptions(){
	const answers = await inquirer.prompt([{
		name: "number",
		type: "input",
		message: "Number of users to generate",
		default: 1000,
	}]);

	return {
		number: Number(answers.number)
	};
}

async function main(){
	const startTime = new Date();
	//const foo = await getFoo();
	//const bar = await getBar();
	//const [foo, bar] = await Promise.all([getBar(), getFoo()]);
	const options = await getOptions();
	const endTime = new Date();
	const timeDiff = endTime - startTime;
	console.log("elapsed seconds", Math.round(timeDiff/1000));
	//console.log("foo", foo);
	//console.log("bar", bar);
	console.log("options", options);
}

main();