import { h, render } from "preact";
import _ from "lodash";

import TranslationProvider from "../pz-util/TranslationProvider";

import CreateZonetraitModal from "./CreateZonetraitModal";
import DeleteZonetraitModal from "./DeleteZonetraitModal";
import ZonetraitRater from "./ZonetraitRater";

document.addEventListener("DOMContentLoaded", function(){
	const zonetraitLists = document.getElementsByClassName("zonetrait-list");

	_.forEach(zonetraitLists, function(el){
		const enableRating = Boolean(el.getAttribute("data-enableRating"));
		const enableCreating = Boolean(el.getAttribute("data-enableCreating"));
		const enableDeleting = Boolean(el.getAttribute("data-enableDeleting"));

		const listContainer = el.getElementsByClassName("zonetraits")[0];
		const zonetraitElements = el.getElementsByClassName("zonetrait");
		
		
		_.forEach(zonetraitElements, function(z){
			const id = z.getAttribute("data-id");
			const rating = z.getAttribute("data-rating");
			const totalRatings = z.getAttribute("data-totalRatings");

			render((
				<ZonetraitRater {...{id, enableRating, rating, totalRatings}} />
			), z.getElementsByClassName("rater")[0]);
		});

		if(enableCreating){
			const openButton = el.getElementsByClassName("create-zonetrait-button")[0];
			const zoneId = el.getAttribute("data-zoneId");
			const existingTraits = _.map(zonetraitElements, o => o.getAttribute("data-name"));
			render((
				<TranslationProvider locale={document.getElementById("locale_create-zonetrait-modal")}>
					<CreateZonetraitModal {...{openButton, zoneId, existingTraits}}/>
				</TranslationProvider>
			), el.getElementsByClassName("create-zonetrait-modal")[0]);
		}

		if(enableDeleting){
			render((
				<TranslationProvider locale={document.getElementById("locale_delete-zonetrait-modal")}>
					<DeleteZonetraitModal listContainer={listContainer} />
				</TranslationProvider>
			), el.getElementsByClassName("delete-zonetrait-modal")[0]);
		}
	});
});