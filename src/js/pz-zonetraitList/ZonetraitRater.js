import { h, Component } from "preact";
import Rater from "../components/Rater";

import { rateZonetrait } from "../pz-util/api/zonetraits";

function rate(id, rating){
	rateZonetrait(id, rating)
		.catch(function(){
			console.error("Failed to submit rating");
		});
}

/**
 * @param {Number} props.id The id of the zonetrait to rate
 * @param {Number} props.rating The initial rating of the zonetrait
 * @param {Number} props.totalRatings The number of ratings already made on the zonetrait
 * @param {Bool} [props.enableRating=false] If true will allow the user to rate
 */
export default function(props){
	const { id, rating, totalRatings, enableRating } = props;
	return (
		<div style={{display: "flex", justifyContent: "flex-start", alignItems: "center"}}>
			<Rater
				initialRating={Math.round(rating)}
				enableRating={enableRating}
				onRate={rating => enableRating ? rate(id, rating) : null}
			/>
			{totalRatings &&
				<div style={{marginLeft: 8}}>({totalRatings})</div>
			}
		</div>
	);
}