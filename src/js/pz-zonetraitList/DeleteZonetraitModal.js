import { h, Component } from "preact";
import Translatable from "../pz-util/Translatable";

import { deleteZonetrait } from "../pz-util/api/zonetraits";

import { ZONETRAIT_DELETE_DELAY } from "../pz-util/constants";

/**
 * @property {Bool} this.state.open The current state of the modal
 * @property {String} this.state.name The name of the currently selected zonetrait
 * @property {Number} this.state.id The id of the currently selected zonetrait
 * @property {String} this.state.error An error message to display, if any
 * @property {Bool} this.state.loading Whether or not a delete request has been sent to the server
 * 
 * @param {HTMLElement} this.props.listContainer The HTML element representing the container for the zonetrait list
 * @param {Func} this.props.t Translation function
 */
class DeleteZonetraitModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,
			name: "",
			id: null,
			error: "",
			loading: false,
		};

		this.baseState = Object.assign({}, this.state);

		const _this = this;
		this.props.listContainer.addEventListener("click", function(e){
			if(e.target.classList.contains("delete-button")){
				const z = e.target.closest(".zonetrait");
				_this.open(z.getAttribute("data-name"), z.getAttribute("data-id"));
			}
		});
	}

	/**
	 * @override
	 */
	render(props, state){
		const { t } = props;
		const { open, name, error, loading } = state;

		return (
			<div className={`modal ${open && "is-active"}`}>
				<div className="modal-background" onClick={this.close.bind(this)} />
				<div className="modal-content">
					<div className="card">
						<header className="card-header">
							<p className="card-header-title">{name}</p>
						</header>

						<div className="card-content">
							<div className="content">
								<p>{t("caption", {name})}</p>
								{error && 
									<p className="has-text-danger">{error}</p>
								}
							</div>

							<div className="level">
								<div className="level-left">
									<button className="button"
										onClick={this.close.bind(this)}
									>
										{t("close")}
									</button>
								</div>
								<div className="level-right">
									<button className={`button is-danger ${loading && "is-loading"}`}
										disabled={!this.canDelete()}
										onClick={this.delete.bind(this)}
									>
										{t("delete")}
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	/**
	 * Open the modal with a specified zonetrait selected
	 * @param {String} name	The name of the zonetrait
	 * @param {Number} id The id of the zonetrait
	 */
	open(name, id){
		this.setState({open: true, name, id});
	}

	/**
	 * Close the modal
	 */
	close(){
		this.setState(this.baseState);
	}

	/**
	 * @returns {Bool} true if the zonetrait can be deleted, false otherwise
	 */
	canDelete(){
		if(
			!this.state.id ||
			!this.state.name ||
			this.state.error ||
			this.state.loading
		){
			return false;
		}

		return true;
	}

	/**
	 * Delete the currently selected zonetrait (this.state.id)
	 * If successful reloads the page
	 */
	delete(){
		if(!this.canDelete())
			return;

		const { id } = this.state;
		const _this = this;

		this.setState({loading: true});
		deleteZonetrait(id)
			.then(function(){
				setTimeout(() => window.location.reload(), ZONETRAIT_DELETE_DELAY);
			}).catch(function(){
				_this.setState({loading: false, error: _this.props.t("unknown_error")});
			});
	}
	
}

export default Translatable(DeleteZonetraitModal);