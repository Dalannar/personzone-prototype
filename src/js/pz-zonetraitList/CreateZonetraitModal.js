import { h, Component } from "preact";
import AutoComplete from "../pz-util/AutoComplete";
import Translatable from "../pz-util/Translatable";

import { searchTraits, getTrait } from "../pz-util/api/traits";
import { createZonetrait } from "../pz-util/api/zonetraits";

import { ZONETRAIT_CREATION_DELAY } from "../pz-util/constants";

/**
 * @property {Bool} this.state.open The current state of the modal
 * @property {String} this.state.id The id of the currently "selected" trait
 * @property {String} this.state.error An error message to display, if any
 * @property {String[]} this.state.traitResults An array with names of traits
 * @property {Bool} this.state.loading Whether or not a creation request has been sent to the server
 * 
 * @param {HTMLElement} this.props.openButton The HTML element that represent the button that opens the modal
 * @param {String[]} this.props.existingTraits An array of names of already existing traits in the zone
 * @param {Number} this.props.zoneId The id of the zone to create zonetraits for
 * @param {Func} this.props.t The translation function
 */
class CreateZonetraitModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,
			id: null,
			error: "",
			traitResults: [],
			loading: false,
		};

		this.baseState = Object.assign({}, this.state);
		this.props.openButton.addEventListener("click", this.open.bind(this));
	}

	/**
	 * @override
	 */
	render(props, state){
		const { open, traitResults, error, loading } = state;
		const { t } = props;

		return (
			<div className={`modal ${open && "is-active"}`}>
				<div className="modal-background" onClick={this.close.bind(this)}/>
				<div className="modal-content" style={{overflow: "visible"}}>
					<div className="card">
						<div className="card-content">
							<div className="field is-grouped">
								<div className="control is-expanded">
									<AutoComplete 
										results={traitResults}
										onChange={e => this.setTrait(e.value)}
										onInput={e => this.searchTraits(e.value)}
										onRequestClearResults={this.clearTraitResults.bind(this)}
										error={error}
									/>
								</div>
								<div className="control">
									<button className="button" onClick={this.close.bind(this)}>{t("close")}</button>
								</div>
								<div className="control">
									<button className={`button is-primary ${loading && "is-loading"}`}
										disabled={!this.canCreate()}
										onClick={this.create.bind(this)}
									>
										{t("create")}
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		);
	}

	/**
	 * Open the modal
	 */
	open(){
		this.setState({open: true});
	}

	/**
	 * Close the modal
	 */
	close(){
		this.setState(this.baseState);
	}

	/**
	 * @returns {Bool} true if the trait can be created, false otherwise
	 */
	canCreate(){
		if(
			!this.state.id ||
			this.state.error ||
			this.state.loading
		){
			return false;
		}

		return true;
	}

	/**
	 * Set a creation request to the server for the currently "selected" trait
	 * If successful reloads the page
	 */
	create(){
		if(!this.canCreate())
			return;

		const { zoneId } = this.props;
		const { id } = this.state;
		const _this = this;

		this.setState({loading: true});
		createZonetrait(zoneId, id)
			.then(function(){
				setTimeout(() => window.location.reload(), ZONETRAIT_CREATION_DELAY);
			}).catch(function(){
				_this.setState({loading: false, error: _this.props.t("unknown_error")});
			});
	}

	/**
	 * Set the currently selected trait
	 * @param {String} name The name of the trait to select
	 */
	setTrait(name){
		if(!name)
			return this.setState({id: null, error: ""});
			
		const existingTraits = this.props.existingTraits.map(o => o.toLowerCase());
		if(existingTraits.indexOf(name.toLowerCase()) !== -1)
			return this.setState({id: null, error: this.props.t("duplicate_trait_error")});

		const _this = this;
		getTrait(name)
			.then(function(results){
				_this.setState({id: results.data.id, error: ""});
			}).catch(function(){
				_this.setState({id: "", error: _this.props.t("invalid_trait_error")});
			});
	}

	/**
	 * Clear the trait search results
	 */
	clearTraitResults(){
		this.setState({traitResults: []});
	}

	/**
	 * Search for traits based based on name
	 * @param {String} search The term to search traits by
	 */
	searchTraits(search){
		const _this = this;
		searchTraits(search, 6)
			.then(function(results){
				_this.setState({traitResults: results.data.map(o => o.name)});
			});
	}
}

export default Translatable(CreateZonetraitModal);