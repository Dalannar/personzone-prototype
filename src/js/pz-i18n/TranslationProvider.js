// React imports
import React from "react";
import PropTypes from "prop-types";

// Other imports
import Polyglot from "node-polyglot";
import _ from "lodash";

/**
 * A translation provider for it's react children
 * @property {object} this.polyglot The polyglot instance used for translation
 * @param {object} this.props.translations An object representing all of the translations available in the application.
 * This object is supplied to polyglot
 */
class TranslationProvider extends React.Component{
	constructor(props){
		super(props);

		this.polyglot = new Polyglot({locale: this.props.locale});
		this.polyglot.replace(this.props.translations);

		this.subscribers = {};
		this.subCount = 0;
	}

	subscribe(f){
		this.subCount = this.subCount + 1;
		this.subscribers[this.subCount] = f;
		return this.subCount;
	}

	unsubscribe(count){
		delete this.subscribers[count];
	}

	getChildContext(){
		return { 
			pz_i18n_polyglot: this.polyglot,
			pz_i18n_subscribe: this.subscribe.bind(this),
			pz_i18n_unsubscribe: this.unsubscribe.bind(this)
		};
	}

	componentWillReceiveProps(nextProps){
		if(
			!_.isEqual(this.props.translations, nextProps.translations) ||
			!_.isEqual(this.props.locale, nextProps.locale)
		){
			this.polyglot.replace(nextProps.translations);
			this.polyglot.locale(nextProps.locale);
			_.forEach(this.subscribers, f => f());
		}
	}

	render(){
		return React.Children.only(this.props.children);
	}
}

TranslationProvider.childContextTypes = {
	pz_i18n_polyglot: PropTypes.object.isRequired,
	pz_i18n_subscribe: PropTypes.func.isRequired,
	pz_i18n_unsubscribe: PropTypes.func.isRequired,
};

TranslationProvider.propTypes = {
	locale: PropTypes.string.isRequired,
	translations: PropTypes.object.isRequired,
};

export default TranslationProvider;