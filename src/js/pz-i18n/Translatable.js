// React imports
import React from "react";
import PropTypes from "prop-types";

import _ from "lodash";

/**
 * Wrap the supplied component in a translation wrapper. The wrapped component received 'this.props.t'
 * which reflects the polyglot translation function
 * @param {Component} WrappedComponent The component to wrap in the translation component
 * @param {string} defaultTranslationPrefix The prefix for all translations in the wrapped components
 * This prefix is added to all calls to 't' on the wrapped component. Adds '.' to the end of the prefix
 */
export default function(WrappedComponent, defaultTranslationPrefix){
    const dtp = defaultTranslationPrefix ? `${defaultTranslationPrefix}.` : "";

    class Translatable extends React.Component{
        constructor(props){
            super(props);

            this.subCount = undefined;
        }

        componentDidMount(){
            this.subCount = this.context.pz_i18n_subscribe(this.onTranslationUpdate.bind(this));
        }

        componentWillUnmount(){
            this.context.pz_i18n_unsubscribe(this.subCount);
        }

        onTranslationUpdate(){
            this.forceUpdate();
        }

        render(){
            return <WrappedComponent t={this.t.bind(this)} {...this.props} />
        }

        t(phrase, params, ...args){
            params = _.assign({"_": `mt: ${phrase}`}, params);
            return this.context.pz_i18n_polyglot.t(`${dtp}${phrase}`, params, ...args);
        }
    }

    Translatable.contextTypes = {
        pz_i18n_polyglot: PropTypes.object.isRequired,
        pz_i18n_subscribe: PropTypes.func.isRequired,
        pz_i18n_unsubscribe: PropTypes.func.isRequired,
    }

    Translatable.displayName = `Translatable(${getDisplayName(WrappedComponent)})`;

    return Translatable;
}

function getDisplayName(WrappedComponent){
    return WrappedComponent.displayName || WrappedComponent.name || "Component";
}