import axios from "axios";

/**
 * Google maps api request to "placesAutoComplete"
 * @param {String} input The input to search
 * @returns {Promise} axios promise
 */
export function placesAutoComplete(input){
	return axios({
		method: "get",
		url: "/api/gapi/placesAutoComplete",
		params: { input }
	});
}