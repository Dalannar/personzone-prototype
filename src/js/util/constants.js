export default {

	// Delay for registering a new user, in milliseconds
	REGISTER_DELAY: 1500,
};