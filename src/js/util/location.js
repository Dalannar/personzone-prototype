/**
 * Update a query string (location search) by updating an existing key or creating it entirely
 * Shamelessly stolen from stackoverflow (https://stackoverflow.com/questions/5999118/how-can-i-add-or-update-a-query-string-parameter)
 * @param {String} uri The query to update (should be location.search)
 * @param {String} key The key to update or create if it doesn't exist
 * @param {*} value The value to insert
 * @returns {String} updated uri to set location.search to
 */
export function updateQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf("?") !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, "$1" + key + "=" + value + "$2");
	}
	else {
		return uri + separator + key + "=" + value;
	}
}