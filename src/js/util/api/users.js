import axios from "axios";

/**
 * Create a user
 * @param {String} name The user's name
 * @param {String} email The user's email
 * @param {String} password The user's password
 * @returns {Promise} Axios promise
 */
export function createUser(name, email, password){
	return axios({
		method: "post",
		url: "/api/users",
		data: { name, email, password }
	});
}