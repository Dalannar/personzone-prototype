import axios from "axios";

/**
 * Check if an email is available
 * @param {String} email The email to check availability for
 * @returns {Promise} Axios promise
 */
export function isEmailAvailable(email){
	return axios({
		method: "get",
		url: `/api/availability/email/${email}`
	});
}