import axios from "axios";

export function placesAutoComplete(input){
	return axios({
		method: "get",
		url: "/api/gapi/placesAutoComplete",
		params: { input }
	});
}