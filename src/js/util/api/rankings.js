import axios from "axios";

export function getRankings({trait_id, page, limit, order, place_id}){
	return axios({
		method: "get",
		url: `/rankings/${trait_id}`,
		params: {
			page, limit, order, place_id,
			render_results_html: true,
		}
	});
}