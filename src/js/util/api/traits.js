import axios from "axios";

export function searchTraits(search, limit){
	return axios({
		method: "get",
		url: "/api/traits",
		params: {search, limit}
	});
}