export default {
	// The delay to delete a zonetrait, in milliseconds
	ZONETRAIT_DELETE_DELAY: 200,
	// The delay to create a zonetrait, in milliseconds
	ZONETRAIT_CREATION_DELAY: 200,

	// The delay to delete a subzone, in milliseconds
	SUBZONE_DELETION_DELAY: 200,
	// The delay to create a subzone, in milliseconds
	SUBZONE_CREATION_DELAY: 200,
};