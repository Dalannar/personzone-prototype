import { h, Component } from "preact";

/**
 * @property {Bool} this.state.open
 * @property {String} this.state.input
 * @property {Number} this.state.activeResult
 * 
 * @param {String} this.props.initialValue The initial value that the input should have
 * @param {String} this.props.error An error to display if there is one, otherwise it is an empty string
 * @param {String[]} this.props.results An array of results to show in the autocomplete dropdown
 * @param {Func} this.props.onRequestClearResults A callback function called whenever results should be cleared
 * @param {Func} this.props.onChange A callback function called whenever the input value is changed or the input loses focus
 * @param {Func} this.props.onInput A callback function called whenever there is new input and results should be recalculated
 */
export default class AutoComplete extends Component{
	constructor(props){
		super(props);
		
		this.state = {
			open: false,
			value: props.initialValue || "",
			activeResult: 0,
		};

		this.baseState = Object.assign({}, this.state);
		this.rootRef = null;

		document.addEventListener("click", this.attemptClose.bind(this));
	}

	render(props, state){
		const { open, activeResult, value } = state;
		const { results, error } = props;
		const onResultClick = this.onResultClick.bind(this);

		return (
			<div 
				ref={ref => this.rootRef = ref} 
				className={`dropdown ${open && "is-active"}`}
				onKeyDown={this.onKeyPress.bind(this)}
				style={{width: "100%"}}
			>
				<div className="dropdown-trigger" style={{width: "100%"}}>
					<div className="field">
						<div className="control">
							<input
								autofocus
								value={value}
								onClick={this.open.bind(this)}
								onFocus={this.open.bind(this)}
								onInput={this.onInput.bind(this)}
								onChange={this.onChange.bind(this)}
								className={`input ${open && "is-focused"} ${error && "is-danger"}`}
							/>
						</div>
						{error &&
							<p className="help is-danger">{error}</p>
						}
					</div>
				</div>
				<div className="dropdown-menu" style={{width: "100%"}} >
					<div className="dropdown-content">
						{results.map(function(name, index){
							return (
								<a onClick={() => onResultClick(name)} className={`dropdown-item ${index === activeResult && "is-active"}`}>{name}</a>
							);
						})}

						{(results.length < 1 && value) &&
							<div className="dropdown-item">No results...</div>
						}

						{(results.length < 1 && !value) &&
							<div className="dropdown-item">Start typing to search...</div>
						}
					</div>
				</div>
			</div>
		);
	}

	open(){
		this.setState({open: true});
	}

	close(){
		this.setState({open: false, activeResult: 0});
	}

	attemptClose(){
		if(this.rootRef){
			if(!this.rootRef.contains(document.target) && !this.rootRef.contains(document.activeElement)){
				this.close();
				this.props.onChange({value: this.state.value});
			}
		}
	}

	onInput(e){
		if(e.target.value){
			this.open();
			this.setState({value: e.target.value});
			this.props.onInput({value: e.target.value});
		}else{
			this.close();
			this.setState({value: e.target.value});
			this.props.onRequestClearResults();
		}
	}

	onChange(e){
		this.setState({value: e.target.value});
		this.props.onChange({value: e.target.value});
	}

	onResultClick(result){
		this.setState({value: result});
		this.props.onChange({value: result});
		this.close();
	}

	onKeyPress(e){
		const k = e.keycode || e.which;
		
		if(k === 38 || k === 40){
			e.preventDefault();
			let newActive = k === 38 ? this.state.activeResult - 1 : this.state.activeResult + 1;
			if(newActive > this.props.results.length - 1)
				newActive = 0;
			else if(newActive < 0)
				newActive = this.props.results.length - 1;

			this.setState({activeResult: newActive});
		}else if(k === 13){
			e.preventDefault();
			const value = this.props.results[this.state.activeResult];
			this.props.onChange({value});
			this.setState({value});
			this.close();
		}
	}
}