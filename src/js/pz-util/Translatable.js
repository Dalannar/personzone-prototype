import { h, Component } from "preact";

export default function(WrappedComponent){
	class Translatable extends Component{
		render(props){
			return <WrappedComponent t={this.context.t} {...props} />;
		}
	}

	Translatable.displayName = `Translatable(${getDisplayName(WrappedComponent)})`;
	return Translatable;
}

function getDisplayName(WrappedComponent){
	return WrappedComponent.displayName || WrappedComponent.name || "Component";
}