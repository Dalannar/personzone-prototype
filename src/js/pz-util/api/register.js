import axios from "axios";

export function checkPurlAvailable(purl){
	return axios({
		method: "get",
		url: `/api/availability/purl/${purl}`
	});
}

export function checkEmailAvailable(email){
	return axios({
		method: "get",
		url: `/api/availability/email/${email}`
	});
}

export function register(first_name, last_name, email, password, purl, location, job_title, dob, gender){
	return axios({
		method: "post",
		url: "/api/users",
		data: { first_name, last_name, email, password, purl, location, job_title, dob, gender }
	});
}