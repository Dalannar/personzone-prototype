import axios from "axios";

export function createComment(subzone_id, commenter_purl, comment){
	return axios({
		url: "/api/comments",
		method: "post",
		data: { subzone_id, commenter_purl, comment }
	});
}

/**
 * Thank a comment
 * @param {Number} id The id of the comment to thank
 * @returns {Promise} axios promise
 */
export function thankComment(id){
	return axios({
		url: `/api/comments/${id}/thank`,
		method: "get",
	});
}