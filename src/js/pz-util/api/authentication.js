import axios from "axios";

export function login(email, password){
	return axios({
		method: "post",
		url: "/api/auth/login",
		data: { email, password }
	});
}