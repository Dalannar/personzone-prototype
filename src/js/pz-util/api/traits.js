import axios from "axios";

/**
 * Search for traits based on the given "search" term
 * @param {String} search The search term to search for traits with
 * @param {Number} limit The number of results to return
 * @returns {Promise} Axios promise
 */
export function searchTraits(search, limit){
	return axios({
		url: "/api/traits",
		method: "get",
		params: {search, limit},
	});
}

/**
 * Get trait by name
 * @param {String} name The name of the trait to get
 * @returns {Promise} Axios promise
 */
export function getTrait(name){
	return axios({
		url: "/api/traits",
		method: "get",
		params: { name }
	});
}