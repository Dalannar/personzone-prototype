import axios from "axios";

export function createZone(type, name, zurl, purl){
	return axios({
		method: "post",
		url: "/api/zones",
		data: { type, name, zurl, purl }
	});
}