import axios from "axios";

/**
 * Create a subzone
 * @param {String} subzone The name of the subzone to create
 * @param {Number} zone_id The id of the zone to create the subzone in
 */
export function createSubzone(subzone, zone_id){
	return axios({
		url: "/api/subzones",
		method: "post",
		data: { subzone, zone_id }
	});
}

/**
 * Delete a subzone
 * @param {Number} id The id of the subzone to delete
 */
export function deleteSubzone(id){
	return axios({
		url: `/api/subzones/${id}`,
		method: "delete",
	});
}