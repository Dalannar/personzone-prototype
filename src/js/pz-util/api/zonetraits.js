import axios from "axios";

/**
 * Create a zonetrait
 * @param {Number} zone_id The id of the zone to create the zonetrait for
 * @param {Number} trait_id The id of the trait to create
 * @returns {Promise} Axios promise
 */
export function createZonetrait(zone_id, trait_id){
	return axios({
		url: "/api/zonetraits",
		method: "post",
		data: { zone_id, trait_id }
	});
}

/**
 * Delete a zonetrait
 * @param {Nmber} id The id of the zonetrait to delete 
 * @returns {Promise} Axios promise
 */
export function deleteZonetrait(id){
	return axios({
		url: `/api/zonetraits/${id}`,
		method: "delete"
	});
}

/**
 * Rate a zonetrait
 * @param {Number} id The Id of the zonetrait to rate
 * @param {Number} rating The rating to give to the zonetrait
 * @returns {Promise} Axios promise
 */
export function rateZonetrait(id, rating){
	return axios({
		url: `/api/zonetraits/${id}/rate`,
		method: "post",
		data: { rating },
	});
}