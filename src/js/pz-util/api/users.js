import axios from "axios";

export function searchUsers(search, limit, page){
	return axios({
		method: "get",
		url: "/api/users/",
		params: { search, limit, page }
	});
}

/**
 * Update a user's details
 * @param {String} purl The user's personal url
 * @param {Object} options The options to update
 * @param {String} [options.first_name] The new first name of the user
 * @param {String} [options.last_name] The new last name of the user
 * @returns {Promise} axios promise
 */
export function updateUser(purl, {first_name, last_name}){
	return axios({
		method: "post",
		url: `/api/users/${purl}`,
		data: {
			first_name, last_name,
		}
	});
}