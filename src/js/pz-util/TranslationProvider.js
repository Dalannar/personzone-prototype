import { h, Component } from "preact";
import Polyglot from "node-polyglot";

/**
 * @param {HTMLElement} this.props.locale The HTMLElement containing the translation configuration and parameters
 */
class TranslationProvider extends Component{
	constructor(props){
		super(props);

		this.polyglot = new Polyglot();
		this.initPolyglot(props.locale);
	}
	
	initPolyglot(locale){
		this.polyglot.locale(locale.getAttribute("data-code"));
		this.polyglot.replace(JSON.parse(locale.getAttribute("data-phrases")));
	}

	getChildContext(){
		return {
			t: this.polyglot.t.bind(this.polyglot),
		};
	}

	render(props){
		return props.children[0];
	}
}

export default TranslationProvider;