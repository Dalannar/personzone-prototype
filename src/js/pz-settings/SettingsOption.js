import { h } from "preact";

import PrivacySelect from "./PrivacySelect";

export default function({...props}){
	const {
		label,
		input, help,
		privacy, onPrivacyChange, disabledPrivacy
	} = props;
	
	return (
		<div>
			<label className="label">{label}</label>
			<div className="columns is-mobile">
				<div className="column">
					{input}
					{help}
				</div>
				<div className="column is-narrow">
					<PrivacySelect
						disabled={disabledPrivacy}
						value={privacy}
						onChange={onPrivacyChange}
					/>
				</div>
			</div>
		</div>
	);
}