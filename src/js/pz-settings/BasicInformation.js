import { h, Component } from "preact";

import SettingsOption from "./SettingsOption";

export default class BasicInformation extends Component {
	constructor(props){
		super(props);

		this.state = {};
	}

	render(){
		return (
			<div>
				<SettingsOption
					label="Name"
					input={
						<input className="input" />
					}
					privacy="private"
					onPrivacyChange={() => {}}
				/>
				<SettingsOption
					label="Email"
					input={
						<input className="input" />
					}
					privacy="private"
					onPrivacyChange={() => {}}
				/>
				<SettingsOption
					label="Personal URL"
					input={
						<input className="input" />
					}
					help={
						<p className="help">Your personal URL uniquely identifies you. You can share it with your friends so they can more easily find you</p>
					}
					privacy="public"
					disabledPrivacy={true}
				/>
			</div>
		);
	}
}