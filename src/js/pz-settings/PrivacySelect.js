import { h } from "preact";

export default function({...props}){
	const { value, onChange, disabled } = props;

	return (
		<div className="select">
			<select disabled={disabled} value={value} onChange={onChange}>
				<option value="public">Public</option>
				<option value="friends">Friends Only</option>
				<option value="private">Private</option>
			</select>
		</div>
	);
}