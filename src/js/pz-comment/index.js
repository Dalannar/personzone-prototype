import { thankComment } from "../pz-util/api/comments";

document.addEventListener("DOMContentLoaded", function(){
	const commentElements = document.getElementsByClassName("comment");

	Array.from(commentElements).forEach(function(el){
		const id = Number(el.getAttribute("data-id"));
		const thanked = Boolean(el.getAttribute("data-thanked"));
		const thankButton = el.getElementsByClassName("thank-button")[0];

		if(!thanked){
			const onClick = function(e){
				thankComment(id);
				
				thankButton.classList.remove("is-outlined");
				thankButton.getElementsByClassName("thank-button-text")[0].innerHTML = "Thanked";
				thankButton.removeEventListener("click", onClick);
			};

			thankButton.addEventListener("click", onClick);
		}
	});
});