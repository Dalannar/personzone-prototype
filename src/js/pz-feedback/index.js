document.addEventListener("DOMContentLoaded", function(){
	const queryParams = JSON.parse(document.getElementById("meta_data").getAttribute("data-queryParams"));

	document.getElementById("feedback_control-filter-zone").addEventListener("change", function(e){
		location.href=`/a/${queryParams.purl}/feedback/${e.target.value}
			?page=0
			&resultsPer=${queryParams.resultsPer}
			&orderBy=${queryParams.orderBy}
			&thanked=${queryParams.thanked}
		`;
	});

	document.getElementById("feedback_control-filter-subzone").addEventListener("change", function(e){
		location.href=`/a/${queryParams.purl}/feedback/${queryParams.zurl}
			?page=0
			&resultsPer=${queryParams.resultsPer}
			&orderBy=${queryParams.orderBy}
			&thanked=${queryParams.thanked}
			&subzone=${e.target.value}
		`;
	});

	document.getElementById("feedback_control-filter-thanked").addEventListener("change", function(e){
		location.href=`/a/${queryParams.purl}/feedback/${queryParams.zurl}
			?page=0
			&resultsPer=${queryParams.resultsPer}
			&orderBy=${queryParams.orderBy}
			&subzone=${queryParams.subzone}
			&thanked=${e.target.value}
		`;
	});
});