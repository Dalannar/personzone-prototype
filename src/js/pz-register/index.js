import { h, render } from "preact";

import Register from "./Register";
import TranslationProvider from "../pz-util/TranslationProvider";

document.addEventListener("DOMContentLoaded", function(){
	render((
		<TranslationProvider locale={document.getElementById("locale_register")}>
			<Register />
		</TranslationProvider>
	), document.getElementById("register_container"));
});