import { h, Component } from "preact";

import Translatable from "../pz-util/Translatable";

import { isUserName, isEmail } from "../../../util/validation";
import { isEmailAvailable } from "../util/api/availability";
import { createUser } from "../util/api/users";
import { register_delay } from "../util/constants";

/**
 * @property {String} this.state.name The value of the name field
 * @property {String} this.state.nameError An error message if there is an error with the name
 * @property {String} this.state.email The value of the email field
 * @property {String} this.state.emailError An error message if there is an error with the email
 * @property {Bool} this.state.emailLoading True if currently checking the availability of the email
 * @property {String} this.state.password The value of the password field
 * @property {String} this.state.passwordError An error message if there is an error with the passwords
 * @property {String} this.state.repeatPassword The value of the password repeat field
 * @property {Bool} this.state.loading True if currently registering the user
 * @property {String} this.state.registerError An error message if there was an error with the registration
 * 
 * @param {Func} this.props.t Translation function
 */
class Register extends Component{
	constructor(props){
		super(props);

		this.state = {
			name: "", nameError: "",
			email: "", emailError: "", emailLoading: false,
			password: "", passwordError: "",
			repeatPassword: "",
			loading: false,
			registerError: "",
		};
	}

	render(props, state){
		const {
			name, nameError,
			email, emailError, emailLoading,
			password, repeatPassword, passwordError,
			loading, registerError
		} = state;

		const { t } = props;

		return (
			<div>
				<div className="field">
					<label className="label">{t("name_label")}</label>
					<div className="control">
						<input className={`input ${nameError && "is-danger"}`}
							value={name}
							placeholder={t("name_placeholder")}
							type="string"
							onInput={e => this.inputName(e.target.value)}
							disabled={loading}
						/>
					</div>
					{nameError && <p className="help is-danger">{nameError}</p>}
				</div>
				<div className="field">
					<label className="label">{t("email_label")}</label>
					<div className={`control ${emailLoading && "is-loading"}`}>
						<input className={`input ${emailError && "is-danger"}`}
							value={email}
							placeholder={t("email_placeholder")}
							type="string"
							onInput={e => this.inputEmail(e.target.value)}
							onChange={e => this.changeEmail(e.target.value)}
							disabled={loading}
						/>
					</div>
					{emailError && <p className="help is-danger">{emailError}</p>}
				</div>

				<div className="columns">
					<div className="column">
						<div className="field">
							<label className="label">{t("password_label")}</label>
							<div className="control">
								<input className={`input ${passwordError && "is-danger"}`}
									value={password}
									type="password"
									onInput={e => this.inputPassword(e.target.value)}
									onChange={e => this.changePassword(e.target.value)}
									disabled={loading}
								/>
							</div>
						</div>
						{passwordError && <p className="help is-danger">{passwordError}</p>}
					</div>
					<div className="column">
						<div className="field">
							<label className="label">{t("repeat_password_label")}</label>
							<div className="control">
								<input className={`input ${passwordError && "is-danger"}`}
									value={repeatPassword}
									type="password"
									onInput={e => this.inputRepeatPassword(e.target.value)}
									onChange={e => this.changeRepeatPassword(e.target.value)}
									disabled={loading}
								/>
							</div>
						</div>
					</div>
				</div>

				<button className={`button is-fullwidth is-primary ${loading && "is-loading"}`}
					disabled={!this.canRegister()}
					onClick={this.register.bind(this)}
				>
					{t("register_button_label")}
				</button>

				{registerError &&
					<p className="help is-danger">{registerError}</p>
				}
			</div>
		);
	}

	register(){
		if(!this.canRegister())
			return;

		const _this = this;
		this.setState({loading: true, registerError: ""}, function(){
			setTimeout(function(){
				createUser(_this.state.name, _this.state.email, _this.state.password).then(function(response){
					return window.location.href = `/a/${response.data.purl}`;
				}).catch(function(){
					_this.setState({loading: false, registerError: _this.props.t("error_register")});
				});
			}, register_delay);	
		});
	}

	canRegister(){
		if(!this.state.name || this.state.nameError)
			return false;

		if(!this.state.email || this.state.emailError || this.state.emailLoading)
			return false;

		if(!this.state.password || !this.state.password || this.state.passwordError)
			return false;

		if(this.state.loading)
			return false;

		return true;
	}

	inputName(name){
		const nameError = name && !isUserName(name) ? this.props.t("error_invalid_name") : "";

		this.setState({name, nameError});
	}

	inputEmail(email){
		this.setState({email, emailError: ""});
	}

	changeEmail(email){
		const _this = this;
		let emailError = email && !isEmail(email) ? this.props.t("error_invalid_email") : "";
		if(email && !emailError){
			this.setState({emailLoading: true}, function(){
				isEmailAvailable(email).then(function(response){
					emailError = response.data.available ? "" : _this.props.t("error_unavailable_email");
					_this.setState({emailLoading: false, emailError});
				}).catch(function(){
					_this.setState({emailLoading: false});
				});
			});
		}else{
			this.setState({email, emailError});
		}
	}

	inputPassword(password){
		this.setState({password, passwordError: ""});
	}

	changePassword(password){
		const passwordError = password && this.state.repeatPassword && password !== this.state.repeatPassword ? this.props.t("error_passwords_dont_match") : "";
		this.setState({password, passwordError});
	}

	inputRepeatPassword(repeatPassword){
		this.setState({repeatPassword, passwordError: ""});
	}

	changeRepeatPassword(repeatPassword){
		const passwordError = repeatPassword && this.state.password && repeatPassword !== this.state.password ? this.props.t("error_passwords_dont_match") : "";
		this.setState({repeatPassword, passwordError});
	}

}

export default Translatable(Register, "register_locale");