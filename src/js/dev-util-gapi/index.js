import { h, render } from "preact";

import PlacesAutoComplete from "./PlacesAutoComplete";

document.addEventListener("DOMContentLoaded", function(){
	render(
		<PlacesAutoComplete />,
		document.getElementById("places-auto-complete")
	);
});