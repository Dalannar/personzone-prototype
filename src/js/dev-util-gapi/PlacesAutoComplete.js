import { h, Component } from "preact";

import PAC from "../components/PlacesAutoComplete";

export default class PlacesAutoComplete extends Component {
	constructor(props){
		super(props);

		this.state = {
			place: {}
		};
	}

	onChange(prediction){
		this.setState({place: prediction});
	}

	render(props, state){
		return (
			<div className="columns">
				<div className="column">
					<div className="box">
						<PAC
							onChange={this.onChange.bind(this)}
						/>
					</div>
				</div>
				<div className="column">
					<div className="box">
						<p>description: {state.place.description}</p>
						<p>place_id: {state.place.place_id}</p>
					</div>
				</div>
			</div>
		);
	}
}