import { h, render } from "preact";

import ZonetraitSelect from "./ZonetraitSelect";
import SearchModal from "./SearchModal";
import ResultsPerSelect from "./ResultsPerSelect";

const scriptTag = document.currentScript;

const startSearchModalOpen = Boolean(scriptTag.getAttribute("data-startSearchModalOpen"));
const resultsPer = Number(scriptTag.getAttribute("data-queryParamResultsPer"));
const orderBy = scriptTag.getAttribute("data-queryParamOrderBy");
const gender = scriptTag.getAttribute("data-queryParamGender");
const trait = scriptTag.getAttribute("data-queryParamTrait");

document.addEventListener("DOMContentLoaded", function(){
	render((
		<ResultsPerSelect updateOnChange value={resultsPer} />
	), document.getElementById("rankings_nav-results-per"));

	render((
		<SearchModal
			changeSearchButton={document.getElementById("rankings_change-search-button")}
			trait={trait}
			startOpen={startSearchModalOpen}
			resultsPer={resultsPer}
			orderBy={orderBy}
			gender={gender}
		/>
	), document.getElementById("rankings_search-modal"));
});