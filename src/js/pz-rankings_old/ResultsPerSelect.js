import { h } from "preact";
import { updateQueryStringParameter } from "../util/location";

/**
 * @param {Number} this.props.value The value of resultsPer
 * @param {Func} [this.props.onChange] Callback function when resultsPer changes
 * @param {Bool} [this.props.updateOnChange=false] If true will update the query string and refresh the page when changing resultsPer
 */
export default function({...props}){
	const { value, updateOnChange, onChange } = props;

	const f = function(e){
		if(updateOnChange)
			location.search = [
				location.search,
				["resultsPer", e.target.value],
				["page", 0]
			].reduce((search, update) => updateQueryStringParameter(search, update[0], update[1]));
		else if(onChange)
			return onChange(e.target.value);
	};

	return (
		<div className="select">
			<select value={value} onChange={f}>
				<option value={10}>Top 10</option>
				<option value={20}>Top 20</option>
				<option value={50}>Top 50</option>
				<option value={100}>Top 100</option>
			</select>
		</div>
	);
}