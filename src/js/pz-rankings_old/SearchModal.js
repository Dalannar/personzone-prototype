import { h, Component } from "preact";
import _ from "lodash";

import ZonetraitSelect from "./ZonetraitSelect";
import ResultsPerSelect from "./ResultsPerSelect";
import GenderSelect from "./GenderSelect";

/**
 * @param {Bool} [this.props.startOpen=false] Whether to start open or not
 * 
 * @param {String} [this.props.trait=""] Initial value of query parameter trait
 * @param {Number} [this.props.resultsPer=10] Initial value of query parameter resultsPer
 * @param {String} [this.props.orderBy="desc"] Initial value of query parameter orderBy
 * @param {String} [this.props.gender="all"] Initial value of query parameter gender
 * @param {HTMLElement} [this.props.changeSearchButton] Button that opens the search modal
 */
export default class SearchModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: props.startOpen,
			trait: props.trait || "",
			orderBy: props.orderBy || "desc",
			resultsPer: props.resultsPer || 10,
			gender: props.gender || "all",
		};

		if(props.changeSearchButton)
			props.changeSearchButton.addEventListener("click", this.open.bind(this));

		this.baseState = _.assign({}, this.state, {open: false});
	}

	render(props, state){
		const {
			open,
			trait,
			orderBy,
			resultsPer,
			gender,
		} = state;

		return (
			<div className={`modal ${open && "is-active"}`}>
				<div className="modal-background" onClick={this.close.bind(this)} />
				<div className="modal-content" style={{overflow: "visible"}}>
					<div className="card">
						<div className="card-header">
							<h1 className="card-header-title">
								Rankings
							</h1>
						</div>
						<div className="card-content">
							<div className="field">
								<label className="label">Trait to rank</label>
								<div className="control">
									<ZonetraitSelect
										trait={trait}
										onChange={this.setTrait.bind(this)}
									/>
								</div>
							</div>
							<div className="field is-grouped">
								<div className="control">
									<label className="label">Order by</label>
									<div className="select">
										<select value={orderBy} onChange={e => this.setOrderBy(e.target.value)}>
											<option value="desc">Descending</option>
											<option value="asc">Ascending</option>
										</select>
									</div>
								</div>
								<div className="control">
									<label className="label">Number of results</label>
									<ResultsPerSelect value={resultsPer} onChange={this.setResultsPer.bind(this)} />
								</div>
								<div className="control">
									<label className="label">Gender</label>
									<GenderSelect value={gender} onChange={e => this.setGender(e.target.value)} />
								</div>
							</div>
							<div className="field is-grouped is-grouped-right">
								<div className="control">
									<button className="button" onClick={this.close.bind(this)}>Close</button>
								</div>
								<div className="control">
									<button 
										className="button is-primary" 
										disabled={!this.canSearch()}
										onClick={this.search.bind(this)}
									>
										Search
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
	
	open(){
		this.setState({open: true});
	}

	close(){
		this.setState(this.baseState);
	}

	/**
	 * Set trait
	 * @param {String} trait
	 */
	setTrait(trait){
		this.setState({trait});
	}

	/**
	 * Set orderBy
	 * @param {String} orderBy
	 */
	setOrderBy(orderBy){
		this.setState({orderBy});
	}

	/**
	 * Set resultsPer
	 * @param {Number} resultsPer 
	 */
	setResultsPer(resultsPer){
		this.setState({resultsPer});
	}

	/**
	 * Set gender
	 * @param {String} gender 
	 */
	setGender(gender){
		this.setState({gender});
	}

	canSearch(){
		if(
			!this.state.trait ||
			!this.state.resultsPer ||
			!this.state.orderBy
		){
			return false;
		}

		return true;
	}

	search(){
		const {
			trait,
			orderBy,
			resultsPer,
			gender
		} = this.state;

		if(this.canSearch())
			window.location.replace(`/rankings/${trait}?orderBy=${orderBy}&resultsPer=${resultsPer}&gender=${gender}`);
	}
}