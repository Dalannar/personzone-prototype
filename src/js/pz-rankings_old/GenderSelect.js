import { h } from "preact";

/**
 * @param {Number} this.props.value The value of gender
 * @param {Func} this.props.onChange Callback function when gender changes
 */
export default function({...props}){
	const { value, onChange } = props;
	
	return (
		<div className="select">
			<select value={value} onChange={onChange}>
				<option value="all">All</option>
				<option value="M">Male</option>
				<option value="F">Female</option>
			</select>
		</div>
	);
}