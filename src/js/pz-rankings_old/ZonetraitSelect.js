import { h, Component } from "preact";
import _ from "lodash";

import AutoComplete from "../components/AutoComplete";

import { searchTraits } from "../pz-util/api/traits";

const TRAITS_SEARCH_LIMIT = 6;

const EXTRA_TRAITS = {
	"All traits": "all",
};

/**
 * @param {String} [this.props.trait=""] Initial value of the trait
 */
export default class ZonetraitSelect extends Component{
	constructor(props){
		super(props);

		this.state = {
			traitsList: [],
			loading: false,
			error: "",
		};

		this.latestSearchId = 0;
	}

	render(props, state){
		const { traitsList, loading, error } = state;
		const { trait } = props;

		return (
			<AutoComplete
				initialValue={trait}
				error={error}
				loading={loading}
				results={traitsList}
				extraResults={_.keys(EXTRA_TRAITS)}
				onInput={this.onInput.bind(this)}
				onChange={this.onChange.bind(this)}
				onRequestClearResults={this.clearTraits.bind(this)}
			/>
		);
	}

	onInput({value}){
		if(this.latestSearchId >= Number.MAX_SAFE_INTEGER){
			this.latestSearchId = 0;
		}
		
		this.latestSearchId++;
		const searchId = this.latestSearchId;

		const _this = this;
		this.setState({loading: true}, function(){
			searchTraits(value, TRAITS_SEARCH_LIMIT)
				.then(function(results){
					// Ignore if this isn't the latest search
					if(searchId === _this.latestSearchId)
						_this.setState({error: "", loading: false, traitsList: _.map(results.data, trait => trait.name)});
					
				}).catch(function(){
					// Ignore if this isn't the latest search
					if(searchId === _this.latestSearchId)
						_this.setState({error: "Couldn't search traits", loading: false, traitsList: []});
				});
		});
	}

	onChange({value}){
		if(!value || _.indexOf(this.state.traitsList, value) !== -1){
			this.setState({error: ""});
			this.props.onChange(value);
		}else if(value in EXTRA_TRAITS){
			this.setState({error: ""});
			this.props.onChange(EXTRA_TRAITS[value]);
		}else{
			this.setState({error: "That is not a valid trait"});
			this.props.onChange("");
		}
	}

	/**
	 * Clears the traits list
	 */
	clearTraits(){
		this.setState({traitsList: []});
	}
}