import { h, render } from "preact";
import _ from "lodash";

import TranslationProvider from "../pz-util/TranslationProvider";
import CreateZoneModal from "./CreateZoneModal";

document.addEventListener("DOMContentLoaded", function(){
	const zoneSidebars = document.getElementsByClassName("zone-sidebar");
	const localeEl = document.getElementById("locale_create_zone_modal");

	
	_.forEach(zoneSidebars, function(el){
		const createZoneModal = el.getElementsByClassName("create-zone-modal")[0];
		const usedZones = JSON.parse(el.getAttribute("data-zones"));
		if(createZoneModal){
			render((
				<TranslationProvider locale={localeEl}>
					<CreateZoneModal
						createZoneBtn={el.getElementsByClassName("create-zone-btn")[0]}
						usedZurls={_.map(usedZones, zone => zone.zurl)}
						purl={el.getAttribute("data-purl")}
					/>
				</TranslationProvider>
			), createZoneModal);
		}
	});
});