import { h, Component } from "preact";
import Translatable from "../pz-util/Translatable";

import { isZurl, isZoneName } from "../../../util/validation";
import { createZone } from "../pz-util/api/zones";

/**
 * @property {Bool} this.state.open Whether or not the modal is open
 * @property {Bool} this.state.loading Whether or not a zone is being created (ajax request sent)
 * @property {String} this.state.name The value of the name field
 * @property {String} this.state.nameError An error pertraining to the name field, if no error then empty string
 * @property {String} this.state.zurl The value of the zurl field
 * @property {String} this.state.zurlError An error pertraining to the zurl field, if no error then empty string
 * @property {String} this.state.type The value of the type select field
 * 
 * @param {HTMLElement} this.props.createZoneBtn The button that opens the modal
 * @param {String[]} this.props.usedZurls A list of zurls that the user already has in use
 * @param {Func} this.props.t Translation function
 */
class CreateZoneModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,
			loading: false,
			name: "",
			nameError: "",
			zurl: "",
			zurlError: "",
			type: "personal",
		};

		this.baseState = Object.assign({}, this.state);

		if(props.createZoneBtn)
			props.createZoneBtn.addEventListener("click", this.open.bind(this));
	}

	render(){
		const { open, loading, name, nameError, zurl, zurlError, type } = this.state;
		const { t } = this.props;

		return (
			<div className={`modal ${open ? "is-active": ""}`}>
				<div className="modal-background" onClick={this.close.bind(this)} />
				<div className="modal-content">
					<div className="card">
						<div className="card-content">

							<div className="field">
								<div className="control">
									<label className="label">{t("name")}</label>
									<input
										className={`input ${nameError ? "is-danger":""}`}
										value={name}
										placeholder={t("name_placeholder")}
										onInput={e => this.changeName(e.target.value)}
										type="string"
									/>
									{nameError &&
										<div className="help is-danger">{nameError}</div>
									}
								</div>
							</div>

							<div className="field is-grouped">
								
								<div className="control">
									<label className="label">{t("zurl")}</label>
									<input
										className={`input ${zurlError ? "is-danger":""}`}
										onInput={e => this.changeZurl(e.target.value)}
										value={zurl}
										placeholder={t("zurl_placeholder")}
										type="string"
									/>
									{zurlError &&
										<div className="help is-danger">{zurlError}</div>
									}
								</div>

								<div className="control">
									<label className="label">{t("type")}</label>
									<div className="select">
										<select
											onChange={e => this.changeType(e.target.value)}
											value={type}
										>
											<option value="personal">{t("type_personal")}</option>
											<option value="custom">{t("type_custom")}</option>
										</select>
									</div>
								</div>
							</div>

							<div className="field is-grouped is-grouped-right">
								<div className="control">
									<button className="button" onClick={this.close.bind(this)}>{t("cancel")}</button>
								</div>
								<div className="control">
									<button 
										className={`button is-primary ${loading ? "is-loading":""}`}
										disabled={!this.canCreate()}
										onClick={this.createZone.bind(this)}
									>
										{t("create")}
									</button>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		);
	}

	open(){
		this.setState({open: true});
	}

	close(){
		this.setState(this.baseState);
	}

	canCreate(){
		if(
			this.state.loading ||
			!this.state.name ||
			this.state.nameError ||
			!this.state.zurl ||
			this.state.zurlError
		){
			return false;
		}


		return true;
	}

	createZone(){
		const _this = this;

		if(this.canCreate()){
			this.setState({loading: true});
			createZone(this.state.type, this.state.name, this.state.zurl, this.props.purl)
				.then(function(results){
					window.location.replace(`/a/${_this.props.purl}/z/${_this.state.zurl}`);
				}).catch(function(){
					_this.close();
				});
		}
	}

	changeName(value){
		const error = (value && !isZoneName(value)) ? this.props.t("english_only_error") : "";
		this.setState({name: value, nameError: error});
	}

	changeZurl(value){
		let error = (value && !isZurl(value)) ? this.props.t("english_only_error") : "";
		if(!error){
			error = this.props.usedZurls.indexOf(value) === -1 ? "" : this.props.t("unavailable_zurl_error");
		}

		this.setState({zurl: value, zurlError: error});
	}

	changeType(value){
		this.setState({type: value});
	}
}

export default Translatable(CreateZoneModal);