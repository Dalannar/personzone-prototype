import { h, Component } from "preact";

import Translatable from "../pz-util/Translatable";
import Rater from "../components/Rater";

/**
 * The speed of the rater animation, in milliseconds
 */
const ANIMATION_SPEED = 500;

/**
 * @property {Number} this.state.rating The current rating to show in the rater
 * @property {Number} this.state.lastRating The last rating shown, used for the animation formula
 * @property {Bool} this.state.hovering If true the mouse is currently hovering over a rating box
 */
class AnimatedRater extends Component {
	constructor(props){
		super(props);

		this.state = {
			rating: 0,
			lastRating: -1,
			hovering: false,
			animating: true,
		};

		this.baseState = Object.assign({}, this.state);

		setInterval(this.animationTick.bind(this), ANIMATION_SPEED);
	}

	render(props, state){
		const { rating, hovering, animating } = state;
		const { t } = props;
		return (
			<div>
				<Rater
					large
					rating={rating}
					enableHover
					onHoverEnter={this.onHoverEnter.bind(this)}
					onHoverLeave={this.onHoverLeave.bind(this)}
					enableRating
					onRate={this.onRate.bind(this)}
				/>
				<div id="welcome_rater-explanation">
					{hovering || !animating ? (
						<h1 className="subtitle has-text-centered">{t(`rating_${rating}`)}</h1>
					):(
						<h1 className="subtitle">&nbsp;</h1>
					)}
				</div>
			</div>
		);
	}

	onHoverEnter(rating){
		this.setState({hovering: true, rating});
	}

	onHoverLeave(){
		const rating = this.state.animating ? 0 : this.state.rating;
		const lastRating = this.state.animating ? -1 : this.state.lastRating;
		this.setState({hovering: false, rating, lastRating});
	}

	animationTick(){
		if(!this.state.hovering && this.state.animating){
			const temp = this.state.rating;
			const newRating = Math.abs(((2 * this.state.rating) % 10) - this.state.lastRating);
			this.setState({rating: newRating, lastRating: temp});
		}
	}

	onRate(rating){
		console.log("here");
		this.setState({rating, animating: false});
	}
}

export default Translatable(AnimatedRater);