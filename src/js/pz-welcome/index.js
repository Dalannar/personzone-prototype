import { h, render } from "preact";

import TranslationProvider from "../pz-util/TranslationProvider";
import AnimatedRater from "./AnimatedRater";

document.addEventListener("DOMContentLoaded", function(){
	const raterLocaleEl = document.getElementById("welcome_rater-locale");
	render((
		<TranslationProvider locale={raterLocaleEl}>
			<AnimatedRater />
		</TranslationProvider>
	), document.getElementById("welcome_rater-container"));
});