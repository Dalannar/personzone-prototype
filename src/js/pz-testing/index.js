import { h, render, Component } from "preact";
import _ from "lodash";

import AutoComplete from "../components/AutoComplete";

class Testing extends Component{
	constructor(props){
		super(props);

		this.state = {
			results: [],
			inputValue: "",
			loading: false,
			closeWhenNoResults: false,
		};
	}

	render(){
		const { results, inputValue, loading, closeWhenNoResults } = this.state;

		return (
			<div className="box" style={{margin: "20px", width: "500px"}}>
				<div>
					InputValue = {inputValue}
				</div>
				<AutoComplete
					autofocus
					closeWhenNoResults={closeWhenNoResults}
					closeWhenNoValue={true}
					loading={loading}
					results={results}
					onInput={this.onInput.bind(this)}
					onChange={this.onChange.bind(this)}
					onRequestClearResults={this.clearResults.bind(this)}
				/>
			</div>
		);
	}

	onInput({value}){
		if (value.length > 5){
			this.setState({closeWhenNoResults: true});
		}else{
			this.setState({closeWhenNoResults: false});
		}

		return;
		this.setState({loading: false, results: _.map(_.range(10), i => `${value}-${i}`)});
	}

	onChange({value}){
		this.setState({inputValue: value});
	}

	clearResults(){
		this.setState({results: []});
	}
}

render(
	<Testing />,
	document.getElementById("testing-area")
);