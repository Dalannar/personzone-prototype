import _ from "lodash";

document.addEventListener("DOMContentLoaded", function(){
	let mapDOM = document.getElementById("map");
	let initialViewport = JSON.parse(mapDOM.getAttribute("data-viewport"));
	
	const map = new google.maps.Map(mapDOM, {
		center: {lat: 0, lng: 0},
		zoom: 1,
	});
	
	if(initialViewport){
		let initialBounds = {
			east: initialViewport.northeast.lng,
			north: initialViewport.northeast.lat,
			south: initialViewport.southwest.lat,
			west: initialViewport.southwest.lng,
		};
		map.fitBounds(initialBounds);
	}

	let markers = {};
	_.each(document.getElementsByClassName("rank-item"), function(ranking){
		const id = ranking.getAttribute("data-id");
		const coordinates = JSON.parse(ranking.getAttribute("data-point")).coordinates;
		if(!coordinates) return;
		
		markers[id] = new google.maps.Marker({
			position: {lat: coordinates[0], lng: coordinates[1]},
			map,
		});

		const markerContent = ranking.getAttribute("data-markerContent");
		const infoWindow = new google.maps.InfoWindow({content: markerContent});
		markers[id].addListener("click", function(){
			infoWindow.open(map, markers[id]);
		});
	});
/*
	const observer = new MutationObserver(function(mutationsList){
		console.log("here", mutationsList);
	});

	observer.observe(document.getElementById("rank-item-list"), {
		attributes: false,
		childList: true,
	});*/
});