import { h, render } from "preact";
import _ from "lodash";

import { getRankings } from "../util/api/rankings";
import ChangeSettingsModal from "./ChangeSettingsModal";

document.addEventListener("DOMContentLoaded", function(){
	const settingsContainer = document.getElementById("rankings_change-settings-modal");
	const settingsContainerStartOpen = Boolean(settingsContainer.getAttribute("data-startOpen"));
	const locationName = settingsContainer.getAttribute("data-locationName");
	const place_id = settingsContainer.getAttribute("data-placeId");
	const trait_id = settingsContainer.getAttribute("data-traitId");
	const traitName = settingsContainer.getAttribute("data-traitName");
	render((
		<ChangeSettingsModal
			openButton={document.getElementById("rankings_open-change-settings-modal-button")}
			startOpen={settingsContainerStartOpen}
			{...{locationName, place_id, trait_id, traitName}}
		/>
	), settingsContainer);

	let page = 0;
	let limit = 10;
	let order = "desc";

	_.each(Array.from(document.getElementsByClassName("load-more-button")), function(button){
		button.addEventListener("click", function(){
			page += 1;
			getRankings({trait_id, page, limit, order, place_id})
				.then(function(response){
					_.each(Array.from(document.getElementsByClassName("rank-item-list")), function(container){
						container.innerHTML += response.data;
					});
				}).catch(() => {});
		});
	});
});

import "./map";