import { h, Component } from "preact";
import _ from "lodash";

import PlacesAutoComplete from "../components/PlacesAutoComplete";
import TraitsAutoComplete from "../components/TraitsAutoComplete";

/**
 * @param {boolean} [this.props.startOpen=false] Whether or not to start with the modal open
 * @param {HTMLElement} [this.props.openButton] The HTMLElement that should open the search modal on click
 * 
 * @param {string} [this.props.place_id]
 * @param {string} [this.props.locationName]
 * @param {string} [this.props.trait_id]
 * @param {string} [this.props.traitName]
 */
export default class ChangeSettingsModal extends Component {
	constructor(props){
		super(props);

		this.state = {
			open: props.startOpen || false,
			trait_id: props.trait_id,
			place_id: props.place_id,
		};

		if(props.openButton)
			props.openButton.addEventListener("click", this.open.bind(this));
	}

	open(){
		this.setState({open: true});
	}

	close(){
		this.setState({open: false});
	}

	setTraitId(trait_id){
		this.setState({trait_id});
	}

	setPlaceId(place_id){
		this.setState({place_id});
	}
	
	applySettings(){
		const trait_id = this.state.trait_id || 0;
		
		let href = `/rankings/${trait_id}?`;
		if(!_.isNil(this.state.place_id)){
			href += `place_id=${this.state.place_id}&`;
		}
	
		window.location.href = href;
	}

	render(props, state){
		return (
			<div className={`modal ${state.open && "is-active"}`}>
				<div className="modal-background" onClick={this.close.bind(this)}></div>
				<div className="modal-content" style={{overflow: "visible"}}>
					<div className="box">
						<div className="field">
							<label className="label">Trait</label>
							<div className="control">
								<TraitsAutoComplete
									initialValue={props.traitName}
									useAllOption={true}
									onChange={({id}) => this.setTraitId(id)}
								/>
							</div>
						</div>

						<div className="field">
							<label className="label">Location</label>
							<div className="control">
								<PlacesAutoComplete
									initialValue={props.locationName}
									onChange={({place_id}) => this.setPlaceId(place_id)}
								/>
							</div>
						</div>

						<div className="field is-grouped is-grouped-right">
							<div className="control">
								<button className="button" onClick={this.close.bind(this)}>Close</button>
							</div>
							<div className="control">
								<button className="button is-primary" onClick={this.applySettings.bind(this)}>Apply</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}