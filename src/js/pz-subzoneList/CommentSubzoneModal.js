import { h, Component } from "preact";
import Translatable from "../pz-util/Translatable";

import { isCommentText } from "../../../util/validation";
import { createComment } from "../pz-util/api/comments";

import { SUBZONE_COMMENT_DELAY } from "../pz-util/constants";


/**
 * @property {Bool} this.state.open The current state of the modal
 * @property {String} this.state.name The name of the subzone currently being commented on
 * @property {Number} this.state.id The id of the subzone currently being commenter on
 * @property {String} this.state.comment The value of the current comment
 * @property {String} this.state.error An error message to display, if any
 * @property {Bool} this.state.loading True if a comment creation request has been sent to the server
 * 
 * @property {String} this.props.username The username to use in text displays
 * @property {HTMLElement} this.props.listContainer The HTML element holding all of the subzones
 * @property {Func} this.props.t Translation function
 */
class CommentSubzoneModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,
			name: "",
			id: null,
			comment: "",
			error: "",
			loading: false,
		};

		this.baseState = Object.assign({}, this.state);
		
		const _this = this;
		this.props.listContainer.addEventListener("click", function(event){
			if(event.target.classList.contains("comment-button")){
				const s = event.target.closest(".subzone");
				_this.open(s.getAttribute("data-name"), s.getAttribute("data-id"));
			}
		});
	}

	/**
	 * @override
	 */
	render(props, state){
		const { open, name, comment, error, loading } = state;
		const { t, username } = props;

		return (
			<div className={`modal ${open && "is-active"}`}>
				<div className="modal-background" onClick={this.close.bind(this)} />
				<div className="modal-content">
					<div className="card">
						<div className="card-content">
							<div className="content">
								<p>{t("caption", {username, subzone: name})}</p>
							</div>
							<div className="field">
								<div className="control">
									<label className="label">{t("comment")}</label>
									<textarea className={`textarea ${error && "is-danger"}`}
										value={comment}
										placeholder={t("comment_placeholder")}
										onInput={e => this.changeComment(e.target.value)}									
									/>
									{error &&
										<p className="help is-danger">{error}</p>
									}
								</div>
							</div>
							<div className="field is-grouped is-grouped-right">
								<div className="control">
									<button 
										className="button"
										onClick={this.close.bind(this)}
									>{t("close")}</button>
								</div>
								<div className="control">
									<button
										className={`button is-primary ${loading && "is-loading"}`}
										disabled={!this.canComment()}
										onClick={this.comment.bind(this)}
									>
										{t("submit")}
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	/**
	 * @returns {Bool} Whether a comment can be submitted or not
	 */
	canComment(){
		const {comment, error, loading } = this.state;
		if(!comment || error || loading)
			return false;

		return true;
	}

	/**
	 * Create a comment
	 */
	comment(){
		if(!this.canComment())
			return;

		const { id, comment } = this.state;
		const { t, commenterPurl } = this.props;
		const _this = this;

		this.setState({loading: true});
		createComment(id, commenterPurl, comment)
			.then(function(){
				setTimeout(_this.close.bind(_this), SUBZONE_COMMENT_DELAY);
			}).catch(function(){
				_this.setState({loading: false, error: t("unknown_error")});
			});
	}

	/**
	 * Opens the comment modal
	 * @param {String} name The name of the subzone to comment on 
	 * @param {Number} id The id of the subzone to comment on
	 */
	open(name, id){
		this.setState({ open: true, name, id });
	}

	/**
	 * Closes (and resets) the comment modal
	 */
	close(){
		this.setState(this.baseState);
	}

	/**
	 * Change the value of the comment to the value given
	 * @param {String} value The new value of the comment
	 */
	changeComment(value){
		let error = "";
		if(value && !isCommentText(value))
			error = this.props.t("invalid_comment_error");
			
		this.setState({comment: value, error});
	}
}

export default Translatable(CommentSubzoneModal);