import { h, Component } from "preact";
import Translatable from "../pz-util/Translatable";

import { deleteSubzone } from "../pz-util/api/subzones";

import { SUBZONE_DELETION_DELAY } from "../pz-util/constants";

/**
 * @property {Bool} this.state.open Whether the modal is open or not
 * @property {String} this.state.name The name of the currently "selected" subzone
 * @property {Number} this.state.id The id of the currently "selected" subzone
 * @property {String} this.state.error An error message to display, if any
 * @property {Bool} this.state.loading True if a delete request has been sent to the server
 * 
 * @property {Func} this.props.t Translation function
 * @property {HTMLElement} this.props.listContainer The element containing all of the subzones
 */
class DeleteSubzoneModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,
			name: "",
			id: null,
			error: "",
			loading: false,
		};

		this.baseState = Object.assign({}, this.state);
		
		const _this = this;
		this.props.listContainer.addEventListener("click", function(event){
			if(event.target.classList.contains("delete-button")){
				const s = event.target.closest(".subzone");
				_this.open(s.getAttribute("data-name"), s.getAttribute("data-id"));
			}
		});
	}
	
	/**
	 * @override
	 */
	render(props, state){
		const { open, name, error, loading } = state;
		const { t } = props;

		return (
			<div className={`modal ${open && "is-active"}`}>
				<div className="modal-background" onClick={this.close.bind(this)} />
				<div className="modal-content">
					<div className="card">
						<header className="card-header">
							<p className="card-header-title">{name}</p>
						</header>

						<div className="card-content">
							<div className="content">
								<p>{t("caption", {subzone: name})}</p>
								<p className="has-text-danger">{error}</p>
							</div>

							<div className="level">
								<div className="level-left">
									<button
										className="button"
										onClick={this.close.bind(this)}
									>
										{t("close")}
									</button>
								</div>
								<div className="level-right">
									<button
										className={`button is-danger ${loading && "is-loading"}`}
										disabled={!this.canDelete()}
										onClick={this.delete.bind(this)}
									>
										{t("delete")}
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	/**
	 * @returns {Bool} Whether or not a subzone can be deleted
	 */
	canDelete(){
		const { id, loading } = this.state;
		if(!id || loading)
			return false;

		return true;
	}

	delete(){
		if(!this.canDelete())
			return;

		const { id } = this.state;
		const { t } = this.props;
		const _this = this;
		
		this.setState({loading: true});
		deleteSubzone(id)
			.then(function(){
				setTimeout(() => window.location.reload(), SUBZONE_DELETION_DELAY);
			}).catch(function(){
				_this.setState({loading: false, error: t("unknown_error")});
			});
	}

	/**
	 * Opens the modal
	 * @param {String} name The name of the subzone to delete
	 * @param {Number} id The id of the subzone to delete
	 */
	open(name, id){
		this.setState({open: true, name, id});
	}

	/**
	 * Closes the modal
	 */
	close(){
		this.setState(this.baseState);
	}
}

export default Translatable(DeleteSubzoneModal);