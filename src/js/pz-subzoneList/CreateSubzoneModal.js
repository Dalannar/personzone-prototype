import { h, Component } from "preact";
import Translatable from "../pz-util/Translatable";

import { isSubzoneName } from "../../../util/validation";
import { createSubzone } from "../pz-util/api/subzones";

import { SUBZONE_CREATION_DELAY } from "../pz-util/constants";

/**
 * @property {Bool} this.state.open The current state of the modal
 * @property {String} this.state.name The value of the name field
 * @property {String} this.state.error An error message to display, if any
 * @property {Bool} this.state.loading True if a creation request has been sent to the server
 * 
 * @param {HTMLElement} this.props.openButton The button that opens the modal window
 * @param {String[]} this.props.existingSubzones An array of the already existing subzones
 * @param {Func} this.props.t Translation function
 */
class CreateSubzoneModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,
			name: "",
			error: "",
			loading: false,
		};

		this.baseState = Object.assign({}, this.state);
		this.props.openButton.addEventListener("click", this.open.bind(this));
	}

	/**
	 * @override
	 */
	render(props, state){
		const { open, name, error, loading } = state;
		const { t } = props;

		return (
			<div className={`modal ${open && "is-active"}`}>
				<div className="modal-background" onClick={this.close.bind(this)}/>
				<div className="modal-content">
					<div className="card">
						<div className="card-content">
							<div className="field">
								<div className="control">
									<label className="label">{t("subzone")}</label>
									<input
										autofocus
										type="string"
										className={`input ${error && "is-danger"}`}
										value={name}
										placeholder={t("subzone_placeholder")}
										onInput={e => this.changeName(e.target.value)}
										onKeyPress={this.onKeyPress.bind(this)}
									/>
									{error &&
										<p className="help is-danger">{error}</p>
									}
								</div>
							</div>
							<div className="field is-grouped is-grouped-right">
								<div className="control">
									<button className="button" onClick={this.close.bind(this)}>{t("close")}</button>
								</div>
								<div className="control">
									<button 
										className={`button is-primary ${loading && "is-loading"}`}
										disabled={!this.canCreate()}
										onClick={this.create.bind(this)}
									>
										{t("create")}
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	/**
	 * Handles key press on the name field
	 */
	onKeyPress(e){
		const k = e.which || e.keycode;
		if(k === 13)
			this.create();
	}

	/**
	 * @returns {Bool} Whether a subzone can be created or not
	 */
	canCreate(){
		const { name, error, loading } = this.state;
		if(!name || error || loading)
			return false;

		return true;
	}

	/**
	 * Creates a subzone
	 */
	create(){
		if(!this.canCreate())
			return;

		const { name } = this.state;
		const { t, zoneId } = this.props;
		const _this = this;

		this.setState({loading: true});
		createSubzone(name, zoneId)
			.then(function(){
				setTimeout(() => window.location.reload(), SUBZONE_CREATION_DELAY);
			}).catch(function(){
				_this.setState({loading: false, error: t("unknown_error")});
			});
	}

	/**
	 * Changes the value of the name
	 * @param {String} value The value of the name
	 */
	changeName(value){
		let error = "";
		if(value && !isSubzoneName(value))
			error = this.props.t("english_only_error");

		if(!error){
			const existingSubzones = this.props.existingSubzones.map(o => o.toLowerCase());
			if(existingSubzones.indexOf(value.toLowerCase()) !== -1)
				error = this.props.t("duplicate_subzone_error");
		}

		this.setState({name: value, error});
	}

	/**
	 * Opens the modal
	 */
	open(){
		this.setState({open: true});
	}

	/**
	 * Closes (and resets) the modal
	 */
	close(){
		this.setState(this.baseState);
	}
}

export default Translatable(CreateSubzoneModal);