import { h, render } from "preact";

import TranslationProvider from "../pz-util/TranslationProvider";

import CommentSubzoneModal from "./CommentSubzoneModal";
import CreateSubzoneModal from "./CreateSubzoneModal";
import DeleteSubzoneModal from "./DeleteSubzoneModal";

document.addEventListener("DOMContentLoaded", function(){
	const subzoneLists = document.getElementsByClassName("subzone-list");

	Array.from(subzoneLists).forEach(function(el){
		const enableCommenting = Boolean(el.getAttribute("data-enableCommenting"));
		const enableCreating = Boolean(el.getAttribute("data-enableCreating"));
		const enableDeleting = Boolean(el.getAttribute("data-enableDeleting"));

		const listContainer = el.getElementsByClassName("subzones")[0];
		const subzoneElements = el.getElementsByClassName("subzone");

		if(enableCommenting){
			const commenterPurl = el.getAttribute("data-commenterPurl");
			const username = el.getAttribute("data-username");

			render((
				<TranslationProvider locale={document.getElementById("locale_comment-subzone-modal")}>
					<CommentSubzoneModal {...{listContainer, username, commenterPurl}} />
				</TranslationProvider>
			), el.getElementsByClassName("comment-subzone-modal")[0]);
		}

		if(enableCreating){
			const existingSubzones = Array.from(subzoneElements).map(s => s.getAttribute("data-name"));
			const openButton = el.getElementsByClassName("create-subzone-button")[0];
			const zoneId = el.getAttribute("data-zoneId");

			render((
				<TranslationProvider locale={document.getElementById("locale_create-subzone-modal")}>
					<CreateSubzoneModal {...{existingSubzones, openButton, zoneId}} />
				</TranslationProvider>
			), el.getElementsByClassName("create-subzone-modal")[0]);
		}

		if(enableDeleting){
			render((
				<TranslationProvider locale={document.getElementById("locale_delete-subzone-modal")}>
					<DeleteSubzoneModal {...{listContainer}} />
				</TranslationProvider>
			), el.getElementsByClassName("delete-subzone-modal")[0]);
		}
	});
});