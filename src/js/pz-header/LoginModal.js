import { h, Component } from "preact";
import Translatable from "../pz-util/Translatable";

import { login } from "../pz-util/api/authentication";

// Delay before sending the login request to the server, in milliseconds
const LOGIN_DELAY = 500; 

/**
 * @property {Bool} this.state.open Whether or not the modal is open
 * @property {String} this.state.email The value of the email field
 * @property {String} this.state.password The value of the password field
 * @property {Bool} this.state.error Whether or not there has been an error while logging in
 * @property {Bool} this.state.loading Wether or not we are attempting a login
 * 
 * @property {Object} this.baseState The original state that the loginModal is initialized with
 * 
 * @param {HTMLElement} this.props.loginBtn The login button that opens the modal
 * @param {Func} this.props.onOpen Function called whenever the loginModal opens
 * @param {Func} this.props.t Translation function
 */
class LoginModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,
			email: "",
			password: "",
			error: false,
			loading: false,
		};
		this.baseState = Object.assign({}, this.state);

		if(props.loginBtn)
			props.loginBtn.addEventListener("click", this.open.bind(this));
	}

	render(){
		const { open, email, password, error, loading } = this.state;
		const { t } = this.props;
		
		return (
			<div className={`modal ${open ? "is-active" : ""}`} onKeyPress={this.handleKeyPress.bind(this)}>
				<div className="modal-background" onClick={this.close.bind(this)} />
				<div className="modal-content">
					<div className="card">
						<div className="card-content">
							
							<div className="field">
								<div className="control has-icons-left">
									<input
										autofocus
										className={`input ${error ? "is-danger":""}`}
										type="email"
										placeholder={t("email_placeholder")}
										value={email}
										onInput={this.changeEmail.bind(this)}
									/>
									<span className="icon is-small is-left">
										<i className="fa fa-envelope" />
									</span>
								</div>
							</div>

							<div className="field">
								<div className="control has-icons-left">
									<input
										ref={(input) => this.passwordInput = input}
										className={`input ${error ? "is-danger":""}`}
										type="password"
										placeholder={t("password_placeholder")}
										value={password}
										onInput={this.changePassword.bind(this)}
									/>
									<span className="icon is-small is-left">
										<i className="fa fa-lock" />
									</span>
								</div>
							</div>

							<div className="field">
								<div className="control">
									<label className="checkbox is-unselectable">
										<input type="checkbox" />
										&nbsp;{t("remember_me")}
									</label>
								</div>
							</div>

							<div className="field is-grouped is-grouped-right">
								<div className="control">
									<button className="button" onClick={this.close.bind(this)}>{t("close")}</button>
								</div>
								<div className="control">
									<button 
										className={`button is-primary ${loading ? "is-loading":""}`}
										disabled={!this.canLogin()}
										onClick={this.login.bind(this)}
									>
										{t("login")}
									</button>
								</div>
							</div>

						</div>

						<footer className="card-footer">
							<a className="card-footer-item" href="/register">{t("register")}</a>
						</footer>
					</div>
				</div>
			</div>
		);
	}

	handleKeyPress(e){
		const key = e.which || e.keycode;
		if(key === 13){
			this.login();
		}
	}

	open(){ 
		this.setState({open: true});
		this.props.onOpen();
	}

	close(){ 
		this.setState(this.baseState);
	}

	changeEmail(e){
		this.setState({email: e.target.value});
	}

	changePassword(e){
		this.setState({password: e.target.value});
	}

	canLogin(){
		if(this.state.loading)
			return false;

		if(this.state.email.length < 1)
			return false;

		if(this.state.password.length < 1)
			return false;

		return true;
	}

	login(){
		if(!this.canLogin())
			return;

		const _this = this;
		_this.setState({loading: true});
		setTimeout(function(){
			login(_this.state.email, _this.state.password)
				.then(function(results){
					window.location.replace(`/a/${results.data.purl}`);
				}).catch(function(){
					_this.passwordInput.select();
					_this.setState({error: true, loading: false});
				});
		}, LOGIN_DELAY);
	}
}

export default Translatable(LoginModal);