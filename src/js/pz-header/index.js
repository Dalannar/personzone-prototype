import { h, render } from "preact";
import TranslationProvider from "../pz-util/TranslationProvider";
import LoginModal from "./LoginModal";
import SearchModal from "./SearchModal";

import axios from "axios";

document.addEventListener("DOMContentLoaded", function(){
	// Make sure to add the correct padding (because the navbar is fixed to the top)
	document.getElementsByTagName("body")[0].classList.add("has-navbar-fixed-top");

	// Add functionality to the burger button
	document.getElementById("header_burgerBtn").addEventListener("click", function(){
		document.getElementById("header_navbarMenu").classList.toggle("is-active");
	});

	// Render the login modal
	if(document.getElementById("header_loginModal")){
		render((
			<TranslationProvider locale={document.getElementById("header_loginModalLocale")}>
				<LoginModal
					loginBtn={document.getElementById("header_loginBtn")} 
					onOpen={function(){
						document.getElementById("header_navbarMenu").classList.remove("is-active");
					}}
				/>
			</TranslationProvider>
		), document.getElementById("header_loginModal"));
	}

	// Render the search modal
	if(document.getElementById("header_searchModal")){
		render((
			<TranslationProvider locale={document.getElementById("header_searchModalLocale")}>
				<SearchModal 
					searchBtn={document.getElementById("header_searchBtn")}
					onOpen={function(){
						document.getElementById("header_navbarMenu").classList.remove("is-active");
					}}
				/>
			</TranslationProvider>
		), document.getElementById("header_searchModal"));
	}

	// Set the event listeners for change locale
	const localeBtns = document.getElementsByClassName("header_set-locale-button");
	for(let i = 0; i < localeBtns.length; i++){
		localeBtns[i].addEventListener("click", function(e){
			axios({
				method: "get",
				url: `/api/i18n/set-locale/${e.target.getAttribute("data-locale")}`
			}).then(function(){
				location.reload();
			});
		});
	}

	// Add logout button functionality
	const logoutBtn = document.getElementById("header_logoutBtn");
	if(logoutBtn){
		logoutBtn.addEventListener("click", function(){
			axios({
				method: "get",
				url: "/api/auth/logout"
			}).then(function(){
				window.location.replace("/");
			});
		});
	}
});