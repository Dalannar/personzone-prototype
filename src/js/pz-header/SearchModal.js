import { h, Component } from "preact";
import Translatable from "../pz-util/Translatable";

import { searchUsers } from "../pz-util/api/users";

/**
 * @property {Bool} this.state.open Whether the modal is open or not
 * @property {Bool} this.state.focusedInput If the search input is focused or not
 * @property {String} this.state.searchTerm The value of the search field
 * @property {Bool} this.state.searching Whether or not there is a searching in progress
 * @property {Object[]} this.state.results The results gathered from a search
 * 
 * @param {HTMLElement} this.props.searchBtn The button that opens the search modal
 * @param {Func} this.props.onOpen A callback function for when the modal is opened
 * @param {Func} this.props.t The translation function
 */
class SearchModal extends Component{
	constructor(props){
		super(props);

		this.state = {
			open: false,

			focusedInput: true,
			searchTerm: "",

			searching: false,
			results: [],
		};

		this.baseState = Object.assign({}, this.state);

		if(props.searchBtn)
			props.searchBtn.addEventListener("click", this.toggleVisibility.bind(this));
	}

	render(){
		const { open, searching, searchTerm, results, focusedInput } = this.state;
		const { t } = this.props;

		return (
			<div className={`modal ${open ? "is-active":""}`}>
				<div className="modal-background" onClick={this.close.bind(this)} />
				<div className="modal-content" style={{overflow: "visible"}}>
					<div className="card">
						<div className="card-content">
							
							<div className={`dropdown ${focusedInput ? "is-active":""}`} style={{width: "100%"}}>
								<div className="dropdown-trigger" style={{width: "100%"}}>
									<div className="field">
										<div className={`control ${searching ? "is-loading":""}`}>
											<input
												autoFocus
												className="input"
												onFocus={this.handleFocusInput.bind(this)}
												onBlur={this.handleBlurInput.bind(this)}
												value={searchTerm}
												onInput={e => this.inputSearchTerm(e.target.value)}
												type="string"
											/>
										</div>
									</div>
								</div>
								<div className="dropdown-menu" style={{width: "100%"}}>
									<div className="dropdown-content">
										{results.map(this.getUserItem)}
										{(searchTerm && results.length > 0) &&
											<span>
												<hr className="dropdown-divider" />
												<div className="dropdown-item">
													<div className="level">
														<div className="level-item">
															<a href="/" className="button is-primary is-fullWidth">{t("more_results")}</a>
														</div>
													</div>
												</div>
											</span>
										}
										{(searchTerm && results.length < 1 && !searching) &&
											<p className="dropdown-item"><b>{t("no_results")}</b></p>
										}
										{!searchTerm &&
											<p className="dropdown-item"><b>{t("start_typing")}</b></p>
										}
									</div>
								</div>
							</div>

							<div className="field is-grouped is-grouped-right" style={{marginTop: 64}}>
								<div className="control">
									<button className="button" onClick={this.close.bind(this)}>{t("close")}</button>
								</div>
							</div>
						</div>
						<footer className="card-footer">
							<a href="/" className="card-footer-item">{t("advanced_search")}</a>
						</footer>
					</div>
				</div>
			</div>
		);
	}

	getUserItem(user){
		return (
			<a key={user.purl} href={`/a/${user.purl}`} className="dropdown-item">
				<div className="media">
					<div className="media-content">
						<div className="content">
							<i className="fa fa-user" />
							<b>&nbsp;&nbsp;{`${user.first_name} ${user.last_name}`}&nbsp;</b>
							<span className="tag is-light">
								/{user.purl}
							</span>
						</div>
					</div>
				</div>
			</a>
		);
	}

	toggleVisibility(){
		return this.state.open ? this.close() : this.open();
	}

	open(){
		this.setState({ open: true });
		this.props.onOpen();
	}

	close(){
		this.setState(this.baseState);
	}

	inputSearchTerm(val){
		const _this = this;
		this.setState({searchTerm: val}, function(){
			if(val)
				_this.searchUsers();
			else
				_this.clearResults();
		});
	}

	searchUsers(){
		this.setState({searching: true});
		const _this = this;
		searchUsers(this.state.searchTerm, 6, 0)
			.then(function(results){
				_this.setState({results: results.data, searching: false});
			}).catch(function(){
				_this.setState({results: false, searching: false});
			});
	}

	clearResults(){
		this.setState({results: []});
	}

	handleFocusInput(){
		this.setState({focusedInput: true});
	}

	handleBlurInput(){
		const _this = this;
		setTimeout(function(){
			_this.setState({focusedInput: false});
		}, 100);
	}
}

export default Translatable(SearchModal);