import { h, Component } from "preact";
import _ from "lodash";

const DEFAULTS_NUMBER_RATINGS = 5;

/**
 * @property {Number} this.state.rating The current rating. Ranges from 1 to 5 or null if no rating
 * @property {Number} this.state.hoverRating The current hovered rating. Ranges from 1 to 5 or null if no rating is being hovered
 * 
 * @param {Number} [this.props.initialRating] The initial rating. Use this if the rater must start with a default rating but the value should be handled by the rater
 * @param {Number} [this.props.rating] The rating of the rater. If null the rating will be handled by the rater component
 * 
 * @param {Bool} [this.props.enableHover=false] If true hovering will be allowed and hover events will be fired
 * @param {Func} [this.props.onHoverEnter] Callback when the mouse enters a rating box
 * @param {Func} [this.props.onHoverLeave] Callback when the mouse leaves rating box
 * 
 * @param {Bool} [this.props.enableRating=false] If true rating will be allowed and rate events will be fired
 * @param {Func} [this.props.onRate] Callback when the user clicks a rating
 * 
 * @param {Bool} [this.props.large=false] If true will display larger rating boxes
 */
export default class Rater extends Component{
	constructor(props){
		super(props);

		this.state = {
			rating: props.initialRating || this.props.rating || null,
			hoverRating: null,
		};
	}

	/**
	 * @override
	 */
	render(props, state){
		const activeRating = state.hoverRating || props.rating || state.rating;
		const { numberRatings, large } = props;

		const onHoverEnter = this.onHoverEnter.bind(this);
		const onRate = this.onRate.bind(this);

		return (
			<div className="media-object">
				<div className="media-content">
					<div className="rater"
						onMouseLeave={() => this.onHoverLeave()}
					>
						{_.map(_.range(numberRatings || DEFAULTS_NUMBER_RATINGS), function(i){
							return (
								<div key={i}
									className={`rating-box ${large && "is-large"} rating-${i+1}`}
									style={(activeRating >= i+1) ? {} : {borderColor: "black", background: "none", boxShadow: "none"}}
									onMouseEnter={() => onHoverEnter(i+1)}
									onClick={() => onRate(i+1)}
								/>
							);
						})}
					</div>
				</div>
			</div>
		);
	}

	/**
	 * Callback when mouse enters a rating box
	 * @param {Number} rating The rating of the box the mouse entered
	 */
	onHoverEnter(rating){
		if(this.props.enableHover || this.props.enableRating){
			// TODO: Do we still need to keep this ?
			this.setState({hoverRating: rating});
			if(this.props.onHoverEnter)
				this.props.onHoverEnter(rating);
		}
	}

	/**
	 * Callback when mouse leaves the rater
	 */
	onHoverLeave(){
		if(this.props.enableHover || this.props.enableRating){
			// TODO: Do we still need to keep this
			this.setState({hoverRating: null});
			if(this.props.onHoverLeave)
				this.props.onHoverLeave();
		}
	}

	/**
	 * Callback when user rates
	 * @param {Number} rating The rating the user gave
	 */
	onRate(rating){
		if(this.props.enableRating){
			this.setState({rating});
			if(this.props.onRate)
				this.props.onRate(rating);
		}
	}
}