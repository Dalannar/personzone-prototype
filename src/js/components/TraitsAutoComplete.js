import { h, Component } from "preact";
import _ from "lodash";

import AutoComplete from "./AutoComplete";

import { searchTraits } from "../util/api/traits";

/**
 * @property {object[]} this.state.predictions
 * @property {boolean} this.state.loading
 * @property {object} this.state.chosenPrediction
 * 
 * @param {func} this.props.onChange A callback function called whenever the chosen trait has changed. Is called with the object of the prediction
 * @param {string} this.props.initialValue The initial value of the input
 * @param {boolean} this.props.useAllOption Whether or not to use the all traits option
 */
export default class TraitsAutoComplete extends Component {
	constructor(props){
		super(props);

		this.state = {
			predictions: [],
			loading: false,
			chosenPrediction: {}
		};
	}

	clearPredictions(){
		this.setState({predictions: []});
	}

	onInput({value}){
		const _this = this;
		this.setState({loading: true}, function(){
			searchTraits(value, 8).then(function(response){
				_this.setState({loading: false, predictions: response.data});
			}).catch(function(){
				_this.setState({loading: false, predictions: []});
			});
		});
	}
	
	onChange({value}){
		let chosenPrediction = {};
		if(value === "All traits"){
			chosenPrediction = {id: 0, name: "All traits"};
		}else{
			_.each(this.state.predictions, p => {
				if(p.name === value){
					chosenPrediction = p;
					return false;
				}
			});
		}

		this.setState({chosenPrediction});
		if(_.isFunction(this.props.onChange))
			this.props.onChange(chosenPrediction);
	}

	render(props, state){
		return (
			<div>
				<AutoComplete
					initialValue={props.initialValue}
					loading={state.loading}
					results={_.map(state.predictions, p => p.name)}
					onRequestClearResults={this.clearPredictions.bind(this)}
					onInput={this.onInput.bind(this)}
					onChange={this.onChange.bind(this)}
					extraResults={props.useAllOption ? ["All traits"] : []}
				/>
			</div>
		);
	}
}