import { h, Component } from "preact";
import _ from "lodash";

/**
 * @property {Bool} this.state.open The current state of the dropdown
 * @property {String} this.state.value The current value of the input element
 * @property {Number} this.state.activeResult The index of the currently selected result (in props.results) or -1 for non selected
 * 
 * @property {HTMLElement} this.rootRef Reference to the root element
 * @property {HTMLElement} this.inputRef Reference to the input element
 * 
 * @param {String[]} this.props.results An array of results to show in the dropdown
 * @param {String[]} [this.props.extraResults=[]] An array of extra results to show
 * @param {Bool} [this.props.loading=false] The current state of the loading dropdown item
 * @param {String} [this.props.error=""] If set, will display as the error message
 * 
 * @param {Func} this.props.onRequestClearResults Callback to request clearing the results
 * @param {Func} this.props.onChange Callback when the value of the input has changed
 * @param {Func} this.props.onInput Callback when the value of the input is being changed (should refresh results)
 * 
 * @param {String} [this.props.initialValue=""] The initial value of the input element
 * @param {Bool} [this.props.disableKeyboard=false] If true disables keyboard interaction for the dropdown and selecting results
 * @param {Bool} [this.props.autofocus=false] If true set's the input element to have autofocus
 * @param {Bool} [this.props.closeWhenNoValue=false] If true will close the dropdown if the input value is empty
 * @param {Bool} [this.props.closeWhenNoResults=false] If true will close the dropdown if there are no results to show and loading is not set
 * 
 * @param {String} [this.props.startTypingText="Start typing to search..."] Text to display when the input value is empty
 * @param {String} [this.props.noResultsText="No results..."] Text to display when there are no results
 * @param {String} [this.props.loadingText="Loading..."] Text to display when loading
 * @param {String} [this.props.loadingIconName="fa-circle-o-notch"] Name of fa icon to display in the loading bar (including "fa-" prefix)
 */
export default class AutoComplete extends Component{
	constructor(props){
		super(props);
		
		this.state = {
			open: false,
			value: props.initialValue || "",
			activeResult: -1,
		};

		this.baseState = Object.assign({}, this.state);
		this.rootRef = null;
		this.inputRef = null;

		document.addEventListener("click", this.attemptClose.bind(this));
	}

	render(props, state){
		// No value was given so set the default to true
		if(!_.has(props, "openWhenEmpty"))
			props.openWhenEmpty = true;

		const { activeResult, value } = state;
		const { 
			results,
			extraResults,
			error,
			loading,
			autofocus,
			closeWhenNoResults,
			closeWhenNoValue,
			startTypingText,
			noResultsText,
			loadingText,
			loadingIconName,
		} = props;
		const onResultClick = this.onResultClick.bind(this);

		let dropdownOpen = this.state.open;
		if(closeWhenNoResults && results.length < 1) dropdownOpen = false;
		if(closeWhenNoValue && !this.state.value) dropdownOpen = false;

		return (
			<div 
				ref={ref => this.rootRef = ref} 
				className={`dropdown ${dropdownOpen && "is-active"}`}
				onKeyDown={this.onKeyPress.bind(this)}
				style={{width: "100%"}}
			>
				<div className="dropdown-trigger" style={{width: "100%"}}>
					<div className="field">
						<div className="control">
							<input
								ref={ref => this.inputRef = ref}
								autofocus={autofocus}
								value={value}
								onClick={this.open.bind(this)}
								onFocus={this.open.bind(this)}
								onInput={this.onInput.bind(this)}
								onChange={this.onChange.bind(this)}
								className={`input ${dropdownOpen && "is-focused"} ${error && "is-danger"}`}
							/>
						</div>
						{error &&
							<p className="help is-danger">{error}</p>
						}
					</div>
				</div>
				<div className="dropdown-menu" style={{width: "100%"}} >
					<div className="dropdown-content">
						{results.map(function(name, index){
							return (
								<a onClick={() => onResultClick(name)} className={`dropdown-item ${index === activeResult && "is-active"}`}>{name}</a>
							);
						})}

						{loading &&
							<div className="dropdown-item">
								<div className="level">
									<div className="level-left">
										{loadingText || "Loading..."}
									</div>
									<div className="level-right">
										<i className={`fa fa-spin ${loadingIconName || "fa-circle-o-notch"}`} />
									</div>
								</div>
							</div>
						}

						{(results.length < 1 && value && !loading) &&
							<div className="dropdown-item">
								{noResultsText || "No results..."}
							</div>
						}

						{(results.length < 1 && !value) &&
							<div className="dropdown-item">
								{startTypingText || "Start typing to search..."}
							</div>
						}

						{extraResults && results &&
							<hr className="dropdown-divider" />
						}
						{_.map(extraResults, function(name, index){
							return (
								<a onClick={() => onResultClick(name)} className="dropdown-item">{name}</a>
							);
						})}
					</div>
				</div>
			</div>
		);
	}

	/**
	 * Open the dropdown
	 */
	open(){
		if(this.canOpen()){
			this.setState({open: true});
			if(this.state.value){
				if(_.isFunction(this.props.onInput))
					this.props.onInput({value: this.state.value});
			}
		}
	}

	/**
	 * @returns {Bool} Whether the dropdown can be opened or not
	 */
	canOpen(){
		if(!this.props.openWhenEmpty && !this.state.value)
			return false;

		return true;
	}

	/**
	 * Close the dropdown menu
	 */
	close(){
		this.setState({open: false, activeResult: -1});
	}

	/**
	 * Attempt to close the dropdown menu if the user has clicked away
	 */
	attemptClose(){
		if(this.rootRef){
			if(!this.rootRef.contains(document.target) && !this.rootRef.contains(document.activeElement)){
				if(this.state.open){
					this.close();
					this.props.onChange({value: this.state.value});
				}
			}
		}
	}

	/**
	 * Callback function called whenever the user changes the input
	 * @param {Object} e The event object
	 */
	onInput(e){
		const _this = this;

		// There is some value in the input, open the dropdown and call props.onInput
		if(e.target.value){
			this.setState({value: e.target.value}, function(){
				_this.open();
				_this.props.onInput({value: e.target.value});
			});

		// The input value is empty, close dropdown if needed to and call props.onRequestClearResults
		}else{
			if(!this.props.openWhenEmpty)
				this.close();
			
			this.setState({value: e.target.value});
			this.props.onRequestClearResults();
		}
	}

	/**
	 * Callback function called whenever the value of the input changes (lost focus)
	 * @param {Object} e The event object
	 */
	onChange(e){
		this.setState({value: e.target.value});
		if(this.props.disableKeyboard){
			if(this.inputRef) this.inputRef.blur();
			this.props.onChange({value: e.target.value});
			this.close();
		}
	}

	/**
	 * Callback function called whenever the user has clicked a result with the mouse
	 * @param {String} result The value of the result the user clicked
	 */
	onResultClick(result){
		const _this = this;
		this.setState({value: result}, function(){
			_this.props.onChange({value: result});
			_this.close();
		});
	}

	/**
	 * Callback function called whenever the user presses a key on the input
	 * @param {Object} e The event object
	 */
	onKeyPress(e){
		if(this.props.disableKeyboard) return;

		const k = e.keycode || e.which;

		if(k === 38 || k === 40){
			e.preventDefault();
			let newActive = k === 38 ? this.state.activeResult - 1 : this.state.activeResult + 1;
			if(newActive > this.props.results.length - 1)
				newActive = -1;
			else if(newActive < -1)
				newActive = this.props.results.length - 1;

			this.setState({activeResult: newActive});
		}else if(k === 13){
			e.preventDefault();
			const value = this.state.activeResult < 0 ? this.state.value : this.props.results[this.state.activeResult];
			this.props.onChange({value});
			
			const _this = this;
			this.setState({value}, function(){
				_this.close();
				// Blur the input if we can
				if (_this.inputRef) _this.inputRef.blur();

			});
		}
	}
}