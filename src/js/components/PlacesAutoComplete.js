import { h, Component } from "preact";
import _ from "lodash";

import AutoComplete from "./AutoComplete";

import { placesAutoComplete } from "../util/api/gapi";

/**
 * @property {object[]} this.state.predictions
 * @property {boolean} this.state.loading
 * @property {object} this.state.chosenPrediction
 * 
 * @param {func} this.props.onChange A callback function called whenever the chosen place has changed. Is called with the object of the prediction
 * @param {string} this.props.initialValue The initial value of the input
 */
export default class PlacesAutoComplete extends Component {
	constructor(props){
		super(props);

		this.state = {
			predictions: [],
			loading: false,
			chosenPrediction: {}
		};
	}

	clearPredictions(){
		this.setState({predictions: []});
	}

	onInput({value}){
		const _this = this;
		this.setState({loading: true}, function(){
			placesAutoComplete(value).then(function(response){
				_this.setState({loading: false, predictions: response.data.json.predictions});
			}).catch(function(){
				_this.setState({loading: false, predictions: []});
			});
		});
	}

	onChange({value}){
		let chosenPrediction = {};
		_.each(this.state.predictions, p => {
			if(p.description === value){
				chosenPrediction = p;
				return false;
			}
		});

		this.setState({chosenPrediction});
		if(_.isFunction(this.props.onChange))
			this.props.onChange(chosenPrediction);
	}

	render(props, state){
		return (
			<div>
				<AutoComplete
					initialValue={props.initialValue}
					loading={state.loading}
					results={_.map(state.predictions, p => p.description)}
					onRequestClearResults={this.clearPredictions.bind(this)}
					onInput={this.onInput.bind(this)}
					onChange={this.onChange.bind(this)}
				/>
			</div>
		);
	}
}