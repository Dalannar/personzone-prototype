const path = require("path");
const webpack = require("webpack");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

function isExternal(module){
	const context = module.context;

	if(typeof context != "string")
		return false;

	return context.indexOf("node_modules") !== -1;
}

module.exports = {

	// Entry point for webpack.
	// Add multiple lines for each SPA
	entry: {
		"pz-header": path.join(__dirname, "src", "js", "pz-header"),
		"pz-register": path.join(__dirname, "src", "js", "pz-register"),
		"pz-zoneSidebar": path.join(__dirname, "src", "js", "pz-zoneSidebar"),
		"pz-subzoneList": path.join(__dirname, "src", "js", "pz-subzoneList"),
		"pz-zonetraitList": path.join(__dirname, "src", "js", "pz-zonetraitList"),
		"pz-rankings": path.join(__dirname, "src", "js", "pz-rankings"),
		"pz-testing": path.join(__dirname, "src", "js", "pz-testing"),
		"pz-welcome": path.join(__dirname, "src", "js", "pz-welcome"),
		"pz-feedback": path.join(__dirname, "src", "js", "pz-feedback"),
		"pz-comment": path.join(__dirname, "src", "js", "pz-comment"),
		"pz-settings": path.join(__dirname, "src", "js", "pz-settings"),

		"dev-util-gapi": path.join(__dirname, "src", "js", "dev-util-gapi"),
	},

	// The output of the compilation
	output: {
		path: path.join(__dirname, "public", "scripts"),
		filename: "[name].min.js"
	},

	// The modules to use for compiling
	module: {
		loaders: [
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				loader: "babel-loader",
				query: {
					presets: ["react", "es2015", "stage-0"],
					plugins: [
						["transform-react-jsx", { pragma: "h"}]
					],
				}
			},
		]

	},

	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			include: /\.min\.js$/,
			minimize: true
		}),

		new webpack.optimize.CommonsChunkPlugin({
			name: "vendors",
			minChunks: function(module){
				return isExternal(module);
			}
		}),

		new BundleAnalyzerPlugin({
			analyzerMode: "static",
			reportFilename: "../../project/bundles_report.html",
			openAnalyzer: false,
			statsFilename: "../../project/bundles_report_stats.json"
		}),
	]
};