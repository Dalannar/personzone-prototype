const axios = require("axios");
const assert = require("chai").assert;
const winston = require("winston");
const sequelize = require("sequelize");

try {
	require("../util/node").loadEnvironment([
		"TEST_SERVER_HOST", "TEST_SERVER_PORT",
		"TEST_DATABASE_HOST", "TEST_DATABASE_PORT",
		"TEST_DATABASE_USER", "TEST_DATABASE_PASS",
		"TEST_DATABASE_NAME",
	]);
} catch (e) {
	winston.error(e);
	process.exit(-1);
}

describe("Comments API", function(){
	const db = require("../lib/database")(
		process.env.TEST_DATABASE_HOST,
		process.env.TEST_DATABASE_PORT,
		process.env.TEST_DATABASE_USER,
		process.env.TEST_DATABASE_PASS,
		process.env.TEST_DATABASE_NAME
	);

	before("open database connection", function(done){
		db.sync().then(() => done()).catch(done);
	});

	const comRecUser = {
		first_name: "Comment",
		last_name: "Receiver",
		purl: "commentReceiver",
		email: "commentreceiver@yopmail.com",
		password: "pass",
	};
	let comRecCookie = null;
	let comRecZoneId = null;
	let comRecSubzoneId = null;

	const comMakUser = {
		first_name: "Comment",
		last_name: "Maker",
		purl: "commentMaker",
		email: "commentmaker@yopmail.com",
		password: "pass",
	};
	let comMakId = null;
	let comMakCookie = null;

	before("create comment receiver", function(done){
		axios({
			method: "post",
			url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/users`,
			data: comRecUser
		}).then(function(response){
			comRecCookie = response.headers["set-cookie"];
			done();
		}).catch(done);
	});

	before("get comment receiver zone id", function(done){
		db.models.zone.findOne({
			include: [{
				model: db.models.user,
				where: { purl: comRecUser.purl }
			}]
		}).then(function(zone){
			if(!zone)
				return done(assert.fail());

			comRecZoneId = zone.get("id");
			done();
		}).catch(done);
	}),

	before("create comment receiver subzone", function(done){
		axios({
			method: "post",
			url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/subzones`,
			headers: {
				Cookie: comRecCookie,
			},
			data: {
				zone_id: comRecZoneId,
				subzone: "Random subzone",
			}
		}).then(function(response){
			comRecSubzoneId = response.data.id;
			done();
		}).catch(done);
	});

	before("create comment maker", function(done){
		axios({
			method: "post",
			url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/users`,
			data: comMakUser
		}).then(function(response){
			comMakCookie = response.headers["set-cookie"];
			done();
		}).catch(done);
	});

	before("get comment maker id", function(done){
		db.models.user.findOne({
			where: { purl: comMakUser.purl }
		}).then(function(user){
			comMakId = user.get("id");
			done();
		}).catch(done);
	});

	/**
	 * --------------------------------------------------------------------------------------
	 */
	describe("#create comment", function(){
		const commentText = "This is a sample comment";
		let commentResponse = null;
		
		before("create comment", function(done){
			axios({
				method: "post",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/comments`,
				data: {
					comment: commentText,
					commenter_purl: comMakUser.purl,
					subzone_id: comRecSubzoneId
				},
				headers: {
					Cookie: comMakCookie
				}
			}).then(function(response){
				commentResponse = response;
				done();
			}).catch(done);
		});

		it("should return with status code 201", function(){
			assert.strictEqual(commentResponse.status, 201, `response returned with status code ${commentResponse.status}`);
		});

		it("should create comment object in database", function(done){
			db.models.comment.findById(commentResponse.data.id, {
				include: [ db.models.subzone ],
			}).then(function(comment){
				let err =
					assert.isDefined(comment.get("id"), "comment.id is not defined") ||
					assert.strictEqual(comment.get("subzone_id"), comRecSubzoneId, "comment.subzone_id does not equal the correct subzone id") ||
					assert.strictEqual(comment.get("user_id"), comMakId, "comment.user_id does not equal the commenters id") ||
					assert.strictEqual(comment.get("comment"), commentText, "comment.comment does not equal the comment text supplied") ||
					assert.isFalse(comment.get("thanked"), "comment.thanked is not false") ||
					assert.isNotNull(comment.get("created_at"), "comment.created_at is null") ||
					assert.isNotNull(comment.get("updated_at"), "comment.updated_at is null");
				done(err);
			}).catch(done);
		});
	});

	after("delete both users", function(done){
		db.models.user.destroy({
			where: {
				[sequelize.Op.or]: [{
					purl: comRecUser.purl
				},{
					purl: comMakUser.purl
				}]
			}
		}).then(function(num){
			if(num !== 2)
				return done(assert.fail());

			done();
		}).catch(done);
	});

	after("close database connection", function(){
		db.close();
	});
});