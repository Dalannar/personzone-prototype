const assert = require("chai").assert;
const _ = require("lodash");

const validation = require("../util/validation");

describe("Validation", function(){

	const maleFirstNames = require("../test_data/maleFirstNamesList");
	const femaleFirstNames = require("../test_data/femaleFirstNamesList");
	const lastNames = require("../test_data/lastNamesList");

	describe("#user names", function(){
		it("should pass when testing valid user names", function(){
			_.forEach(_.concat(maleFirstNames, femaleFirstNames, lastNames), function(name){
				return assert.isTrue(validation.isUserName(name));
			});
		});
	});
});