const axios = require("axios");
const assert = require("chai").assert;
const winston = require("winston");
const sequelize = require("sequelize");

try {
	require("../util/node").loadEnvironment([
		"TEST_SERVER_HOST", "TEST_SERVER_PORT",
		"TEST_DATABASE_HOST", "TEST_DATABASE_PORT",
		"TEST_DATABASE_USER", "TEST_DATABASE_PASS",
		"TEST_DATABASE_NAME",
	]);
} catch (e) {
	winston.error(e);
	process.exit(-1);
}

describe("Auth API", function(){
	const db = require("../lib/database")(
		process.env.TEST_DATABASE_HOST,
		process.env.TEST_DATABASE_PORT,
		process.env.TEST_DATABASE_USER,
		process.env.TEST_DATABASE_PASS,
		process.env.TEST_DATABASE_NAME
	);

	before("open database connection", function(done){
		db.sync().then(() => done()).catch(done);
	});

	after("close database connection", function(){
		db.close();
	});

	/**
	 * --------------------------------------------------------------------------------------
	 */
	describe("#logout", function(){
		const testUser = {
			first_name: "Pedro Daniel",
			last_name: "Magalhães Marques",
			email: "pedro@yopmail.com",
			purl: "pedro123",
			password: "pass"
		};

		let loginResponse = null;
		let logoutResponse = null;

		before("create test user", function(done){
			axios({
				method: "post",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/users`,
				data: testUser
			}).then(function(response){
				done();
			}).catch(done);
		});

		before("login with newly created user", function(done){
			axios({
				method: "post",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/auth/login`,
				data: {
					email: testUser.email,
					password: testUser.password,
				}
			}).then(function(response){
				loginResponse = response;
				done();
			}).catch(done);
		});

		before("logout with newly created user", function(done){
			axios({
				method: "get",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/auth/logout`,
				headers: {
					Cookie: loginResponse.headers["set-cookie"]
				}
			}).then(function(response){
				logoutResponse = response;
				done();
			}).catch(done);
		});

		after("delete test user", function(done){
			db.models.user.destroy({
				where: { purl: testUser.purl }
			}).then(function(num){
				done(assert.isAbove(num, 0));
			}).catch(done);
		});

		it("should not return a cookie", function(){
			assert.isUndefined(logoutResponse.headers["set-cookie"], "logout response returned with a set-cookie");
		});

		it("should return with status code 200", function(){
			assert.strictEqual(logoutResponse.status, 200, "logout response did not return with status code 200");
		});
	});

	/**
	 * --------------------------------------------------------------------------------------
	 */
	describe("#login with correct credentials", function(){
		const testUser = {
			first_name: "Pedro Daniel",
			last_name: "Magalhães Marques",
			email: "pedro@yopmail.com",
			purl: "pedro123",
			password: "pass"
		};

		let loginResponse = null;
		let initialLastLogin = null;

		before("create test user", function(done){
			axios({
				method: "post",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/users`,
				data: testUser
			}).then(function(response){
				done();
			}).catch(done);
		});

		before("get initial last_login from database", function(done){
			db.models.auth.findOne({
				include: [{
					model: db.models.user,
					where: { purl: testUser.purl }
				}]
			}).then(function(auth){
				initialLastLogin = auth.get("last_login");
				done();
			}).catch(done);
		});

		before("login with newly created user", function(done){
			this.timeout(5000);
			// Need to wait at least one second here to make sure the database knows to update when we next login
			setTimeout(function(){
				axios({
					method: "post",
					url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/auth/login`,
					data: {
						email: testUser.email,
						password: testUser.password,
					}
				}).then(function(response){
					loginResponse = response;
					done();
				}).catch(done);
			}, 1000);
		});

		after("delete test user", function(done){
			db.models.user.destroy({
				where: { purl: testUser.purl }
			}).then(function(num){
				done(assert.isAbove(num, 0));
			}).catch(done);
		});

		it("should return a login cookie", function(){
			assert.isDefined(loginResponse.headers["set-cookie"], "login response did not return with set-cookie");
		});

		it("should return status code 200", function(){
			assert.strictEqual(loginResponse.status, 200, "login response did not return with status code 200");
		});
		
		it("should update last_login in database", function(done){
			db.models.auth.findOne({
				include: [{
					model: db.models.user,
					where: { purl: testUser.purl },
				}]
			}).then(function(auth){
				done(assert.isAbove(auth.get("last_login"), initialLastLogin), "auth.last_login is not greater then last_login on register");
			}).catch(done);
		});
	});
});