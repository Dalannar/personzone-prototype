const axios = require("axios");
const assert = require("chai").assert;
const winston = require("winston");
const sequelize = require("sequelize");
const _ = require("lodash");

try {
	require("../util/node").loadEnvironment([
		"TEST_SERVER_HOST", "TEST_SERVER_PORT",
		"TEST_DATABASE_HOST", "TEST_DATABASE_PORT",
		"TEST_DATABASE_USER", "TEST_DATABASE_PASS",
		"TEST_DATABASE_NAME",
	]);
} catch (e) {
	winston.error(e);
	process.exit(-1);
}

describe("Users API", function(){
	const db = require("../lib/database")(
		process.env.TEST_DATABASE_HOST,
		process.env.TEST_DATABASE_PORT,
		process.env.TEST_DATABASE_USER,
		process.env.TEST_DATABASE_PASS,
		process.env.TEST_DATABASE_NAME
	);

	before("open database connection", function(done){
		db.sync().then(() => done()).catch(done);
	});

	after("close database connection", function(){
		db.close();
	});

	/**
	 * -------------------------------------------------------------
	 */
	describe("#create user without required variables", function(){
		const testUser = {
			name: "pedro daniel magalhaes marques",
			email: "pedro@yopmail.com",
			password: "pass",
		};

		afterEach("assure user was not created in database", function(done){
			db.models.user.findOne({
				where: {
					email: testUser.email
				},
			}).then(function(user){
				done(assert.isNull(user, "user exists in database"));
			}).catch(done);
		});

		_.forEach(testUser, function(v, k){
			it(`should fail without ${k} in parameters`, function(done){
				axios({
					method: "post",
					url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/users`,
					data: _.omit(testUser, k),
				}).then(function(response){
					done(assert.fail(null, null, "server response status is not 400"));
				}).catch(function(error){
					done(assert.strictEqual(error.response.status, 400, "server response status is not 400"));
				});
			});
		});
	});

	/**
	 * -------------------------------------------------------------
	 */
	describe("#create user", function(){
		const testUser = {
			name: "Pedro Daniel Magalhaes Marques",
			email: "pedro@yopmail.com",
			password: "pass"
		};
		let apiResponse = null;

		before("create user", function(done){
			axios({
				method: "post",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/users`,
				data: testUser,
			}).then(function(response){
				apiResponse = response;
				return done();
			}).catch(done);
		});

		after("delete user", function(done){
			db.models.user.destroy({
				where: { email: testUser.email }
			}).then(() => done()).catch(done);
		});

		it("should create user in database", function(){

		});

		it("should return user's purl", function(){
			assert.isNotEmpty(apiResponse.data.purl, "api response did not return user's purl");
		});
		it("should return 201", function(){
			assert.strictEqual(apiResponse.status, 201, "api response is not 201");
		});

		it("should create user in database with default values");

	});
});