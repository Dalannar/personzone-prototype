const axios = require("axios");
const assert = require("chai").assert;
const winston = require("winston");
const sequelize = require("sequelize");

try {
	require("../util/node").loadEnvironment([
		"TEST_SERVER_HOST", "TEST_SERVER_PORT",
		"TEST_DATABASE_HOST", "TEST_DATABASE_PORT",
		"TEST_DATABASE_USER", "TEST_DATABASE_PASS",
		"TEST_DATABASE_NAME",
	]);
} catch (e) {
	winston.error(e);
	process.exit(-1);
}

describe("Availability API", function(){
	const db = require("../lib/database")(
		process.env.TEST_DATABASE_HOST,
		process.env.TEST_DATABASE_PORT,
		process.env.TEST_DATABASE_USER,
		process.env.TEST_DATABASE_PASS,
		process.env.TEST_DATABASE_NAME
	);

	before("open database connection", function(done){
		db.sync().then(() => done()).catch(done);
	});

	after("close database connection", function(){
		db.close();
	});

	/**
	 * --------------------------------------------------------------------------------------
	 */
	describe("#basic availability", function(){
		const testUser = {
			first_name: "Pedro Daniel",
			last_name: "Magalhães Marques",
			email: "pedro@yopmail.com",
			purl: "pedro123",
			password: "pass"
		};

		before("create test user", function(done){
			axios({
				method: "post",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/users`,
				data: testUser
			}).then(function(response){
				done();
			}).catch(done);
		});

		after("delete test user", function(done){
			db.models.user.destroy({
				where: { purl: testUser.purl }
			}).then(function(num){
				done(assert.isAbove(num, 0));
			}).catch(done);
		});

		it("should pass with available email", function(done){
			axios({
				method: "get",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/availability/email/validEmail@gmail.com`,
			}).then(function(response){
				let err =
					assert.strictEqual(response.status, 200, "response did not return with status code 200") ||
					assert.isTrue(response.data.available, "response did not return with 'available: true'");
				done(err);
			}).catch(done);
		});

		it("should fail with unavailable email", function(done){
			axios({
				method: "get",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/availability/email/${testUser.email}`,
			}).then(function(response){
				let err =
					assert.strictEqual(response.status, 200, "response did not return with status code 200") ||
					assert.isFalse(response.data.available, "response did not return with 'available: false'");
				done(err);
			}).catch(done);
		});

		it("should pass with available purl", function(done){
			axios({
				method: "get",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/availability/purl/availablepurl`,
			}).then(function(response){
				let err =
					assert.strictEqual(response.status, 200, "response did not return with status code 200") ||
					assert.isTrue(response.data.available, "response did not return with 'available: true'");
				done(err);
			}).catch(done);
		});

		it("should fail with unavailable purl", function(done){
			axios({
				method: "get",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/availability/purl/${testUser.purl}`,
			}).then(function(response){
				let err =
					assert.strictEqual(response.status, 200, "response did not return with status code 200") ||
					assert.isFalse(response.data.available, "response did not return with 'available: false'");
				done(err);
			}).catch(done);
		});

		it("should fail without email value", function(done){
			axios({
				method: "get",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/availability/email`,
			}).then(function(response){
				done(assert.strictEqual(response.status, 400, "response did not return with status code 400"));
			}).catch(function(err){
				done(assert.strictEqual(err.response.status, 400, "response did not return with status code 400"));
			});
		});

		it("should fail without purl value", function(done){
			axios({
				method: "get",
				url: `${process.env.TEST_SERVER_HOST}:${process.env.TEST_SERVER_PORT}/api/availability/purl`,
			}).then(function(response){
				done(assert.strictEqual(response.status, 400, "response did not return with status code 400"));
			}).catch(function(err){
				done(assert.strictEqual(err.response.status, 400, "response did not return with status code 400"));
			});
		});
	});
});