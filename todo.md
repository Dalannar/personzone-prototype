#New todo list#

#Critical#
	










#Critical#
- [x*] Sanitize avatar upload

- [] New Translatable: Check to see if there is a problem with trying to load translation data from the <meta> if the DOMContent is not loaded

- [] Need to look at rankings query! I suspect a person might be able to appear multiple times in the ranking, once for each zone with that trait

- [] Add translations to rankings

- [] Add a "powered by google" on the search bars without a google map

#Medium importance#
- [] Test get flow-ratings with specific zone selection (create test for this as well)

- [] Write tests for /ratings/zone-ratings

- [x] Migrate to the latest version of sequelize (I swear new versions just keep popping up all the time...)

- [] Update validation.isComment function. Currently just returns true to everything

- [] Add date of birth and gender to registration and database

- [] Add admin support and make sure only admins can use the "rpi" (render properties inspector) functionality

- [] Bug with the AutoComplete component. If keyboard is disabled, clicking with the mouse on a result won't work the first time

#Low importance#
- [] Update db-pop with -verbose setting (show progress, how many ratings it generated, etc...)

- [] Check db-pop-ratings. Doing a large amount of ratings with bulkCreate seems to just break everything and completely block forever. (I've found 20000 ratings to be fine and quick, but 70000 is too much and blocked the entire script (no errors))

- [] Check api calls input validation when it comes to value lenght. Perphaps we need to be limiting to 'x' characters

- [x] Check what is causing the "promise handler not used" when running database_populate

- [x] Change the use of primary color in buttons according to Andre

- [] Delete all fake data used during api tests (example: users are deleted but not their auths object)

- [] Allow the user to change the order of the zones in the zone list

- [] On update user API, see about returning the new values back to the user. By not sending the values that we actually store in the database the client has no idea what to store on it's end (example, the client won't (shouldn't) know if it needs to capitalize the name or not)

- [] Create additional i18n tests to test if we get the appropriate locale if there is no cookie set... (how to do this ?)

- [] When creating new subzone, compare the name without trailing spaces at the end (to existing subzones). Right now "a " != "a", but fails when creating it

- [] Change pz-util into just util and maybe some other changes (pz-* should reference webpack entry points)

- [] Change getJSON accessLevels and include 'admin' as one of them

- [] Move /welcome route to it's own file (?)

- [] Create system for changing routes easily (change localhost:3000/welcome => localhost:3000/app/welcome)

- [] Add created_at and updated_at to trait.getData() => define in ACD

 #Login#

 - [] Add functionality to remember me checkbox