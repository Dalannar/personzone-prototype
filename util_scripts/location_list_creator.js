const googleMaps = require("@google/maps");
const inquirer = require("inquirer");
const fs = require("fs");
const winston = require("winston");
const _ = require("lodash");

const { loadEnvironment } = require("../util/node");
const { extractLocationDetails } = require("../util/gapi");
const OUT_FILE_NAME = "locations.js";

const env = loadEnvironment(["google_maps_api_key"]);

const gapi = googleMaps.createClient({
	key: env.google_maps_api_key
});

let places = [];

async function readList(){
	return Promise.all(_.map(require(`../${OUT_FILE_NAME}`)), function(place){
		return new Promise(async (resolve, reject) => {
			const nPlace = await getDetails(place.place_id);
			places.push(nPlace);
			resolve();
		});
	});
}

function finish(exitCode=0){
	let out = "module.exports = [\n";
	_.each(places, place => {
		out += `\t${JSON.stringify(place)},\n`;
	});
	out+= "];";
	fs.writeFile(OUT_FILE_NAME, out, err => {
		if(err)
			winston.error(err);
		process.exit(err ? -1 : exitCode);
	});
}

async function getInput(){
	const {input} = await inquirer.prompt([{
		name: "input",
		type: "input",
		message: "Enter a name to search, or exit"
	}]);

	return input;
}

async function mainLoop(){
	try {
		const input = await getInput();
		if(input === "exit")
			return finish();

		if(input === "read list"){
			await readList();
			return finish();
		}

		const predictions = await getPredictions(input);
		if(_.isEmpty(predictions)){
			console.log(`Could not find any predictions for input: ${input}`);
			return null;
		}
		const place = await confirmPlace(predictions);
		if(!place)
			return null;

		const details = _.assign({}, await getDetails(place.place_id), {place_id: place.place_id});
		places.push(details);

	} catch (err) {
		console.log("There was an error");
		console.log(err);
		finish(-1);
	}
}

function getDetails(placeid){
	return new Promise(function(resolve, reject){
		gapi.place({placeid}, function(err, response){
			if(err)
				return reject(err);
			
			resolve(extractLocationDetails(response.json.result));
		});
	});
}

async function confirmPlace(predictions){
	let { place } = await inquirer.prompt([{
		name: "place",
		type: "list",
		choices: _.concat(_.map(predictions, p => p.description), "none of above"),
		message: "Choose a location"
	}]);

	if(place === "none of above")
		return false;

	_.each(predictions, prediction => {
		if(prediction.description === place){
			place = prediction;
			return false;
		}
	});

	return place;
}

async function getPredictions(input){
	return new Promise(function(resolve, reject){
		gapi.placesAutoComplete({input}, function(err, response){
			if(err)
				return reject(err);
			
			return resolve(response.json.predictions);
		});
	});
}

(async function(){
	while(true){
		await mainLoop();
	}
})();