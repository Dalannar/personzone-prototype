const _ = require("lodash");
const inquirer = require("inquirer");
const winston = require("winston");

const { processBatches } = require("../util/generators");

const BATCH_SIZE = 1000;

async function getOptions(){
	const {min, max} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of zonetraits to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of zonetraits to generate?",
		default: 15
	}]);

	return {
		min: _.toNumber(min),
		max: _.toNumber(max),
	};
}

async function main(db, generators){
	return new Promise(async (resolve, reject) => {
		const [{min, max}, nZones, traitIds] = await Promise.all([
			getOptions(),
			db.models.zone.count(),
			_.map(await db.models.trait.findAll({attributes: ["id"]}), trait => trait.get("id"))
		]).catch(err => reject(err));

		if(nZones <= 0)
			winston.info("No zones found to generate subzones for");

		let zonetraitsGenerated = 0;
		const nBatches = _.ceil(nZones / BATCH_SIZE);
		async function batchF(batch){
			const zones = await db.models.zone.findAll({
				attributes: ["id"],
				include: [{
					model: db.models.zonetrait,
					attributes: ["trait_id"]
				}],
				limit: BATCH_SIZE,
				offset: BATCH_SIZE * batch
			});

			let zonetraits = [];
			_.each(zones, zone => {
				let usedTraitIds = _.map(zone.get("zonetraits"), zonetrait => zonetrait.get("trait_id"));
				let usableTraitIds = _.without(traitIds, ...usedTraitIds);
				_.each(_.range(_.random(min, max)), () => {
					const zonetrait = generators.zonetrait.generate({zone_id: zone.get("id"), trait_ids: usableTraitIds});
					if(zonetrait){
						usableTraitIds = _.without(usableTraitIds, zonetrait.trait_id);
						zonetraits.push(zonetrait);
					}
				});
			});

			return db.transaction(transaction => {
				return db.models.zonetrait.bulkCreate(zonetraits, {transaction}).then(() => {
					zonetraitsGenerated += zonetraits.length;
				});
			});
		}

		await processBatches(nBatches, batchF).catch(err => reject(err));
		winston.info(`Generated ${zonetraitsGenerated} total zonetraits`);
		return resolve();
	});
}

module.exports = main;