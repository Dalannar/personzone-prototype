const _ = require("lodash");
const inquirer = require("inquirer");
const winston = require("winston");

const { processBatches } = require("../util/generators");

const BATCH_SIZE = 1000;

async function getOptions(){
	const {min, max} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of ratings to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of ratings to generate?",
		default: 40
	}]);

	return {
		min: _.toNumber(min),
		max: _.toNumber(max),
	};
}

async function main(db, generators){
	return new Promise(async (resolve, reject) => {
		const [{min, max}, nZonetraits, userIds] = await Promise.all([
			getOptions(),
			db.models.zonetrait.count(),
			_.map(await db.models.user.findAll({attributes: ["id"]}), user => user.get("id"))
		]).catch(err => reject(err));

		if(nZonetraits <= 0)
			winston.info("No zonetraits found to generate ratings for");

		let ratingsGenerated = 0;
		const nBatches = _.ceil(nZonetraits / BATCH_SIZE);
		async function batchF(batch){
			const zonetraits = await db.models.zonetrait.findAll({
				attributes: ["id", "zone_id"],
				include: [{
					model: db.models.rating,
					attributes: ["user_id"]
				}, {
					model: db.models.zone,
					attributes: ["user_id"]
				}],
				limit: BATCH_SIZE,
				offset: BATCH_SIZE * batch
			});

			let ratings = [];
			_.each(zonetraits, zonetrait => {
				let usedUserIds = _.map(zonetrait.get("ratings"), rating => rating.get("user_id"));
				usedUserIds.push(zonetrait.get("zone").get("user_id"));
				let usableUserIds = _.without(userIds, ...usedUserIds);

				_.each(_.range(_.random(min, max)), () => {
					const rating = generators.rating.generate({zonetrait_id: zonetrait.get("id"), user_ids: usableUserIds});
					if(rating){
						usableUserIds = _.without(usableUserIds, rating.user_id);
						ratings.push(rating);
					}
				});
			});

			return db.transaction(transaction => {
				return db.models.rating.bulkCreate(ratings, {transaction}).then(() => {
					ratingsGenerated += ratings.length;
				});
			});
		}

		await processBatches(nBatches, batchF).catch(err => reject(err));
		winston.info(`Generated ${ratingsGenerated} total ratings`);
		return resolve();
	});
}

module.exports = main;