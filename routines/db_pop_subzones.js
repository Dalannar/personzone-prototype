const _ = require("lodash");
const inquirer = require("inquirer");
const winston = require("winston");

const { processBatches } = require("../util/generators");

const BATCH_SIZE = 1000;

async function getOptions(){
	const {min, max} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of subzones to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of subzones to generate?",
		default: 15
	}]);

	return {
		min: _.toNumber(min),
		max: _.toNumber(max),
	};
}

async function main(db, generators){
	return new Promise(async (resolve, reject) => {
		const [{min, max}, nZones] = await Promise.all([
			getOptions(),
			db.models.zone.count()
		]).catch(err => reject(err));

		if(nZones <= 0)
			winston.info("No zones found to generate subzones for");

		let subzonesGenerated = 0;
		const nBatches = _.ceil(nZones / BATCH_SIZE);
		async function batchF(batch){
			const zones = await db.models.zone.findAll({
				attributes: ["id"],
				include: [{
					model: db.models.subzone,
					attributes: ["name"]
				}],
				limit: BATCH_SIZE,
				offset: BATCH_SIZE * batch
			});

			let subzones = [];
			_.each(zones, zone => {
				let usedNames = _.map(zone.get("subzones"), subzone => subzone.get("name"));
				_.each(_.range(_.random(min, max)), () => {
					const subzone = generators.subzone.generate({zone_id: zone.get("id"), used_names: usedNames});
					if(subzone){
						usedNames.push(subzone.name);
						subzones.push(subzone);
					}
				});
			});

			return db.transaction(transaction => {
				return db.models.subzone.bulkCreate(subzones, {transaction}).then(() => {
					subzonesGenerated += subzones.length;
				});
			});
		}

		await processBatches(nBatches, batchF).catch(err => reject(err));
		winston.info(`Generated ${subzonesGenerated} total subzones`);
		return resolve();
	});
}

module.exports = main;