const _ = require("lodash");
const inquirer = require("inquirer");
const winston = require("winston");

const { processBatches } = require("../util/generators");

const BATCH_SIZE = 1000;

async function getOptions(){
	const {min, max} = await inquirer.prompt([{
		name: "min",
		type: "input",
		message: "Minimum number of comments to generate?",
		default: 0,
	},{
		name: "max",
		type: "input",
		message: "Maximum number of comments to generate?",
		default: 40
	}]);

	return {
		min: _.toNumber(min),
		max: _.toNumber(max),
	};
}

async function main(db, generators){
	return new Promise(async (resolve, reject) => {
		const [{min, max}, nSubzones, userIds] = await Promise.all([
			getOptions(),
			db.models.subzone.count(),
			_.map(await db.models.user.findAll({attributes: ["id"]}), user => user.get("id"))
		]).catch(err => reject(err));

		if(nSubzones <= 0)
			winston.info("No subzones found to generate comments for");

		let commentsGenerated = 0;
		const nBatches = _.ceil(nSubzones / BATCH_SIZE);
		async function batchF(batch){
			const subzones = await db.models.subzone.findAll({
				attributes: ["id", "zone_id"],
				include: [{
					model: db.models.zone,
					attributes: ["user_id"]
				}],
				limit: BATCH_SIZE,
				offset: BATCH_SIZE * batch
			});

			let comments = [];
			_.each(subzones, subzone => {
				let usableUserIds = _.without(userIds, subzone.get("zone").get("user_id"));
				_.each(_.range(_.random(min, max)), () => {
					const comment = generators.comment.generate({subzone_id: subzone.get("id"), user_ids: usableUserIds});
					if(comment)
						comments.push(comment);
				});
			});

			return db.transaction(transaction => {
				return db.models.comment.bulkCreate(comments, {transaction}).then(() => {
					commentsGenerated += comments.length;
				});
			});
		}

		await processBatches(nBatches, batchF).catch(err => reject(err));
		winston.info(`Generated ${commentsGenerated} total comments`);
		return resolve();
	});
}

module.exports = main;