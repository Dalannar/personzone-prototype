const winston = require("winston");
const _ = require("lodash");
const { processBatches } = require("../util/generators");

const env = require("../util/node").loadEnvironment([
	"DATABASE_HOST", "DATABASE_PORT", "DATABASE_USER", "DATABASE_PASS", "DATABASE_NAME"
]);

winston.info(`Running ${__filename}`);
winston.info(`Targeted database: ${process.env.DATABASE_USER}@${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}`);
winston.info(`Database name: ${process.env.DATABASE_NAME}`);

const db = require("../lib/database")(
	env.DATABASE_HOST,
	env.DATABASE_PORT,
	env.DATABASE_USER,
	env.DATABASE_PASS,
	env.DATABASE_NAME
);

const BATCH_SIZE = 200;

function calculateAverage(ratings, prevAvg, prevN){
	const sum = _.reduce(ratings, (sum, rating) => sum + rating.get("rating"), prevAvg*prevN);
	return sum / (ratings.length + prevN);
}

const Op = require("sequelize").Op;

(async function(){
	await db.sync();

	const nOutdatedZonetraits = await db.models.zonetrait.count({
		where: { rank_outdated: true }
	});

	winston.info(`Found ${nOutdatedZonetraits} that need rank updated. On it`);
	let nBatches = _.ceil(nOutdatedZonetraits / BATCH_SIZE);
	
	async function processBatch(batch){
		const zonetraits = await db.models.zonetrait.findAll({
			where: { rank_outdated: true },
			include: [{
				model: db.models.rating,
				where: {
					updated_at: {
						[Op.gte]: db.col("zonetrait.rank_updated_at"),
					}
				}
			}],
			limit: BATCH_SIZE,
			offset: BATCH_SIZE * batch,
		});

		return Promise.all(_.map(zonetraits, function(z){
			z.set("average_rating", calculateAverage(z.get("ratings"), z.get("average_rating"), z.get("number_ratings")));
			z.set("number_ratings", z.get("number_ratings") + z.get("ratings").length);
			z.set("rank_updated_at", db.fn("NOW"));
			z.set("rank_outdated", false);
			return z.save();
		}));
	}

	await processBatches(nBatches, processBatch).catch(() => {});
	winston.info("Done");
	process.exit(0);
})();