/**
 * @todo Add support to choose which generator config file to use
 */

const { loadEnvironment } = require("../util/node");
const inquirer = require("inquirer");
const winston = require("winston");

const env = loadEnvironment(["DATABASE_HOST", "DATABASE_PORT", "DATABASE_USER", "DATABASE_PASS", "DATABASE_NAME"]);

const db = require("../lib/database")(
	env.DATABASE_HOST, env.DATABASE_PORT, env.DATABASE_USER, env.DATABASE_PASS, env.DATABASE_NAME
);
const generators = require("../lib/generators")(require("../lib/generators/config/default"));

db.sync({}).then(async function(){
	while(true){ await mainLoop(); }
}).catch(() => {});

/**
 * Exit the subroutine normally
 */
function exit(){
	db.close().then(() => process.exit(0));
}

let subroutine = null;
async function mainLoop(){
	if(!subroutine)
		subroutine = selectSubroutine;

	const temp = subroutine;
	subroutine = null;
	await temp(db, generators).catch(err => winston.error(err));
}

async function selectSubroutine(){
	const { s } = await inquirer.prompt([{
		name: "s",
		type: "list",
		message: "What type of data to generate?",
		choices: ["users", "subzones", "zonetraits", "ratings", "comments", "exit"]
	}]).catch(err => {winston.error(err); exit();});

	switch(s){
		case "users": return subroutine = require("./db_pop_users");
		case "subzones": return subroutine = require("./db_pop_subzones");
		case "zonetraits": return subroutine = require("./db_pop_zonetraits");
		case "ratings": return subroutine = require("./db_pop_ratings");
		case "comments": return subroutine = require("./db_pop_comments");
		case "exit":
		default:
			return exit();
	}
}