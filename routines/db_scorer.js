/**
 * Basic temporary script for calculating scores for the database instances
 */

const winston = require("winston");
const asyncWhile = require("async-while");
const _ = require("lodash");

// Load environment variables
require("../util/node").loadEnvironment([
	"DATABASE_HOST", "DATABASE_PORT", "DATABASE_USER", "DATABASE_PASS", "DATABASE_NAME"
]);

winston.info(`Running ${__filename}`);
winston.info(`Targeted database: ${process.env.DATABASE_USER}@${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}`);
winston.info(`Database name: ${process.env.DATABASE_NAME}`);

const db = require("../lib/database")(
	process.env.DATABASE_HOST,
	process.env.DATABASE_PORT,
	process.env.DATABASE_USER,
	process.env.DATABASE_PASS,
	process.env.DATABASE_NAME
);

db.sync()
	.then(() => main(db))
	.catch(err => exitWithError(db, "Error with syncing with database", err));

/**
 * 
 */
function exitWithError(db, error, trace){
	winston.error(error);
	winston.error(trace);
	db.close();
	process.exit(-1);
}

/**
 * 
 */
function calculateScore(ratings){
	return _.sumBy(ratings, rating => rating.get("rating"));
}

/**
 * 
 */
function scoreBunch(db, zonetraits){
	const promises = _.map(zonetraits, function(zonetrait){
		zonetrait.set("score", calculateScore(zonetrait.get("ratings")));
		zonetrait.set("score_updated_at", db.fn("NOW"));
		zonetrait.set("outdated_score", false);
		return zonetrait.save();
	});

	return Promise.all(promises);
}

/**
 *  
 */
function main(db){
	winston.info("Scoring zonetraits...");
	db.models.zonetrait.findAll({
		where: { outdated_score: true },
		include: [ db.models.rating ]
	}).then(function(zonetraits){
		winston.info(`Found ${zonetraits.length} zonetraits that need score updating... on it!`);
		
		const zonetraitBunches = _.chunk(zonetraits, 200);
		let updateIndex = 0;

		asyncWhile(function(){
			if(updateIndex < zonetraitBunches.length){
				return true;
			}else{
				winston.info("Finished updating all zonetraints in the database");
				process.exit(0);
				return false;
			}
		}, function(){
			const temp = updateIndex;
			updateIndex += 1;
			winston.info(`Scoring zonetrait bunch ${updateIndex}/${zonetraitBunches.length}`);
			return scoreBunch(db, zonetraitBunches[temp]);
		})();

	}).catch(err => exitWithError(db, "Error with getting zonetraits", err));
}