const _ = require("lodash");
const inquirer = require("inquirer");
const winston = require("winston");

const { processBatches } = require("../util/generators");

const BATCH_SIZE = 2500;

async function getOptions(){
	const {number} = await inquirer.prompt([{
		name: "number",
		type: "input",
		message: "Number of users to generate",
		default: 1000,
	}]);

	return {
		number: _.toNumber(number)
	};
}

async function main(db, generators){
	return new Promise(async (resolve, reject) => {
		const [{number}, maxUserId, maxZoneId, maxLocationId] = await Promise.all([
			getOptions(),
			(await db.models.user.max("id") || 0) + 1,
			(await db.models.zone.max("id") || 0) + 1,
			(await db.models.location.max("id") || 0) + 1,
		]).catch(err => reject(err));
		
		let usersGenerated = 0;
		let zonesGenerated = 0;
		let locationsGenerated = 0;
		let curZoneId = maxZoneId;
		let curLocationId = maxLocationId;
		const nBatches = _.ceil(number / BATCH_SIZE);
		function batchF(batch){
			let users = [];
			let privacies = [];
			let zones = [];
			let auths = [];
			let locations = [];
			let user_locations = [];

			let rangeStart = maxUserId + (batch * BATCH_SIZE);
			let rangeEnd = _.min([maxUserId + ((batch+1) * BATCH_SIZE), maxUserId + number]);

			_.each(_.range(rangeStart, rangeEnd), function(id){
				const data = generators.user.generate({id, zone_id: curZoneId, location_id: curLocationId});
				privacies.push(data.user_privacy);
				zones.push(data.zones);
				auths.push(data.auth);
				locations.push(data.locations);
				users.push(_.omit(data, ["user_privacy", "zones", "auth", "locations"]));
				curZoneId += data.zones.length;
				curLocationId += data.locations.length;

				_.each(data.locations, location => {
					user_locations.push({ user_id: id, location_id: location.id });
				});
			});
	
			zones = _.flatten(zones);
			locations = _.flatten(locations);
	
			return db.transaction(transaction => {
				return db.models.user.bulkCreate(users, {transaction}).then(() => {
					return db.models.auth.bulkCreate(auths, {transaction}).then(() => {
						return db.models.zone.bulkCreate(zones, {transaction}).then(() => {
							return db.models.user_privacy.bulkCreate(privacies, {transaction}).then(() => {
								return db.models.location.bulkCreate(locations, {transaction}).then(() => {
									return db.models.user_location.bulkCreate(user_locations, {transaction}).then(() => {
										usersGenerated += users.length;
										zonesGenerated += zones.length;
										locationsGenerated += locations.length;
									});
								});
							});
						});
					});
				});
			});
		}
	
		await processBatches(nBatches, batchF).catch(err => reject(err));
		winston.info(`Generated ${usersGenerated} total users`);
		winston.info(`Generated ${zonesGenerated} total zones`);
		winston.info(`Generated ${locationsGenerated} total locations`);
		return resolve();
	});
}

module.exports = main;