module.exports = {
	"PRIVACIES": ["private", "public", "friends"],
	"GENDERS": ["O", "M", "F"],
	"BLOOD_TYPES": ["O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"],
	"RELATIONSHIP_STATUSES": ["single", "in_relation", "engaged", "married", "complicated", "open", "widow", "separated", "divorced", "civil_union"],
	"EXPERIENCE_TYPES": ["educational", "professional"],
	"LOCATION_TYPES": ["location", "birth_place"],
};