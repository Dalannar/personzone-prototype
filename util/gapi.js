const _ = require("lodash");

module.exports = {
	/**
	 * Extract location details from a google place object (from placeDetails API)
	 * @param {object} placeObj The place object to extract information from
	 * @returns {object} the extracted information from the place object, in the format of the database
	 */
	extractLocationDetails: function(placeObj){
		let location = {};

		// Extract point location (lat and lng)
		if(placeObj.geometry)
			location.point = { type: "Point", coordinates: [placeObj.geometry.location.lat, placeObj.geometry.location.lng]};

		if(placeObj.formatted_address)
			location.formatted_address = placeObj.formatted_address;

		// Extract textual information
		_.each(placeObj.address_components, component => {
			_.each(component.types, type => {
				switch(type){
					case "administrative_area_level_3":
					case "administrative_area_level_2":
					case "administrative_area_level_1":
					case "locality":
					case "country":
					case "route":
						location[type] = component.long_name;
						return false;
				}
			});
		});

		return location;
	},
};