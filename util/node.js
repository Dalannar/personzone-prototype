const winston = require("winston");

module.exports = {
	
	/**
	 * Load environment variables from .env file (using ".dotenv" node_module)
	 * @param {String[]} [requiredVariables=[]] Any variables that need to be present in the loaded variables
	 * @returns {Object} Returns process.env after environment variables have been loaded into it
	 * @throws {ReferenceError} If any value given in requiredVariables is not present in the loaded environment variables
	 */
	loadEnvironment: function(requiredVariables=[]){
		require("dotenv").config();
		requiredVariables.forEach(function(v){
			if(process.env[v] === undefined)
				throw new ReferenceError(`Required variable '${v}' not present in .env file`);
		});

		return process.env;
	},

	/**
	 * Add a callback function to a SIGINT command. This function is a wrapper around code that enables this for windows systems as well as UNIX
	 * @param {function} f The function to add as callback to SIGINT
	 */
	onSIGINT: function(f){
		if(process.platform === "win32"){
			require("readline").createInterface({
				input: process.stdin,
				output: process.stdout
			}).on("SIGINT", function(){
				process.emit("SIGINT");
			});
		}

		process.on("SIGINT", f);
	}
};