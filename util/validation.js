const _ = require("lodash");
const validator = require("validator");
const regularExpressions = require("./regularExpressions");

/**
 * Return a string that corresponds to the given string in "Word Case"
 * @param {String} str The string to change
 * @returns {String} The string in "Word Case"
 */
function toWordCase(str){
	return _.trim(_.join(_.map(_.split(_.toLower(str), " "), _.upperFirst), " "));
}

/**
 * Check if the value supplied is a valid personal user URL (purl)
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isPurl(value){
	return true;
	//return validator.isAlphanumeric(value);
}

/**
 * Check if the value supplied is a valid zone URL (zurl)
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isZurl(value){
	return validator.matches(value, /^[-a-zA-Z0-9_]+$/i);
}

/**
 * Check if the value supplied is a valid first name
 * @param {String} value The value to check
 * @returns {Bool}
 * 
 * @todo Remove this at some point
 * @deprecated Use isUserName instead
 */
function isFirstName(value){
	return isUserName(value);
}

/**
 * Check if the value supplied is a valid last name
 * @param {String} value The value to check
 * @returns {Bool}
 * 
 * @todo Remove this at some point
 * @deprecated Use isUserName instead
 */
function isLastName(value){
	return isUserName(value);
}

/**
 * Check if the value supplied is a valid user name
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isUserName(value){
	return regularExpressions.userName.test(value);
}

/**
 * Check if the value supplied is a valid email
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isEmail(value){
	return validator.isEmail(value);
}

/**
 * Check if the value supplied is a valid job title
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isJobTitle(value){
	return validator.matches(value, /^[a-zA-Z ]+$/i);
}

/**
 * Check if the value supplied is a valid zone name
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isZoneName(value){
	return validator.matches(value, /^[-a-zA-Z0-9_ ]+$/i);
}

/**
 * Check if the value supplied is a valid comment text
 * @todo This just returns true for now... Update this in the future
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isCommentText(value){
	return true;
}

/**
 * Check if the value supplied is a valid subzone name
 * @param {String} value The value to check
 * @returns {Bool}
 */
function isSubzoneName(value){
	return validator.matches(value, /^[a-zA-Z ]+$/i);
}

module.exports = {
	toWordCase,
	isPurl,
	isZurl,
	isFirstName,	
	isLastName,
	isUserName,
	isEmail,
	isJobTitle,
	isZoneName,
	isCommentText,
	isSubzoneName
};