const XRegExp = require("xregexp");

module.exports = {
	// Regular expression to match on user name
	userName: new XRegExp(
		"^" + // Match at the start of the input
		"(" + // Start group alternation
			"[^\\p{^L}ºª+*]" + // Match on unicode letters excluding º and ª (needs the weird double negative)
			"|" + // Alternation for the group
			"[\\s'-\\.]" + // Or on a separator (space) or single quote (for O'Brien)
		")+" + // End group alternation and repeat it
		"$", // Match at the end of the input
		
		"xi" // Flags
	),
};