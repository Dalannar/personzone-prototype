module.exports = {
	/**
	 * Enable route functions to use async/await
	 * Use this in router.get(..., asyncMiddleware(async function(req, res, next)))
	 * @returns {func} middleware functions to be placed into router functions
	 */
	asyncMiddleware: function(fn){
		return function(req, res, next){
			return Promise.resolve(fn(req, res, next)).catch(next);
		};
	}
};