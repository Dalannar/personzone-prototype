const async = require("async");
const winston = require("winston");

module.exports = {
	/**
	 * Process a function multiple batches
	 * @param {number} number The number of times to run the batch function
	 * @param {func => Promise} func A function to run every batch. Should return a Promise
	 * @returns {Promise} promise that resolves when all the batches have been processed or rejects when an error has occurred
	 */
	processBatches: function(number, func){
		return new Promise(function(resolve, reject){
			let batch = 0;
			return async.whilst(
				function(){ return batch < number; },
				function(callback){
					batch++;
					winston.info(`Processing batch [${batch}/${number}]`);
					return func(batch-1).then(() => callback(null)).catch(err => callback(err));
				},
				function(err){
					return err ? reject(err, batch) : resolve();
				}
			);
		});
	}
};