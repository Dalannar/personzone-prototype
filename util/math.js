const _ = require("lodash");

module.exports = {

	/**
	 * Normalize rng weights so that it may be used in `weightedRNG` function
	 * @param {Object} [weights={}] The weights, in the format of `option` => `int weight`
	 * @returns {Array[]} An array of [<str>, <int>], representing the weights to be used in `weightedRNG`
	 */
	normalizeRNGWeights: function(weights={}){
		let min = 0;
		return _.map(weights, function(v, k){
			min += v;
			
			// Update special cases
			switch(k){
				case "%null%": k = null; break;
				case "%true%": k = true; break;
				case "%false%": k = false; break;
			}

			return [k, min];
		});
	},

	/**
	 * Pick an option randomly based on probability weights. The weights parameter should first be normalized with `normalizeRNGWeights` function
	 * @param {Array[]} weights The weight probabilities to use
	 * @returns {*} The option picked
	 */
	weightedRNG: function(weights=[]){
		if(weights.length < 1)
			return null;
		
		const max = weights[weights.length-1][1];
		const rng = _.random(1, max);
		for(let i=0; i < weights.length; i++){
			if(weights[i][1] >= rng)
				return weights[i][0];
		}
	}
};