/**
 * Main entry point to starting the server
 */

const path = require("path");

// Logging package
const winston = require("winston");
// For some reason need to remove it first and then add it to be able to configure winston
// because winston...
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
	level: process.env.NODE_LOG_LEVEL,
	colorize: true,
	prettyPrint: true,
	humanReadableUnhandledException: true,
});

require("dotenv").config();

const _ = require("lodash");

try{
	require("./util/node").loadEnvironment([
		"NODE_ENV", "NODE_LOG_LEVEL",
		"SERVER_PORT", "SERVER_SESSION_SECRET",
		"DATABASE_HOST", "DATABASE_PORT", "DATABASE_USER", "DATABASE_PASS", "DATABASE_NAME",
		"MAILER_PORT", "MAILER_HOST", "MAILER_USER", "MAILER_PASS",
		"APP_USER_AVATAR_DIR", "APP_MAX_AVATAR_SIZE", "APP_IMAGE_DIR",
		"GOOGLE_MAPS_API_KEY"
	]);

}catch (e){
	if(e instanceof ReferenceError){
		winston.error("Environment does not have a required variable");
		winston.error(e.message);
	}

	throw e;
}

// Define the root path of the app (helps with 'requires')
global.__rootPath = path.resolve(__dirname);
global.__rootname = global.__rootPath; // Don't know if it's needed but perphaps it is used in legacy
// Path to the user avatar dir
global.__userAvatarPath = path.resolve(__dirname, process.env.APP_USER_AVATAR_DIR);
// Path to the public images dir
global.__imagePath = path.resolve(__dirname, process.env.APP_IMAGE_DIR);

const db = require("./lib/database")(
	process.env.DATABASE_HOST,
	process.env.DATABASE_PORT,
	process.env.DATABASE_USER,
	process.env.DATABASE_PASS,
	process.env.DATABASE_NAME
);

db.sync().then(function(){
	require("./lib/server")(db).listen(process.env.SERVER_PORT, function(){
		winston.info("Server is running and listening");
	});
}).catch(function(err){
	winston.error("Database initialization error");
	winston.error(err);
	process.exit(-1);
});
