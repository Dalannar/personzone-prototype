/**
 * Setup script. Build database structure and insert required, pre-defined fields
 * WARNING: Using this script will wipe out the current database and create it anew
 */
const winston = require("winston");
const inquirer = require("inquirer");
const ArgumentParser = require("argparse").ArgumentParser;

const parser = new ArgumentParser({
	description: "Build database on the targeted machine (given by loaded environment variables in .env). Any existing database and data will be wiped out.",
});

parser.addArgument(["-y", "--yes"], {
	help: "Force 'yes' response when prompted to proceed with database creation",
	action: "storeTrue",
	defaultValue: false,
	required: false,
	dest: "force",
});

const args = parser.parseArgs();

// Load environment variables
const env = require("../util/node").loadEnvironment([
	"DATABASE_HOST", "DATABASE_PORT", "DATABASE_USER", "DATABASE_PASS", "DATABASE_NAME"
]);

winston.info(`Running ${__filename}`);
winston.info(`Targeted database: ${env.DATABASE_USER}@${env.DATABASE_HOST}:${env.DATABASE_PORT}`);
winston.info(`Database name: ${env.DATABASE_NAME}`);
winston.info("Running this script will wipe out any data in the above mentioned database.");

inquirer.prompt([{
	name: "proceed",
	message: "Are you sure you wish to proceed ?",
	type: "confirm",
	default: false,
	when: () => !args["force"]
}]).then(function(answers){
	if(answers.proceed || args["force"])
		return buildDatabase();
	
}).catch(function(){});

/**
 * 
 */
function exitWithError(db, error, trace){
	winston.error(error);
	winston.error(trace);
	db.close();
	process.exit(-1);
}

/**
 * 
 */
function buildDatabase(){
	const db = require("../lib/database")(
		env.DATABASE_HOST,
		env.DATABASE_PORT,
		env.DATABASE_USER,
		env.DATABASE_PASS,
		env.DATABASE_NAME
	);
	
	return db.sync({
		force: true,
		alter: true,
	}).then(function(){
		winston.info("Database structure built. Inserting default data...");

		let populatePromises = [
			populateTraits(db)
				.catch(err => exitWithError(db, "There was an error inserting default traits", err))
		];

		return Promise.all(populatePromises)
			.then(function(){
				winston.info("Database built and synced. Exiting...");
				process.exit(0);
			});

	}).catch(err => exitWithError(db, "There was an error syncing the database.", err));
}

/**
 * 
 */
function populateTraits(db){
	const traits = require("./traitsList");
	return db.models.trait.bulkCreate(
		traits.map((o) => ({name: o}))
	);
}