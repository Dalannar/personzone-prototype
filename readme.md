# Personzone prototype
This is the first prototype for personzone

## Setting up the application
1. Run `npm install` - Install all of the dependencies
2. Create '.env' environment configuration file and edit in the following properties:

Parameter | Type | Description
--- | --- | ---
NODE_ENV | enum('production', 'development') | Set the node environment
NODE_LOG_LEVEL | enum('error', 'warn', 'info', 'debug', 'silly') | Set the minimum log level
SERVER_PORT | number | Server's listening port
SERVER_SESSION_SECRET | string | String to use as the server's session secret
DATABASE_HOST | string | The host machine hosting the database ('localhost' or ip)
DATABASE_PORT | number | The port the database is listening on
DATABASE_USER | string | The username to login to the database
DATABASE_PASS | string | The password to login to the database
DATABASE_NAME | string | The name of the database to use
DATABASE_FORCE_SYNC | enum('true', 'false') | If set to true, when initializing the server, the database will be rebuilt and it's data wiped
TEST_SERVER_HOST | string | The hostname of the server to contact when testing api
GOOGLE_MAPS_API_KEY | string | The api key for google maps api requests

3. `npm run webpack` - Compile all of the necessary javascript files for the client side
4. `npm run database-build` - Build the database
5. `npm start` or `npm run nodemon` - Start the server normally or using nodemon (restarts server after every change made to the files)

## NPM commands

1. `npm run nodemon` - Start the server using nodemon (restart after every change to files)
2. `npm run webpack` - Compile all of the javascript client files
3. `npm run webpack-watch` - Same as 'webpack' but recompiling after every change to the files
4. `npm run db-build` - Build the database (wipe all of the contents that may exist)
5. `npm run db-pop` - Populate an existing database with data. Script will give options of what data to generate
6. `npm run db-score` - Score the existing zonetraits which have the outdated_score boolean set to true. Can take a while
7. `npm run database-test-populate` - (LEGACY, use db-pop) Populate the database with testing data (should only be done on a freshly built database)
8. `npm run docs-api-build` - Build the documentation for the api
9. `npm run build-scss` - Build the scss files (in watch mode)

## Routines
### db-build

Build the database on the targeted machine (given by environment variables in .env). This will wipe out any data that may already exist.
CLI arguments:
	-h --help Show the help screen for db-build
	-y --yes Force a yes response when prompted to process with the database building

### db-pop

Populate an existing database with ficticious data. The script will give options on what data to generate

### db-scorer

Update all the scores in the database that have outdated_score set to true

## Differences between development and production:
	logging:
		- development and production log the same things in different levels

## Documentation

Default render properties for app pages:
	- authPurl = Personal URL of the currently logged in user, or null if not logged in
	- locale = Object containing translation phrases, used on client-side javascript
	- t = Polyglot translation function, bound to Polyglot object, used in server-side rendering