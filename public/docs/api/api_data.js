define({ "api": [
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "name": "Login",
    "group": "Auth",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>The email to login with</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>The password to login with</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal url</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/logout",
    "title": "Logout",
    "name": "Logout",
    "group": "Auth",
    "version": "0.0.0",
    "filename": "lib/routes/api/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/availability/email/:email",
    "title": "Check email availability",
    "name": "Check_email_availability",
    "group": "Availability",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>The email to check availability for</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "available",
            "description": "<p>Whether the email is available or not</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/availability.js",
    "groupTitle": "Availability"
  },
  {
    "type": "get",
    "url": "/availability/purl/:purl",
    "title": "Check purl availability",
    "name": "Check_purl_availability",
    "group": "Availability",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "purl",
            "description": "<p>The purl to check availability for</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "available",
            "description": "<p>Whether the purl is available or not</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/availability.js",
    "groupTitle": "Availability"
  },
  {
    "type": "post",
    "url": "/comments/",
    "title": "Create comment",
    "name": "Create_comment",
    "group": "Comments",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "subzone_id",
            "description": "<p>The id of the subzone to comment on</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>The text of the comment</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the newly created comment</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "thanked",
            "description": "<p>Whether or not the comment has been thanked</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>The text of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "user_id",
            "description": "<p>The id of the user that created the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "subzone_id",
            "description": "<p>The id of the subzone that the comment was created on</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The timestamp of when the comment was created</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The timestamp of when the comment was updated</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/comments.js",
    "groupTitle": "Comments"
  },
  {
    "type": "get",
    "url": "/comments/",
    "title": "Search for comments",
    "name": "Search_for_comments",
    "group": "Comments",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "subzone_id",
            "description": "<p>The id of a subzone to restringe the comments about</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "order_by",
            "defaultValue": "newest",
            "description": "<p>The order to apply to the results (accepts &quot;newest&quot; or &quot;oldest&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": true,
            "field": "page",
            "defaultValue": "0",
            "description": "<p>The page of results to get</p>"
          },
          {
            "group": "Parameter",
            "type": "bool",
            "optional": true,
            "field": "remove_thanked",
            "defaultValue": "false",
            "description": "<p>Whether or not to show comments that have been thanked</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "comment",
            "description": "<p>The text of the comment</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The timestamp of when the comment was created</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The timestamp of when the comment was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "thanked",
            "description": "<p>Whether or not the comment has been thanked</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "subzone",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "subzone.id",
            "description": "<p>The id of the subzone the comment is about</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "subzone.subzone",
            "description": "<p>The name of the subzone</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/comments.js",
    "groupTitle": "Comments"
  },
  {
    "type": "get",
    "url": "/comments/:id/thank",
    "title": "Thank comment",
    "name": "Thank_comment",
    "group": "Comments",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the comment to thank</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/comments.js",
    "groupTitle": "Comments"
  },
  {
    "type": "get",
    "url": "/flow-ratings",
    "title": "Get flow ratings",
    "name": "Get_flow_ratings",
    "group": "Ratings",
    "description": "<p>A flow is a link between two users. This api will return all ratings made by user 'a' to user 'b' Note that, if receiver_zurl is provided and a zone with that zurl does not exist the api call will not launch a ZoneNotFoundError. Instead it will quietly respond with an empty array</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "rater_purl",
            "description": "<p>The personal URL of the user that rated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "receiver_purl",
            "description": "<p>The personal URL of the user that was rated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "receiver_zurl",
            "description": "<p>The URL of the zone. If this is not present, then return the ratings for every zone that belongs to the receiver</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the rating</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>The rating</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The date when the rating was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The date when the rating was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zonetrait_id",
            "description": "<p>The id of the zonetrait rated</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>The id of the user that rated</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "zonetrait",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zonetrait.id",
            "description": "<p>The id of the zonetrait this rating belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "zonetrait.created_at",
            "description": "<p>The date when the zonetrait was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "zonetrait.updated_at",
            "description": "<p>The date when the zonetrait was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zonetrait.trait_id",
            "description": "<p>The id of the trait</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zonetrait.zone_id",
            "description": "<p>The id of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "zonetrait.zone",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zonetrait.zone.id",
            "description": "<p>The id of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zonetrait.zone.type",
            "description": "<p>The type of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zonetrait.zone.name",
            "description": "<p>The name of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zonetrait.zone.zurl",
            "description": "<p>The URL of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zonetrait.zone.user_id",
            "description": "<p>The id of the user who owns the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "zonetrait.zone.created_at",
            "description": "<p>The date when the zone was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "zonetrait.zone.updated_at",
            "description": "<p>The date when the zone was updated</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 200 OK\n[\n\t{\n\t\t\"id\": 2,\n\t\t\"rating\": 4,\n\t\t\"created_at\": \"2017-10-25T12:46:55.000Z\",\n\t\t\"updated_at\": \"2017-10-25T12:46:55.000Z\",\n\t\t\"zonetrait_id\": 3,\n\t\t\"user_id\": 2,\n\t\t\"zonetrait\": {\n\t\t\t\"id\": 3,\n\t\t\t\"created_at\": \"2017-10-23T11:12:57.000Z\",\n\t\t\t\"updated_at\": \"2017-10-23T11:12:57.000Z\",\n\t\t\t\"trait_id\": 50,\n\t\t\t\"zone_id\": 1,\n\t\t\t\"zone\": {\n\t\t\t\t\"id\": 1,\n\t\t\t\t\"type\": \"personal\",\n\t\t\t\t\"name\": \"Personal\",\n\t\t\t\t\"zurl\": \"personal\",\n\t\t\t\t\"user_id\": 1,\n\t\t\t\t\"created_at\": \"2017-10-23T11:12:53.000Z\",\n\t\t\t\t\"updated_at\": \"2017-10-23T11:12:53.000Z\"\n\t\t\t}\n\t\t}\n\t}\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/ratings.js",
    "groupTitle": "Ratings",
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>Unauthorized request</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "UserNotFoundError",
            "description": "<p>The user could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"Unauthorized request.\"\n}",
          "type": "json"
        },
        {
          "title": "UserNotFoundError",
          "content": "HTTP/1.1 404 Not Found\n{\n\terror: \"UserNotFoundError\",\n\tmessage: \"User with purl 'pedro123' could not be found.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/",
    "title": "Create subzone for zone",
    "name": "Create_subzone_for_zone",
    "group": "Subzone",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "subzone",
            "description": "<p>The name of the subzone to create</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "zone_id",
            "description": "<p>The id of the zone to add the subzone to</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the newly created subzone</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "zone_id",
            "description": "<p>The id of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "subzone",
            "description": "<p>The name of the newly created subzone</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The date when the subzone was created</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The date when the subzone was last updated</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/subzones.js",
    "groupTitle": "Subzone"
  },
  {
    "type": "delete",
    "url": "/:id",
    "title": "Delete subzone",
    "name": "Delete_subzone",
    "group": "Subzone",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the subzone to delete</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/subzones.js",
    "groupTitle": "Subzone"
  },
  {
    "type": "get",
    "url": "/traits/",
    "title": "Get traits",
    "name": "Get_traits",
    "group": "Traits",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "search",
            "description": "<p>A search term to apply to the results</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": true,
            "field": "limit",
            "defaultValue": "6",
            "description": "<p>A maximum number of results to return</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the trait</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "trait",
            "description": "<p>The name of the trait</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/traits.js",
    "groupTitle": "Traits"
  },
  {
    "type": "post",
    "url": "/users/",
    "title": "Create user",
    "name": "Create_user",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>The user's first name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>The user's last name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The user's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The user's password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "location",
            "description": "<p>The user's location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "job_title",
            "description": "<p>The user's job title</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 201 Created\n{\n\t\"purl\": \"pedro123\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users",
    "error": {
      "fields": {
        "409": [
          {
            "group": "409",
            "optional": false,
            "field": "PurlAlreadyExistsError",
            "description": "<p>Purl already exists and is in use</p>"
          },
          {
            "group": "409",
            "optional": false,
            "field": "EmailAlreadyExistsError",
            "description": "<p>Email already exists and is in use</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "PurlAlreadyExistsError",
          "content": "HTTP/1.1 409 Conflict\n{\n\terror: \"PurlAlreadyExistsError\",\n\tmessage: \"Purl 'pedro123' already exists.\"\n}",
          "type": "json"
        },
        {
          "title": "EmailAlreadyExistsError",
          "content": "HTTP/1.1 409 Conflict\n{\n\terror: \"EmailAlreadyExistsError\",\n\tmessage: \"Email 'pedro@yopmail.com' already exists.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/users/:purl/avatar",
    "title": "Get avatar",
    "name": "Get_avatar",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 200 OK",
          "type": "image/jpeg"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/:purl",
    "title": "Get user",
    "name": "Get_user",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>The user's first name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>The user's last name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The date when the user was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The date when the user was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>The user's location</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "job_title",
            "description": "<p>The user's job title</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "zones",
            "description": "<p>The user's zones</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zones.id",
            "description": "<p>The id of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zones.type",
            "description": "<p>The type of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zones.name",
            "description": "<p>The name of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zones.zurl",
            "description": "<p>The zone's URL</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "zones.created_at",
            "description": "<p>The date when the zone was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "zones.updated_at",
            "description": "<p>The date when the zone was updated</p>"
          }
        ],
        "owner": [
          {
            "group": "owner",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The user's email</p>"
          },
          {
            "group": "owner",
            "type": "Object",
            "optional": false,
            "field": "auth",
            "description": "<p>An object containing authentication items</p>"
          },
          {
            "group": "owner",
            "type": "Bool",
            "optional": false,
            "field": "auth.email_verified",
            "description": "<p>If the user has verified their email or not</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Not logged in:",
          "content": "HTTP/1.1 200 OK\n{\n\t\"id\": 2,\n\t\"first_name\": \"Andre\",\n\t\"last_name\": \"Da Silva\",\n\t\"purl\": \"andre123\",\n\t\"created_at\": \"2017-10-22T11:57:05.000Z\",\n\t\"updated_at\": \"2017-10-22T11:57:05.000Z\",\n\t\"location\": \"Munich\",\n\t\"job_title\": \"Brain Surgeon\",\n\t\"zones\": [\n\t\t{\n\t\t\t\"id\": 3,\n\t\t\t\"type\": \"personal\",\n\t\t\t\"name\": \"Personal\",\n\t\t\t\"zurl\": \"personal\",\n\t\t\t\"created_at\": \"2017-10-22T11:57:05.000Z\",\n\t\t\t\"updated_at\": \"2017-10-22T11:57:05.000Z\"\n\t\t},\n\t\t{\n\t\t\t\"id\": 4,\n\t\t\t\"type\": \"custom\",\n\t\t\t\"name\": \"For Friends\",\n\t\t\t\"zurl\": \"forfriends\",\n\t\t\t\"created_at\": \"2017-10-22T11:57:05.000Z\",\n\t\t\t\"updated_at\": \"2017-10-22T11:57:05.000Z\"\n\t\t}\n\t]\n}",
          "type": "json"
        },
        {
          "title": "Logged in:",
          "content": "HTTP/1.1 200 OK\n{\n\t\"id\": 2,\n\t\"first_name\": \"Andre\",\n\t\"last_name\": \"Da Silva\",\n\t\"purl\": \"andre123\",\n\t\"created_at\": \"2017-10-22T11:57:05.000Z\",\n\t\"updated_at\": \"2017-10-22T11:57:05.000Z\",\n\t\"location\": \"Munich\",\n\t\"job_title\": \"Brain Surgeon\",\n\t\"email\": \"andre@yopmail.com\",\n\t\"auth\": {\n\t\t\"email_verified\": true\n\t},\n\t\"zones\": [\n\t\t{\n\t\t\t\"id\": 3,\n\t\t\t\"type\": \"personal\",\n\t\t\t\"name\": \"Personal\",\n\t\t\t\"zurl\": \"personal\",\n\t\t\t\"created_at\": \"2017-10-22T11:57:05.000Z\",\n\t\t\t\"updated_at\": \"2017-10-22T11:57:05.000Z\"\n\t\t},\n\t\t{\n\t\t\t\"id\": 4,\n\t\t\t\"type\": \"custom\",\n\t\t\t\"name\": \"For Friends\",\n\t\t\t\"zurl\": \"forfriends\",\n\t\t\t\"created_at\": \"2017-10-22T11:57:05.000Z\",\n\t\t\t\"updated_at\": \"2017-10-22T11:57:05.000Z\"\n\t\t}\n\t]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users",
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "UserNotFoundError",
            "description": "<p>The user could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UserNotFoundError",
          "content": "HTTP/1.1 404 Not Found\n{\n\terror: \"UserNotFoundError\",\n\tmessage: \"User with purl 'pedro123' could not be found.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/users/:purl/resend-email-verification",
    "title": "Resend email verification",
    "name": "Resend_email_verification",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users",
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>Unauthorized request</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "optional": false,
            "field": "EmailAlreadyVerifiedError",
            "description": "<p>Email is already verified</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "UserNotFoundError",
            "description": "<p>The user could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UserNotFoundError",
          "content": "HTTP/1.1 404 Not Found\n{\n\terror: \"UserNotFoundError\",\n\tmessage: \"User with purl 'pedro123' could not be found.\"\n}",
          "type": "json"
        },
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"Unauthorized request.\"\n}",
          "type": "json"
        },
        {
          "title": "EmailAlreadyVerifiedError",
          "content": "HTTP/1.1 403 Forbidden\n{\n\terror: \"EmailAlreadyVerifiedError\",\n\tmessage: \"The email is already verified.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/users/",
    "title": "Search users",
    "name": "Search_users",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>The search term to use when searching for users</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "defaultValue": "6",
            "description": "<p>The limit of results to get</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "0",
            "description": "<p>The page of results to get</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>The first name of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>The last name of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal url</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>The user's location</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "job_title",
            "description": "<p>The user's job title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 200 OK\n[\n\t{\n\t\t\"first_name\": \"Pedro Daniel\",\n\t\t\"last_name\": \"Magalhaes Marques\",\n\t\t\"purl\": \"Pedro4341\",\n\t\t\"location\": \"Norwich\",\n\t\t\"job_title\": \"Junior\"\n\t}\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/:purl/avatar",
    "title": "Set avatar",
    "name": "Set_avatar",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "image",
            "description": "<p>The new avatar</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users",
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>Unauthorized request</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"Unauthorized request.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/users/:purl",
    "title": "Update user",
    "name": "Update_user",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "first_name",
            "description": "<p>The user's new first name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "last_name",
            "description": "<p>The user's new last name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "location",
            "description": "<p>The user's new location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "job_title",
            "description": "<p>The user's new job title</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users",
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>Unauthorized request</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "UserNotFoundError",
            "description": "<p>The user could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UserNotFoundError",
          "content": "HTTP/1.1 404 Not Found\n{\n\terror: \"UserNotFoundError\",\n\tmessage: \"User with purl 'pedro123' could not be found.\"\n}",
          "type": "json"
        },
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"Unauthorized request.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/users/:purl/verify-email",
    "title": "Verify email",
    "name": "Verify_email",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "purl",
            "description": "<p>The user's personal URL</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>The email verification hash to test</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/users.js",
    "groupTitle": "Users",
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "UserNotFoundError",
            "description": "<p>The user could not be found</p>"
          }
        ],
        "422": [
          {
            "group": "422",
            "optional": false,
            "field": "InvalidHashError",
            "description": "<p>Hash is not valid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UserNotFoundError",
          "content": "HTTP/1.1 404 Not Found\n{\n\terror: \"UserNotFoundError\",\n\tmessage: \"User with purl 'pedro123' could not be found.\"\n}",
          "type": "json"
        },
        {
          "title": "InvalidHashError",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n\terror: \"InvalidHashError\",\n\tmessage: \"Hash '3f1774e7bb7779160c939a0370f8fbc9' is not valid.\",\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/zones/",
    "title": "Create zone",
    "name": "Create_zone",
    "group": "Zones",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>The type of zone to create</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>The name for the zone</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "zurl",
            "description": "<p>The url for the zone</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "user_id",
            "description": "<p>The id of the user the zone belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>The type of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>The name of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "zurl",
            "description": "<p>The url of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The date when the zone was created</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The date when the zone was updated</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/zones.js",
    "groupTitle": "Zones"
  },
  {
    "type": "delete",
    "url": "/zones/:zurl",
    "title": "Delete zone",
    "name": "Delete_zone",
    "group": "Zones",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "zurl",
            "description": "<p>The url of the zone</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "purl",
            "description": "<p>The personal url of the owner of the zone</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/zones.js",
    "groupTitle": "Zones"
  },
  {
    "type": "get",
    "url": "/zones/:zurl",
    "title": "Get zone",
    "name": "Get_zone",
    "group": "Zones",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "zurl",
            "description": "<p>The zone's url</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "purl",
            "description": "<p>The personal url of the user who owns the zone</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "id",
            "description": "<p>The zone's id</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "user_id",
            "description": "<p>The id of the user that this zone belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>The zone's type</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>The zone's name</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "zurl",
            "description": "<p>The zone's url</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The date when the zone was created</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The date when the zone was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "object[]",
            "optional": false,
            "field": "subzones",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "subzones.id",
            "description": "<p>The id of the subzone</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "subzones.zone_id",
            "description": "<p>The id of the zone this subzone belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "subzone.subzone",
            "description": "<p>The name of the subzone</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "subzone.created_at",
            "description": "<p>The date when the subzone was created</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "subzone.updated_at",
            "description": "<p>The date when the subzone was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "object[]",
            "optional": false,
            "field": "zonetraits",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "zonetraits.id",
            "description": "<p>The id of the zonetrait</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "zonetraits.zone_id",
            "description": "<p>The id of the zone this zonetrait belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "zonetraits.trait_id",
            "description": "<p>The id of the trait this zonetrait represents</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "zonetraits.created_at",
            "description": "<p>The date when the zonetrait was created</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "zonetraits.updated_at",
            "description": "<p>The date when the zonetrait was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "zonetraits.trait",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "zonetraits.trait.id",
            "description": "<p>The id of the trait</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "zonetraits.trait.trait",
            "description": "<p>The name of the trait</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/zones.js",
    "groupTitle": "Zones"
  },
  {
    "type": "post",
    "url": "/zonetraits",
    "title": "Create zonetrait",
    "name": "Create_zonetrait",
    "group": "Zonetraits",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "trait_id",
            "description": "<p>The id of the trait</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "zone_id",
            "description": "<p>The id of the zone</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the zonetrait that was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "trait_id",
            "description": "<p>The id of the trait</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zone_id",
            "description": "<p>The id of the zone</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The timestamp of when the zonetrait was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The timestamp of when the zonetrait was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "trait",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "trait.id",
            "description": "<p>The id of the trait</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "trait.trait",
            "description": "<p>The name of the trait</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Zonetrait created",
          "content": "HTTP/1.1 201 Created\n{\n\t\"id\": 5,\n\t\"created_at\": \"2017-10-23T10:20:17.000Z\",\n\t\"updated_at\": \"2017-10-23T10:20:17.000Z\",\n\t\"trait_id\": 26,\n\t\"zone_id\": 1,\n\t\"trait\": {\n\t\t\"id\": 26,\n\t\t\"trait\": \"Cheerful\"\n\t}\n}",
          "type": "json"
        },
        {
          "title": "Zonetrait already exists",
          "content": "HTTP/1.1 200 OK\n{\n\t\"id\": 5,\n\t\"created_at\": \"2017-10-23T10:20:17.000Z\",\n\t\"updated_at\": \"2017-10-23T10:20:17.000Z\",\n\t\"trait_id\": 26,\n\t\"zone_id\": 1,\n\t\"trait\": {\n\t\t\"id\": 26,\n\t\t\"trait\": \"Cheerful\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/zonetraits.js",
    "groupTitle": "Zonetraits",
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>Unauthorized request</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ZoneNotFoundError",
            "description": "<p>The zone could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ZoneNotFoundError",
          "content": "\tHTTP/1.1 404 Not Found\n\t{\n \t\terror: \"ZoneNotFoundError\",\n\t\tmessage: `Zone with id '1652' could not be found.`\n\t}",
          "type": "json"
        },
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"Unauthorized request.\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/zonetraits/:id",
    "title": "Delete zonetrait",
    "name": "Delete_zonetrait",
    "group": "Zonetraits",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the zonetrait</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Response",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/zonetraits.js",
    "groupTitle": "Zonetraits",
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>Unauthorized request</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ZonetraitNotFoundError",
            "description": "<p>The zonetrait could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"Unauthorized request.\"\n}",
          "type": "json"
        },
        {
          "title": "ZonetraitNotFoundError",
          "content": "HTTP/1.1 404 Not Found\n{\n\terror: \"ZonetraitNotFoundError\",\n\tmessage: `Zonetrait with id '${this.zonetraitId}' could not be found.`\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/zonetraits/:id/rate",
    "title": "Rate zonetrait",
    "name": "Rate_zonetrait",
    "group": "Zonetraits",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the zonetrait</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>The (new) rating</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the rating</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zonetrait_id",
            "description": "<p>The id of the usertrait</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>The rating given</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>The timestamp of when the rating was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>The timestamp of when the rating was updated</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>The id of the user who rated</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Rating created",
          "content": "HTTP/1.1 201 Created\n{\n\t\"id\": 3,\n\t\"rating\": 5,\n\t\"created_at\": \"2017-10-23T10:43:50.000Z\",\n\t\"updated_at\": \"2017-10-23T10:43:54.975Z\",\n\t\"zonetrait_id\": 3,\n\t\"user_id\": 2\n}",
          "type": "json"
        },
        {
          "title": "Rating updated",
          "content": "HTTP/1.1 200 OK\n{\n\t\"id\": 3,\n\t\"rating\": 5,\n\t\"created_at\": \"2017-10-23T10:43:50.000Z\",\n\t\"updated_at\": \"2017-10-23T10:43:54.975Z\",\n\t\"zonetrait_id\": 3,\n\t\"user_id\": 2\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/zonetraits.js",
    "groupTitle": "Zonetraits",
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "UnauthorizedError",
            "description": "<p>Unauthorized request</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ZonetraitNotFoundError",
            "description": "<p>The zonetrait could not be found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "UnauthorizedError",
          "content": "HTTP/1.1 401 Unauthorized\n{\n\terror: \"UnauthorizedError\",\n\tmessage: \"Unauthorized request.\"\n}",
          "type": "json"
        },
        {
          "title": "ZonetraitNotFoundError",
          "content": "HTTP/1.1 404 Not Found\n{\n\terror: \"ZonetraitNotFoundError\",\n\tmessage: `Zonetrait with id '${this.zonetraitId}' could not be found.`\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/i18n/set-locale/:locale",
    "title": "Set prefered locale",
    "name": "Set_prefered_locale",
    "group": "i18n",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "locale",
            "description": "<p>The locale to set as prefered</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>The code of the locale set</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "translations",
            "description": "<p>An object containing translation data</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "lib/routes/api/i18n.js",
    "groupTitle": "i18n"
  }
] });
